import React, { useState, useEffect } from "react";
import { Dimensions, Text, TextInput, TouchableOpacity, View ,StyleSheet, Keyboard, ScrollView, Picker, KeyboardAvoidingView} from "react-native";
import loginStyles from "../styles/loginStyles";
const window = Dimensions.get('window');
import HeaderCard from "../components/HeaderCard";

import { useSelector } from 'react-redux';
import {selectLanguage} from '../features/language/languageSlice'
import FetchService from "../services/FetchService";

function Login (props) {
  const language = useSelector(selectLanguage);
  const [keyFocus,setKeyFocus]=useState(false)
  const [ dealerCode, setDealerCode ] = useState('')
  const [ showDealerCode, setShowDealerCode ] = useState(false)
  
  const [countries, setCountries]=useState('')
  const [cities, setCities]=useState('')
  const [cityLoading, setCityLoading]=useState(true)
  const [countryLoading, setCountryLoading]=useState(true)
  const [firstName,setFName]=useState('')
  const [lastName,setLName]=useState('')
  const [country,setCountry]=useState({
    capital: "Dhaka",
    code: "BD",
    continent: {_id: "5e821761311eb9259c0ba82d", objectId: "mSxk54vkg6", code: "AS", name: "Asia"},
    continentCode: "AS",
    continentName: "Asia",
    currency: "BDT",
    emoji: "🇧🇩",
    emojiU: "U+1F1E7 U+1F1E9",
    name: "Bangladesh",
    native: "Bangladesh",
    objectId: "AWnxgoUzw0",
    phone: "880",
    _id: "5e8218a9a0be4401500e4d37"
  })
  const [city,setCity]=useState({
    altName: "",
    cityId: 1185241,
    country: "5e8218a9a0be4401500e4d37",
    countryCode: "BD",
    countryName: "Bangladesh",
    isCapital: true,
    location: {latitude: 23.7104, longitude: 90.40744},
    name: "Dhaka",
    objectId: "Q0rzROBMnh",
    population: 10356500,
    _id: "5e82297958cd81174c1df387"
  })
  const [address1,setAddress1]=useState('')
  const [address2,setAddress2]=useState('')
  const [phone,setPhone]=useState('')
  const [email,setEmail]=useState('')
  const [password,setPassword]=useState('')
  const [password2,setPassword2]=useState('')
  const [signupError, setSignupError]=useState('')
  const [ countryId , setCountryId ] = useState("5e8218a9a0be4401500e4d37")
  const [ cityId , setCityId ] = useState("5e82297958cd81174c1df387")

  const _keyboardDidShow = () => {
    setKeyFocus(true)
  };

  const _keyboardDidHide = () => {
    setKeyFocus(false)
  };
  useEffect(()=>{
    FetchService(props.navigation,"GET","/api/geo/country")
    .then(res=>setCountries(res))
    .then(()=>setCountryLoading(false))
    .then ( ()=> FetchService(props.navigation,"GET",`/api/geo/${countryId}/city`))
    .then(res=>setCities(res))
    .then(()=>setCityLoading(false))
    .catch(er=>console.log("--e",er))

    Keyboard.addListener("keyboardDidShow", _keyboardDidShow);
    Keyboard.addListener("keyboardDidHide", _keyboardDidHide);
    return () => {
      Keyboard.removeListener("keyboardDidShow", _keyboardDidShow);
      Keyboard.removeListener("keyboardDidHide", _keyboardDidHide);
    }
  },[])
  function findCity(country){
    setCountry(country)
    setCountryId(country._id)

    FetchService(props.navigation,"GET",`/api/geo/${countryId}/city`)
    .then(res=>setCities(res))
    .then(()=>setCityLoading(false))
    .catch(er=>console.log("--e",er))
  }

  function handleSubmit() {

    if (firstName.length<1){
      setSignupError(
        language==='en'?
        'insert first name'
        :"নামের প্রথম অংশ")
    }
    else if (lastName.length<1){
      setSignupError(
        language==='en'?
        'insert last name'
        :"নামের শেষ অংশ")
    }
    else if (!country._id){
      setSignupError(
        language==='en'?
        'insert country'
        :"দেশের নাম")
    }
    else if (!city._id){
      setSignupError(
        language==='en'?
        'insert city'
        :"শহরের নাম")
    }
    else if (address1.length<1 && address2.length<1){
      setSignupError(
        language==='en'?
        'insert address'
        :"ঠিকানা")
    }
    else if (phone.length<1){
      setSignupError(
        language==='en'?
        'insert phone number'
        :"ফোন নাম্বার")
    }
    // else if (email.length<1){
    //   setSignupError('insert email')
    // }
    else if (password.length<1){
      setSignupError(
        language==='en'?
        'insert password'
        :"পাসওয়ার্ড আবশ্যক")
    }
    else if (password.length<3){
      setSignupError(
        language==='en'?
        'password at least 3 characters'
        :"পাসওয়ার্ড কমপক্ষে তিন ডিজিট")
    }
    else if (password!==password2){
      setSignupError(
        language==='en'?
        'passwords do not match'
        :"পাসওয়ার্ড সাদৃশ্যপূর্ণ নয়")
    }
    else{
      let body={
        firstName,lastName,country:country.name,city:city.name,address1,address2, password,password2,phone,email,dealerCode
      }
      FetchService(props.navigation,"POST","/customer/auth/register",3,body)
      .then(res=>{
        console.log(res)
        if(res.inserted){
          props.navigation.navigate("AppStack")
        }
        else setSignupError(Object.values(res)[0])
      })
      .catch(er=>{
        // setSignupError(er)
      })
    }

  }
    return (
      <View
      style={loginStyles.loginCont}
      >
        {
          !keyFocus?
          <HeaderCard
            botMarg={window.height/12}
            half
            />
          :<View/>
        }
        
      <KeyboardAvoidingView
        style={{
          backgroundColor:'#FFFFDD',
          width: window.width/1.1,
          height: window.height/1.8,
          alignSelf:'center',
          borderRadius:15
        }}
      >
        <Text
        numberOfLines={2}
        style={{
          color:'red', 
          alignSelf:'center',
          margin:20,

        }}>
        {signupError}
        </Text>
      <ScrollView
      contentInsetAdjustmentBehavior="automatic"
      >
        
        
          <View
          style={{
            height:window.height/10,
            width:window.width-50,
            alignItems:'center',
            alignSelf:'center',
            justifyContent:'center'
          }}
          >
          {/* title and input starts */}   
          <Text
          style={{
            color:'#00163D',
            fontSize:20,
            alignSelf:'flex-start',
            marginLeft:10
          }}
          >{
            language==='en'?
            "Registration form"
            :"রেজিস্ট্রেশন ফর্ম"  
          }
          </Text>
          </View>
          <View style={loginStyles.signupEmailTop}>
            <View
            style={{
              flexDirection:'row',
              justifyContent:'space-between',
              width: window.width*305/375,
            }}
            >
              <TextInput
                
                placeholderTextColor="#212121"
                placeholder={
                  language==='en'?
                  "First name"
                  :"নামের প্রথম অংশ"
                }
                value={firstName}
                keyboardType="default"
                style={[loginStyles.txtInputSignup,{
                  width: window.width*140/375,
                }]}
                autoFocus={true}
                onChangeText={setFName}
                blurOnSubmit={false}

                />
                <TextInput
                
                placeholderTextColor="#212121"
                placeholder={
                  language==='en'?
                  "Last name"
                :"নামের শেষ অংশ"
              }
                value={lastName}
                keyboardType="default"
                style={[loginStyles.txtInputSignup,{
                  width: window.width*140/375,
                }]}
                onChangeText={setLName}
                blurOnSubmit={false}

                />
            </View>
            <View
            style={{
              flexDirection:'row',
              justifyContent:'space-between',
              width: window.width*305/375,
            }}
            >
              <View>
              <Text
              style={styles.pickerText}
              >{
                language==='en'?
                      "Country":"দেশ"
              }</Text>
                <View
                style={styles.pickerContainer}
                >
                  {
                    // countryLoading?
                    <Text
                    style={styles.pickerStyle}
                    >{
                    language==='en'?
                    "    Bangladesh"
                    :"    বাংলাদেশ"
                    }</Text>
                    // :<Picker
                    // selectedValue={countryId}
                    // style={styles.pickerStyle}
                    // prompt={
                    //   language==='en'?
                    //   "Select Country":"দেশ নির্বাচন করুন"
                    // }
                    // onValueChange={async (itemValue, itemIndex) =>findCity(itemValue)}>
                    //   {
                    //     countries.map(city=><Picker.Item label={city.name} value={city._id} />)
                    //   }
                    // </Picker>
                  }
                </View>
                </View>
                <View>
                <Text
                style={styles.pickerText}
                >{
                language==='en'?
                "City":"শহর"
                }</Text>
                <View
                style={styles.pickerContainer}
                >
                  {
                    // cityLoading?
                    <Text
                    style={styles.pickerStyle}
                    >{
                    language==='en'?
                    "    Dhaka":"    ঢাকা"
                    }</Text>
                  //   :<Picker
                  //   selectedValue={cityId}
                  //   style={styles.pickerStyle}
                  //   prompt="Select City"
                  //   onValueChange={async (itemValue, itemIndex) =>{
                  //     setCity(itemValue)
                  //     setCityId(itemValue._id)
                  //   }}>
                  //     {
                  //       cities.map(city=><Picker.Item label={city.name} value={city._id} />)
                  //     }
                  // </Picker>

                  }
                  </View>
                </View>
            </View>
            <View>
              <TextInput
                
                placeholderTextColor="#212121"
                placeholder={
                  language==='en'?
                  "Address":"ঠিকানা"
                }
                value={address1}
                keyboardType="default"
                style={loginStyles.txtInputSignup}
                onChangeText={setAddress1}
                blurOnSubmit={false}

                />
            </View>
            <View
            style={{
              flexDirection:'row',
              justifyContent:'space-between',
              width: window.width*305/375,
              alignItems:'center'
            }}
            >{
              showDealerCode?
              <TextInput
                
                placeholderTextColor="#212121"
                placeholder={
                  language==='en'?
                  "Referral Code"
                  :"রেফারেন্স কোড"
                }
                value={dealerCode}
                keyboardType="default"
                style={[loginStyles.txtInputSignup,{
                  width: window.width*140/375,
                }]}
                onChangeText={setDealerCode}
                blurOnSubmit={false}
                />
                :<View
                style={{
                  marginVertical: 10,
                  height: window.height/19,
                  width: window.width*140/375,
                }}
                />
            }
            <TouchableOpacity
            style={{
              height: window.height/19,
              width: window.width/2.7,
              borderRadius:5,
              borderWidth:1,
              justifyContent:'center',
              alignItems:'center',
              borderColor:'#6E5D4B',
            }}
            onPress={()=>{
              setShowDealerCode(!showDealerCode)
            }}
            >
            <Text
                style={{
                  textAlign:'center',
                  color:'#6E5D4B',
                  fontWeight:'bold',
                  fontSize:9
                }}
                >{
                language==='en'?
                "Add referral code\n(optional)":"রেফারেন্স যোগ করুন"
                }</Text>
            </TouchableOpacity>
            </View>
          
            <Text
            style={{
              color:'#00163D',
              fontSize:20,
              alignSelf:'flex-start',
              marginLeft:40,
              marginVertical:10
            }}
            > 
            {
              language==='en'?
              "Contact Info":"যোগাযোগ এর মাধ্যম"
              }
          </Text>
            <View
            style={{
              flexDirection:'row',
              justifyContent:'space-between',
              width: window.width*305/375,
            }}
            >
              <TextInput
                
                placeholderTextColor="#212121"
                placeholder={
                  language==='en'?
                  "Phone":"ফোন"
                }
                value={phone}
                keyboardType='phone-pad'
                style={[loginStyles.txtInputSignup,{
                  width: window.width*140/375,

                }]}
                onChangeText={setPhone}
                blurOnSubmit={false}

                />
                <TextInput
                
                placeholderTextColor="#212121"
                placeholder={
                  language==='en'?
                  "Email ( Not required )":"ইমেইল ( আবশ্যক নয় )"
                }
                value={email}
                keyboardType='email-address'
                style={[loginStyles.txtInputSignup,{
                  width: window.width*140/375,

                }]}
                onChangeText={setEmail}
                blurOnSubmit={false}

                />
            </View>
            <View
            style={{
              flexDirection:'row',
              justifyContent:'space-between',
              width: window.width*305/375,
            }}
            >
              <TextInput
                
                placeholderTextColor="#212121"
                placeholder={
                  language==='en'?
                  "Password":"পাসওয়ার্ড"
                }
                secureTextEntry={true}
                value={password}
                keyboardType="default"
                style={[loginStyles.txtInputSignup,{
                  width: window.width*140/375,
                }]}
                onChangeText={setPassword}
                blurOnSubmit={false}

                />
                <TextInput
                
                placeholderTextColor="#212121"
                placeholder={
                  language==='en'?
                  "Confirm":"কনফার্ম করুন"
                }
                secureTextEntry={true}
                value={password2}
                keyboardType="default"
                style={[loginStyles.txtInputSignup,{
                  width: window.width*140/375,
                }]}
                onChangeText={setPassword2}
                blurOnSubmit={false}

                />
            </View>
          </View>
          {/* title and input ends */}
          <View
            style={{
              margin:15,
              justifyContent:'space-between',
              width:window.width/2,
              alignItems:'center',
              alignSelf:'center'    
            }}
            >
          <TouchableOpacity
        style={{
          height:window.height/15,
          width:window.width/2.5,
          borderRadius:5,
          backgroundColor:"#FFFE01",
          justifyContent:'center'
        }}
        onPress={handleSubmit}
        >
          <Text
          style={{
            fontSize:15,
            color:'black',
            alignSelf:'center'
          }}
          >
            {
            language==='en'?
            "Register":"রেজিস্টার"
            }
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
        style={{
          height:window.height/20,
          width:window.width/2.5,
          alignSelf:'center',
          backgroundColor:"transparent",
          justifyContent:'center',
          marginVertical:20
        }}
        onPress={()=>props.navigation.navigate("Login")}
        >
          <Text
          style={{
            fontSize:12,
            color:'#00163D',
            alignSelf:'center',
            fontWeight:'bold',
            textAlign:'center'
          }}
          >
            {
            language==='en'?
            "Already have account? Login":"লগইন করুন"
            }
          </Text>
        </TouchableOpacity>
        </View>
        </ScrollView>
        </KeyboardAvoidingView>
        
      </View>
    );
  }

const styles = StyleSheet.create({
  pickerText:{
    fontSize:8,
    alignSelf:'flex-start',
    marginVertical:5
  },
  pickerContainer:{
    backgroundColor:'#fff',
    borderRadius:5,
    height:50,
    elevation:3,
    height: window.height/21,
    width: window.width*140/375
  },
  pickerStyle:{
    height: 20,
    width: window.width/2.5,
    marginTop:5
  }
});

  export default Login