
export default function FindNumber (number,language){
    if (language=='en'){
        return number
    }
    else {
        let numberStr=number.toString()
        let bnNumberStr=""
          for (let i = 0 ; i<numberStr.length;i++ ){
              switch(numberStr[i]) {
                  case '0':
                      bnNumberStr=bnNumberStr+"০"
                      break;
                  case '1':
                      bnNumberStr=bnNumberStr+"১"
                      break;
                  case '2':
                      bnNumberStr=bnNumberStr+"২"
                      break;
                  case '3':
                      bnNumberStr=bnNumberStr+"৩"
                      break;
                  case '4':
                      bnNumberStr=bnNumberStr+"৪"
                      break;
                  case '5':
                      bnNumberStr=bnNumberStr+"৫"
                      break;
                  case '6':
                      bnNumberStr=bnNumberStr+"৬"
                      break;
                  case '7':
                      bnNumberStr=bnNumberStr+"৭"
                      break;
                  case '8':
                      bnNumberStr=bnNumberStr+"৮"
                      break;
                  default:
                      bnNumberStr=bnNumberStr+"৯"
                }
          }
          return bnNumberStr
        }
      }