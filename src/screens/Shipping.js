import React, { useState, useEffect } from 'react';
import {
  StyleSheet,
  ScrollView,
  StatusBar,
  Text,
  Dimensions,
  View,
} from 'react-native';
const window = Dimensions.get('window');
import iconCheck from '../assets/icons/iconCheck.png';
import iconCross from '../assets/icons/iconCross.png';

import {
  Colors,
} from 'react-native/Libraries/NewAppScreen';
import HeaderCard from "../components/HeaderCard";

import FetchService from '../services/FetchService';
import { TouchableOpacity } from 'react-native-gesture-handler';
import FastImage from 'react-native-fast-image';

const Shipping: () => React$Node = ({navigation}) => {
  useEffect(()=>{
    // FetchService(navigation,"GET","/api/category")
    // .then(async res=>setData(res.data))
    // .then(()=>setLoading(false))

  },[navigation])


  return (
    <>
      <StatusBar barStyle="dark-content" />
        <ScrollView
          style={styles.scrollView}>
            <HeaderCard
            half
            botMarg={ window.height/9 }
            />
            <View
              style={{
                backgroundColor:'#FFFFDD',
                width: window.width/1.1,
                height: window.height/2.3,
                alignSelf:'center',
                borderRadius:15
              }}
              >
                <Text
                numberOfLines={1}
                style={{
                  fontSize:17,
                  fontWeight:'bold',
                  textAlign:'center',
                  marginTop:50,
                  marginBottom:40
                }}
                >
                  Different shipping address?
                </Text>
                <View
                style={{
                  flexDirection:'row',
                  justifyContent:'space-around'
                }}
                >
                  <View
                  style={{
                    height:window.width/3.5,
                    width:window.width/3.5,
                    backgroundColor:'black',
                    borderRadius:10,
                    alignItems:'center',
                    justifyContent:'center'
                  }}
                  >
                    <TouchableOpacity
                    onPress={()=>navigation.navigate("PayMethod")}
                    >
                    <FastImage
                      style={{
                        width: window.height / 10,
                        height: window.height / 10,
                      }}
                      source={iconCheck}
                    />
                    </TouchableOpacity>
                  </View>

                  <View
                  style={{
                    height:window.width/3.5,
                    width:window.width/3.5,
                    backgroundColor:'black',
                    borderRadius:10,
                    alignItems:'center',
                    justifyContent:'center'
                  }}
                  >
                    <TouchableOpacity
                    onPress={()=>navigation.navigate("PayMethod")}
                    >
                    <FastImage
                      style={{
                        width: window.height / 10,
                        height: window.height / 10,
                      }}
                      source={iconCross}
                    />
                    </TouchableOpacity>
                  </View>
                  
                </View>
              </View>
            
            
        </ScrollView>
    </>
  );
};

const styles = StyleSheet.create({
  scrollView: {
    backgroundColor:"#FFF"
  },
  engine: {
    position: 'absolute',
    right: 0,
  },
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: '600',
    color: Colors.black,
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: '400',
    color: Colors.dark,
  },
  highlight: {
    fontWeight: '700',
  },
  footer: {
    color: Colors.dark,
    fontSize: 12,
    fontWeight: '600',
    padding: 4,
    paddingRight: 12,
    textAlign: 'right',
  },
});

export default Shipping;
