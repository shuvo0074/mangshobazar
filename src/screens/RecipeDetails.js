import React, { useState, useEffect } from 'react';
import {
  Alert,
  Platform,
  Dimensions,
  StatusBar,
  View,
  Text,
  TouchableOpacity,
  ActivityIndicator,
  Image
} from 'react-native';
const window = Dimensions.get('window');
import iconCartBlack from '../assets/icons/iconCartBlack.png';
import iconTag from '../assets/icons/iconTag.png';
import iconCookBlack from '../assets/icons/iconCookBlack.png';
import iconPersonBlack from '../assets/icons/iconPersonBlack.png';
import iconTimeBlack from '../assets/icons/iconTimeBlack.png';
import iconShare from '../assets/icons/iconShare.png';
import RecipeSlider from '../components/RecipeSlider'

import { ScrollView } from 'react-native-gesture-handler';
import FetchService from '../services/FetchService';
import { BaseUrl } from '../env';
import IngredientSlider from "../components/IngredientSlider";
import HTML from "react-native-render-html";

import {
  addToCart,
  selectCart
} from '../features/counter/counterSlice'
import { useSelector, useDispatch } from 'react-redux';
import Styles from '../styles/Styles';
import FastImage from 'react-native-fast-image';
import {selectLanguage} from '../features/language/languageSlice'
import SendAnalytics from '../services/SendAnalytics';

const EventDetails: () => React$Node = ({navigation}) => {
  const language = useSelector(selectLanguage);
  const dispatch = useDispatch();
  const [ htmlHeight, setHtmlHeight] = useState(false)
  const [eventData,setEventData]= useState({})
  const [loading,setLoading]=useState(true)
  const [data,setData]= useState([])
  const [ itemData, setItemData ] = useState([])

  function findItem (id) {
    FetchService(navigation,"GET","/api/post/"+id+"/?requiredProducts=1&image=1&primaryCategory=1")
    .then(async res=>{
      setEventData(res)
      let itemArray=[]
      await res.requiredProducts.forEach(element => {
        FetchService(navigation,"GET","/api/product/"+element._id)
        .then(res=>itemArray.push({
          ...res,
          price:{
            ...res.pricing[res.pricing.findIndex(item=>item._id===element.variation)].price,
            attribute:res.pricing[res.pricing.findIndex(item=>item._id===element.variation)].attribute,
            _id:element.variation,
            defaultVariation:element.variation
          },
          quantity:element.quantity,
          total:res.pricing[res.pricing.findIndex(item=>item._id===element.variation)].price.offer ||
                res.pricing[res.pricing.findIndex(item=>item._id===element.variation)].price.offer !==''?
                parseInt(res.pricing[res.pricing.findIndex(item=>item._id===element.variation)].price.offer)*element.quantity:
                parseInt(res.pricing[res.pricing.findIndex(item=>item._id===element.variation)].price.regular)*element.quantity,
          selectedPrice:res.pricing[res.pricing.findIndex(item=>item._id===element.variation)].price.offer ||
                res.pricing[res.pricing.findIndex(item=>item._id===element.variation)].price.offer !==''?
                parseInt(res.pricing[res.pricing.findIndex(item=>item._id===element.variation)].price.offer):
                parseInt(res.pricing[res.pricing.findIndex(item=>item._id===element.variation)].price.regular)
        }))
        .catch(err=>console.log(err))
      });
      setItemData(itemArray)
    })
    .then(()=>FetchService(navigation,"GET","/api/post?primaryCategory=1"))
    .then(response=>setData(response.data))
    .then(()=>setLoading(false))
  }

  useEffect(()=>{
    const {id} = navigation.state.params
    SendAnalytics(navigation,id,"post")
    findItem(id)
  },[navigation])
    if (loading)
    return <ActivityIndicator size="large" color="#00163D" style={{
    alignSelf:'center',
    marginTop:'80%'
  }} />
  const modEventData = language === 'en'? {...eventData}:{...eventData,...eventData.bn}

    return (
    <>
      <StatusBar barStyle="dark-content" />
      <ScrollView
      style={{
        width:window.width,
        backgroundColor:'white'
      }}
      >
        {/* Top Deatils Card */}
        <View
        style={{
          height:window.height/2.6,
          width:window.width,
          backgroundColor:'white'
        }}
        >
        <View
        style={{
          height:window.height/2.9,
          width:window.width,
          backgroundColor:"#FFB655",
          borderBottomLeftRadius:20,
          borderBottomRightRadius:20
        }}
        >
          {/* Image - Title Container */}
          <View
          style={{
            height:window.height/4.5,
            width:window.width,
            backgroundColor:"#FEFEBE",
            borderBottomLeftRadius:20,
            borderBottomRightRadius:20,
            flexDirection:'row'
          }}
          >
            <View
            style={{
              width:window.width/2,
              height:window.height/4.5,
              borderBottomLeftRadius:20,
            }}
            >
              <FastImage
              source={{
                priority: FastImage.priority.high,
                uri:
                eventData.image?
                BaseUrl+eventData.image[0].full:"http://mangshobazar.com:5000/images/logo-app.png"
              }}
              style={{
                height:window.height/4.5,
                width:window.width/2,
                resizeMode:'contain',
                borderBottomLeftRadius:20
              }}
              />
            </View>

            <View
            style={{
              width:window.width/2,
              height:window.height/4.5,
              borderBottomRightRadius:20,
              justifyContent:'center'
            }}
            >
              <Text
              numberOfLines={2}
              style={{
                fontSize:20,
                fontWeight:'bold',
                marginLeft:10
              }}
              >{modEventData.name}</Text>

              <Text
              numberOfLines={2}
              style={{
                fontSize:12
              }}
              >
                {/* {modEventData.} */}
              </Text>
            </View>
          </View>
          {/* Image - Title Container */}
          {/* Meta Data container */}
          <View
          style={{
            flexDirection:'row'
          }}
          >
            <View
            style={{
              width:window.width/3,
              height:window.height/13,
              marginTop:5,
              flexDirection:"row",
              justifyContent:'center',
              alignItems:'center'
            }}
            >
              <Image
              source={iconPersonBlack}
              style={{
                resizeMode:'contain',
                height:window.height/25,
                width:window.height/25
              }}
              />
              <Text
              style={{
                margin:4
              }}
              >{modEventData.preparationTime}</Text>
            </View>
            <View
            style={{
              width:window.width/3,
              height:window.height/13,
              marginTop:5,
              flexDirection:"row",
              justifyContent:'center',
              alignItems:'center'
            }}
            >
              <Image
              source={iconTimeBlack}
              style={{
                resizeMode:'contain',
                height:window.height/25,
                width:window.height/25
              }}
              />
              <Text
              style={{
                margin:4
              }}
              >{modEventData.cookingTime}</Text>
            </View>
            <View
            style={{
              width:window.width/3,
              height:window.height/13,
              marginTop:5,
              flexDirection:"row",
              justifyContent:'center',
              alignItems:'center'
            }}
            >
              <Image
              source={iconCookBlack}
              style={{
                resizeMode:'contain',
                height:window.height/25,
                width:window.height/25
              }}
              />
              <Text
              style={{
                margin:4
              }}
              >{modEventData.servingSize}</Text>
            </View>
          </View>
        </View>
        {/* add all */}
        <TouchableOpacity
          style={{
            width:window.width/3,
            height:window.height/20,
            backgroundColor:'white',
            borderRadius:window.height/40,
            elevation:5,
            position:'absolute',
            bottom:window.height/60,
            left:window.width/3,
            justifyContent:'space-between',
            flexDirection:'row',
            alignItems:'center',
            paddingHorizontal:10
          }}
          onPress={()=>{
            itemData.forEach(element => {
              dispatch(addToCart(element))
            })
          }
          }
          >
            <Image
              source={iconCartBlack}
              style={{
                resizeMode:'contain',
                height:window.height/30,
                width:window.height/30,
                alignSelf:'center'
              }}
              />
            <Text
              style={{
                fontSize:12
              }}
              >Add all to cart</Text>
          </TouchableOpacity>
          {/* add all */}
        {/* Top Deatils Card without add all */}
        </View>
        {/* Top Deatils Card */}
        <IngredientSlider
        data={itemData.slice(0,8)}
        title="Ingredients"
        onClick={(id)=>navigation.navigate("EventDetails",{id})}
        />
        {/* Recipe description */}

        <Text
        style={{
          fontWeight:'bold',
          marginVertical:10,
          marginLeft:10
        }}
        >Recipe</Text>

        <View
        style={{
          width:window.width,
          backgroundColor:"#FFB655",
          borderBottomLeftRadius:20,
          borderBottomRightRadius:20
        }}
        >
          <View
          style={{
            width:window.width,
            height:window.height/3,
            backgroundColor:"#FEFEBE",
            borderBottomLeftRadius:20,
            borderBottomRightRadius:20  
          }}
          >
            <FastImage
              source={{
                priority: FastImage.priority.high,
                uri:
                eventData.cover?
                BaseUrl+eventData.cover.full:"http://mangshobazar.com:5000/images/logo-app.png"
              }}
              style={{
                width: window.width,
                height:window.height /3,
                resizeMode: 'contain',
                alignSelf:'center',
                borderBottomLeftRadius:20,
                borderBottomRightRadius:20
              }}
            />
            {/* share */}
            {/* <TouchableOpacity
              style={{
                width:window.width/4,
                height:window.height/20,
                backgroundColor:'white',
                borderRadius:window.height/40,
                elevation:5,
                position:'absolute',
                top:window.height/40,
                right:window.height/40,
                justifyContent:'space-between',
                flexDirection:'row',
                alignItems:'center',
                paddingHorizontal:5,
                position:'absolute',
              }}
            >
              <View
              style={{
                height:window.height/25,
                width:window.height/25,
                borderRadius:window.height/25,
                borderWidth:1,
                backgroundColor:"#FFB655",
                justifyContent:'center',
                alignItems:'center'
              }}
              >
              <Image
                  source={iconShare}
                  style={{
                    resizeMode:'contain',
                    height:window.height/40,
                    width:window.height/40,
                    alignSelf:'center'
                  }}
                />
              </View>
                
                <Text
                  style={{
                    fontSize:12,
                    fontWeight:'bold',
                    marginRight:window.width/40
                  }}
                  >Share</Text>
              </TouchableOpacity> */}
          {/* share */}
          </View>
          <View
            style={{
              width:window.width/1.2,
              // height:htmlHeight?window.height/3:window.height/6,
              alignSelf:'center',
              justifyContent:'center',
              paddingTop:10
            }}
            >
            <HTML
              html={modEventData.body}
            />
          </View>
        </View>
        {/* Recipe description */}

        <RecipeSlider
          data={data}
          navigation={navigation}
          title="Related Recipe"
          limit={6}
          darkText
          />

      </ScrollView>
    </>
  );
};

export default EventDetails;
