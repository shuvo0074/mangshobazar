import React, { useEffect, useState } from 'react';
import {
  SafeAreaView,
  StyleSheet,
  Dimensions,
  StatusBar,
  View,
  Text,
  TouchableOpacity,
  Keyboard,
  KeyboardAvoidingView
} from 'react-native';
const window = Dimensions.get('window');

import {
  Colors,
} from 'react-native/Libraries/NewAppScreen';
import HeaderCard from "../components/HeaderCard";
import Geolocation from '@react-native-community/geolocation';
import { ScrollView, TextInput } from 'react-native-gesture-handler';
import iconTrash from '../assets/icons/iconTrash.png';
import FastImage from 'react-native-fast-image';

import {
  removeFromCart,
  selectCart,
  addToCart
} from '../features/counter/counterSlice'
import {selectLanguage} from '../features/language/languageSlice'
import { useSelector, useDispatch, connect } from 'react-redux';
import FindNumber from '../services/FindNumber';
import FetchService from '../services/FetchService';

const Cart: () => React$Node = (props) => {
  const [keyFocus,setKeyFocus]=useState(false)
  const [ couponResult,setCouponResult] = useState('')
  const [ couponValid, setCouponValid ] = useState(false)
  const _keyboardDidShow = () => {
    setKeyFocus(true)
  };

  const _keyboardDidHide = () => {
    setKeyFocus(false)
  };
  useEffect(()=>{
    Geolocation.setRNConfiguration({
      authorizationLevel:'always'
    });
    Geolocation.requestAuthorization

    // Geolocation.getCurrentPosition(info => console.log(info));
      Geolocation.getCurrentPosition(
        (...args) => {
          console.log('getCurrentPosition', args);
        },
        err => {
          console.log('getCurrentPosition.error', err);
          // debugger;
        },
        {
          timeout: 20000,
        },
      )

    Keyboard.addListener("keyboardDidShow", _keyboardDidShow);
    Keyboard.addListener("keyboardDidHide", _keyboardDidHide);

    return () => {
      Keyboard.removeListener("keyboardDidShow", _keyboardDidShow);
      Keyboard.removeListener("keyboardDidHide", _keyboardDidHide);
    }
  },[])
  const [coupon,setCoupon] = useState('')
  const {cart,total} = useSelector(selectCart);
  const language = useSelector(selectLanguage);
  const dispatch = useDispatch();
  function validateCoupon(){
    let items=[]
    cart.forEach(cartItem=>items.push({product:cartItem._id,quantity:cartItem.quantity,variation:cartItem.price._id}))

    const body={
      items,coupon
    }
    FetchService(props.navigation,"POST","/api/validatecoupon",3,body,false)
    .then(res=>{
      setCouponResult(res.discountAmount?res.discountAmount:0)
      setCouponValid(true)
    })
    .catch(err=>setCouponResult(err))
  }
  return (
    <>
      <StatusBar barStyle="dark-content" />
      <SafeAreaView
      style={styles.scrollView}
      >{
        !keyFocus?
        <HeaderCard
          half
          botMarg={ window.height/8 }
          />
          :<View/>
      }
      {/* table header */}
      <ScrollView
      style={{
        backgroundColor:'#F9F89C',
        borderTopLeftRadius:20,
        borderTopRightRadius:20,
        paddingVertical:50
      }}
      >
      <View
        style={{
          height:window.height/12,
          width:window.width-20,
          borderBottomWidth:1,
          borderTopWidth:1,
          alignSelf:'center',
          flexDirection:'row'
        }}
      >
        <View
        style={{
          width:window.width/3,
          justifyContent:'center',
          alignItems:'center'
        }}
        >
        <Text
          style={{
            fontSize:14,
            fontWeight:'bold'
          }}
          >{
          language==='en'?
          "Product"
          :"পণ্য"}
          </Text>
        </View>

        <View
        style={{
          width:window.width/7,
          justifyContent:'center',
          alignItems:'center'
        }}
        >
          <Text
          style={{
            fontSize:14,
            fontWeight:'bold'
          }}
          >{
            language==='en'?
            "Price":
            "দাম"}
          </Text>
        </View>

        <View
        style={{
          width:window.width/7,
          justifyContent:'center',
          alignItems:'center'
        }}
        >
        <Text
          style={{
            fontSize:13,
            fontWeight:'bold'
          }}
          >{
            language==='en'?
            "Quantity":
            "পরিমাণ"
          }
          </Text>
        </View>

        <View
        style={{
          width:window.width/7,
          justifyContent:'center',
          alignItems:'center'
        }}
        >
        <Text
          style={{
            fontSize:14,
            fontWeight:'bold'
          }}
          >{
            language==='en'?
            "Total":"মোট"}
          </Text>
        </View>

        <View
        style={{
          width:window.width/7,
          justifyContent:'center',
          alignItems:'center'
        }}
        >
        {/* <Text
          style={{
            fontSize:14
          }}
          >{
            language==='en'?
          "Action":""}
          </Text> */}
        </View>

      </View>

      {/* table body */}

      <View
      style={{
        // height:window.height/12*cart.length+20,
        width:window.width-20,
        borderBottomWidth:1,
        alignSelf:'center',
        // paddingTop:20
      }}
      >{
        cart.map(prod=>{
          const product = language==='en'?
            {...prod}:
            {
              ...prod,
              ...prod.bn
            }
          return (
            <View
        style={{
          height:window.height/8,
          width:window.width-20,
          alignSelf:'center',
          flexDirection:'row'
        }}
        key={product.price._id}
      >
        <View
        style={{
          width:window.width/3,
          justifyContent:'center',
          alignItems:'center'
        }}
        >
        <TouchableOpacity
        style={{
          borderRadius:4
        }}
        onPress={() => props.navigation.navigate("EventDetails",{id:product._id})}
        >
        <Text
          style={{
            fontSize:12,
            fontWeight:'bold'
          }}
          >{product.name}
          </Text>
        </TouchableOpacity>
        </View>

        <View
        style={{
          width:window.width/7,
          justifyContent:'center',
          alignItems:'center'
        }}
        >
          <Text
          style={{
            fontSize:12,
            fontWeight:'bold',
            textAlign:'center'
          }}
          >{FindNumber(product.selectedPrice,language)} / {Object.values(product.price.attribute)[0]}
          </Text>
        </View>

        <View
        style={{
          width:window.width/7,
          justifyContent:'center',
          alignItems:'center',
        }}
        >
          <TouchableOpacity
          style={{
            padding:4,
          }}
          onPress={()=>{
            let storeData ={
              ...product,
              quantity:product.quantity+1,
              total:parseInt(product.total)+parseInt(product.selectedPrice)
            }
            dispatch(addToCart(storeData))
          }}
          >
          <View
            style={{
              height:window.height/25-4,
              margin:2,
              width:window.height/25-4,
              borderRadius:window.height/25-4,
              backgroundColor:'black',
              alignItems:'center',
              justifyContent:'center'
            }}
              >
                  <Text
                  style={{
                    fontSize:16,
                    fontWeight:'bold',
                    textAlign:'center',
                    color:'#fff',
                  }}
                  >+</Text>
              </View>
          </TouchableOpacity>
        <Text
          style={{
            fontSize:15,
            fontWeight:'bold',
            textAlign:'center'
          }}
          >{FindNumber(product.quantity,language)}</Text>
          <TouchableOpacity 
          style={{
            padding:4,
          }}
          onPress={()=>{
            if(product.quantity>1){
              let storeData ={
              ...product,
              quantity:product.quantity-1,
              total:parseInt(product.total)-parseInt(product.selectedPrice)
            }
            dispatch(addToCart(storeData))
          }}
        }
        >
        <View
            style={{
              height:window.height/25-4,
              margin:2,
              width:window.height/25-4,
              borderRadius:window.height/25-4,
              backgroundColor:'black',
              alignItems:'center',
              justifyContent:'center'
            }}
          >
              <Text
              style={{
                fontSize:20,
                fontWeight:'bold',
                textAlign:'center',
                color:'#fff',
                marginBottom:3
              }}
              >-</Text>
          </View>
          </TouchableOpacity>
          
        </View>

        <View
        style={{
          width:window.width/7,
          justifyContent:'center',
          alignItems:'center'
        }}
        >
        <Text
          style={{
            fontSize:15,
            fontWeight:'bold'
          }}
          >{FindNumber(product.total,language)}
          </Text>
        </View>

        <TouchableOpacity
        style={{
          width:window.width/7,
          justifyContent:'center',
          alignItems:'center'
        }}
        onPress={()=>dispatch(removeFromCart({
          dataId:product._id,
          priceId:product.price._id,
          total:product.total
        }))}
        >
          <View
          style={{
            backgroundColor:"#B7B609",
            alignItems:'center',
            justifyContent:'center',
            alignSelf:'center',
            width: window.height / 28,
            height: window.height / 28,
            borderRadius: 5
          }}
          >
            <FastImage
              style={{
                width: window.height / 45,
                height: window.height / 45,
              }}
              source={iconTrash}
            />
        </View>
        </TouchableOpacity>
      </View>
          )
        }
      )
      }
      </View>
      {/* table rows end */}

      {/* Total */}
      <View
      style={{
        alignSelf:'flex-end',
        height:window.height/14,
        width:window.width/2
    }}
      >
        <Text
        style={{
          fontSize:16,
          margin:7,
          textAlign:'left',
          fontWeight:'bold',
          marginLeft:-20
        }}
        >
          {
          language==='en'?
          "Total Price: ৳ ":"মোট দামঃ ৳ " }{ FindNumber(total,language)}
        </Text>

      </View>
      {
      couponResult!==''?
      <Text
      style={{
        textAlign:'center',
        fontSize:15,
        color:'green',
        marginBottom:10
      }}
    >
    {
      language==='en'?
      `You got ৳${couponResult} discount by applying coupon ${coupon}`
      :`আপনি পেয়েছেন ৳${couponResult} ডিসকাউন্ট এই কুপন: ${coupon} থেকে`
    }
    </Text>:
    <View/>
    }
    {
      cart.length?
      <KeyboardAvoidingView
      style={{
        width:window.width,
        height:window.height/12,
        flexDirection:'row',
        justifyContent:'space-evenly'
      }}
      >
        <TextInput
          placeholderTextColor="#212121"
          placeholder="Enter coupon"
          value={coupon}
          keyboardType="default"
          style={{
            color: '#616161',
            height:window.height/22,
            width: window.width/3,
            fontSize: 12,
            padding:0,
            paddingLeft: window.width*25/375,
            borderRadius:5,
            backgroundColor:"#fff",
            elevation:5
          }}
          onChangeText={setCoupon}
          blurOnSubmit={false}
          />
          <TouchableOpacity
              onPress={validateCoupon}
              style={{
                height:window.height/22,
                width: window.width/3,
                backgroundColor:'grey',
                borderRadius:5,
                justifyContent:'center',
                elevation:5
              }}
              >
              <Text
              style={{
                textAlign:'center',
                color:'#3B3B3B',
                fontSize:14,
                fontWeight:'bold'
              }}
              >{
                language==='en'?
                "Apply":"এপ্লাই"
                }</Text>
            </TouchableOpacity>
      </KeyboardAvoidingView>:
    <View/>  
    }
        <View
        style={{
          height:window.height/1.6,
          alignSelf:'center'
        }}
        >
            {
              cart.length?
              <TouchableOpacity
            onPress={()=>props.navigation.navigate("Checkout",{coupon,couponValid})}
            style={{
              height:window.height/22,
              width: window.width/3,
              backgroundColor:'#3B3D2F',
              borderRadius:5,
              alignSelf:'flex-end',
              marginTop:window.width/20,
              justifyContent:'center',
              elevation:5
            }}
            >
              <Text
              style={{
                textAlign:'center',
                color:'#fff',
                fontSize:11,
                fontWeight:'bold'
              }}
              >{
                language==='en'?
                "Place Order":"অর্ডার করুন"
                }</Text>
            </TouchableOpacity>
            :<View/>
            }
            <TouchableOpacity
              onPress={()=>props.navigation.navigate("Home")}
              style={{
                height:window.height/22,
                width: window.width/3,
                backgroundColor:'#949494',
                borderRadius:5,
                alignSelf:'flex-end',
                marginTop:window.width/40,
                justifyContent:'center',
                elevation:5
              }}
              >
              <Text
              style={{
                textAlign:'center',
                color:'#3B3B3B',
                fontSize:14,
                fontWeight:'bold'
              }}
              >{
                language==='en'?
                "More Shopping":"শপিং করুন"
                }</Text>
            </TouchableOpacity>
        </View>
        {
          keyFocus?
          <View
          style={{
            height:110
          }}
          />:<View/>
        }
        </ScrollView>
      </SafeAreaView>
    </>
  );
};

const styles = StyleSheet.create({
  scrollView: {
    paddingBottom:80,
    backgroundColor: "#fff",
    minHeight:window.height
  },
  engine: {
    position: 'absolute',
    right: 0,
  },
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: '600',
    color: Colors.black,
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: '400',
    color: Colors.dark,
  },
  highlight: {
    fontWeight: '700',
  },
  footer: {
    color: Colors.dark,
    fontSize: 12,
    fontWeight: '600',
    padding: 4,
    paddingRight: 12,
    textAlign: 'right',
  },
});
export default connect()(Cart)
