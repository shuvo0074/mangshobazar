import React, { useState, useEffect } from 'react';
import {
  Image,
  StyleSheet,
  Dimensions,
  StatusBar,
  ScrollView,
  View,
  Text,
  ActivityIndicator,
  TouchableOpacity
} from 'react-native';
const window = Dimensions.get('window');
import iconLocation from '../assets/icons/iconLocation.png';
// import iconPhone from '../assets/icons/iconPhone.png';
import iconPhone from '../assets/icons/iconPhone.png';
import iconMail from '../assets/icons/iconMail.png';
import iconEdit from '../assets/icons/iconEdit.png';
import editPassword from '../assets/icons/editPassword.png';
import iconLogout from '../assets/icons/iconLogout.png';
import iconHistory from '../assets/icons/iconHistory.png';
// import iconProfileDetails from '../assets/icons/iconProfileDetails.png';
import ImagePicker from 'react-native-image-picker';
import AsyncStorage from '@react-native-community/async-storage';
import {selectLanguage} from '../features/language/languageSlice'
import { useSelector } from 'react-redux';

import HeaderCard from "../components/HeaderCard";

const pickerOptions = {
  title: 'Select Image',
  cancelButtonTitle:'Go back',
  cameraType:'front',
  mediaType:'photo',
  quality:1,
  storageOptions: {
    skipBackup: true,
    path: 'mangsho_bazar',
  },
};
import {
  Colors,
} from 'react-native/Libraries/NewAppScreen';
import FetchService from '../services/FetchService';
import FastImage from "react-native-fast-image";
import defaultLogo from '../assets/images/defaultLogo.png';


const Profile: () => React$Node = ({navigation}) => {
  const language = useSelector(selectLanguage);
  function getProduct (){
    AsyncStorage.getItem('userToken')
    .then(sesToken=>{
      if (sesToken){
        FetchService(navigation,"GET","/customer/api/profile")
        .then(response=>setData(response))
        .then(()=>setLoading(false))
      }
      else {
        navigation.navigate("AuthStack")
      }
    })
  }
  useEffect(()=>{
    const unsubscribe = navigation.addListener('focus',getProduct)
    return unsubscribe;
  },[navigation])
  const [loading, setLoading] = useState(false)
  const [data,setData]=useState([])

  const [ menuDrop , setMenuDrop ] = useState(false)
  const [eventData,setEventData]= useState({})
    function uploadImage(){
      ImagePicker.showImagePicker(pickerOptions, (response) => {
      
        if (response.didCancel) {
          console.log('User cancelled image picker');
        } else if (response.error) {
          console.log('ImagePicker Error: ', response.error);
        } else if (response.customButton) {
          console.log('User tapped custom button: ', response.customButton);
        } else {
          const source = { uri: response.uri };
      
          // You can also display the image using data:
          // const source = { uri: 'data:image/jpeg;base64,' + response.data };
          // setProfileImage(source)
        }
      });
      
    }
  if (loading)
  return <ActivityIndicator size="large" color="#00163D" style={{
    alignSelf:'center',
    marginTop:'80%'
  }} />
  return (
    <>
      <StatusBar barStyle="dark-content" />
      <HeaderCard
        botMarg={window.height/13}
        half
        />
        <ScrollView
          contentInsetAdjustmentBehavior="automatic"
          style={styles.scrollView}>
        <View
        style={styles.imageHolder}
        >
          <Image
            source={{uri:"http://mangshobazar.com/images/logo-app.png"}}
            style={{
              width: window.width,
              height:window.height /3,
              borderRadius:20,
              resizeMode: 'contain',
              backgroundColor:'#FDFEC8'
            }}
          />
        </View>
        <View
          style={{
            width:window.width/1.1,
            height:window.height/1.95,
            elevation:10,
            marginTop:-window.height/15,
            borderRadius:window.height/20,
            backgroundColor:'white',
            alignSelf:'center',
            marginBottom:window.height/25

          }}
        >
        {/* name-edit bar */}
        <View
        style={{
          width:window.width/1.4,
          height:window.height/10,
          alignSelf:'center',
          flexDirection:'row',
          // backgroundColor:'red',
          justifyContent:'space-between',
          alignItems:'center'
        }}
        >
            <Text
            style={{
              fontSize:17,
              fontWeight:'bold',
              marginTop:40,
            }}
            >
              {data.firstName} {data.lastName}
            </Text>
            <TouchableOpacity
            style={{
              height:window.height/20,
              width:window.height/20,
              borderRadius:window.height/40,
              borderWidth:1,
              justifyContent:'center',
              alignItems:'center',
              marginRight: -window.width/5,
            }}
              onPress={()=>navigation.navigate("ChangePassword")}
            >
              <FastImage
              source={editPassword}
              style={{
                width: window.height/40,
                height:window.height/40,
                resizeMode: 'cover'
              }}
            />
            </TouchableOpacity>

            <TouchableOpacity
            style={{
              height:window.height/20,
              width:window.height/20,
              borderRadius:window.height/40,
              borderWidth:1,
              justifyContent:'center',
              alignItems:'center',
              marginRight: -window.width/20
            }}
              onPress={()=>navigation.navigate("EditProfile")}
            >
              <FastImage
              source={iconEdit}
              style={{
                width: window.height/40,
                height:window.height/40,
                resizeMode: 'cover'
              }}
            />
            </TouchableOpacity>

        </View>
        {/* name-edit bar ends */}

        {/* details section */}
        <View
        style={{
          width:window.width,
          height:window.height/3,
          // backgroundColor:'white',
          justifyContent:"space-evenly"
        }}
        >
          <View
          style={{
            height:window.height/12,
            width:window.width,
            // backgroundColor:'red',
            flexDirection:'row'
          }}>
            <View
            style={{
              height:window.height/12,
              width:window.width/3.7,
              alignItems:'center',
              justifyContent:'center',
              // backgroundColor:'blue'
            }}
            >

            <FastImage
              source={iconLocation}
              style={{
                width: window.height/20,
                height:window.height/20,
                resizeMode: 'cover'
              }}
            />
            </View>
            <View
            style={{
              height:window.height/12,
              width:window.width-window.width/2.5,
              justifyContent:'center',
            }}
            >
              <Text
              numberOfLines={2}
              style={{
                fontSize:12,
                marginTop:-5
              }}
              >{data.address1 && data.address1.length?
                data.address1:"Not available"}
              </Text>
            </View>

          </View>

          <View
          style={{
            height:window.height/12,
            width:window.width,
            // backgroundColor:'red',
            flexDirection:'row'
          }}>
            <View
            style={{
              height:window.height/12,
              width:window.width/3.7,
              alignItems:'center',
              justifyContent:'center',
              // backgroundColor:'blue'
            }}
            >

            <FastImage
              source={iconPhone}
              style={{
                width: window.height/20,
                height:window.height/20,
                resizeMode: 'cover'
              }}
            />
            </View>
            <View
            style={{
              height:window.height/12,
              width:window.width-window.width/2.5,
              justifyContent:'center'
            }}
            >
              <Text
              numberOfLines={2}
              style={{
                fontSize:12,
                marginTop:-5
              }}
            >{data.phone && data.phone.length?
              data.phone
              :"Not available"
            }</Text>

            </View>

          </View>

          <View
          style={{
            height:window.height/12,
            width:window.width,
            // backgroundColor:'red',
            flexDirection:'row'
          }}>
            <View
            style={{
              height:window.height/12,
              width:window.width/3.7,
              alignItems:'center',
              justifyContent:'center',
            }}
            >

            <FastImage
              source={iconMail}
              style={{
                width: window.height/20,
                height:window.height/20,
                resizeMode: 'cover'
              }}
            />
            </View>
            <View
            style={{
              height:window.height/12,
              width:window.width-window.width/2.5,
              justifyContent:'center'
            }}
            >
              <Text
              numberOfLines={2}
              style={{
                fontSize:12,
                marginTop:-5
              }}
            >{data.email && data.email.length?
              data.email:
              "Not available"
            }</Text>

            </View>
          </View>
        </View>
        {/* details end */}

        {/* button row */}
        <View
        style={{
          width:window.width/1.2,
          height:window.height/7,
          flexDirection:'row',
          justifyContent:'space-between',
          alignSelf:'center'
        }}
        >
          <TouchableOpacity
          style={{
            height:window.height/18,
            width:window.width/3,
            backgroundColor:'#FFF',
            borderRadius:window.height/40,
            justifyContent:'center',
            alignItems:'center',
            elevation:5,
            flexDirection:'row'
          }}
          onPress={ ()=>{
            AsyncStorage.removeItem('userToken')
            navigation.navigate("AuthStack")
          }}
          >

            <FastImage
              source={iconLogout}
              style={{
                marginRight:15,
                width: window.height/30,
                height:window.height/30,
                resizeMode: 'contain'
              }}
            />
            <Text
            style={{
              fontSize:12,
              fontWeight:'bold',
              textAlign:'center'
            }}
            >
              {
            language==='en'?
            "Logout":"লগআউট"
            }
            </Text>
          </TouchableOpacity>

          <TouchableOpacity
          style={{
            height:window.height/18,
            width:window.width/3,
            backgroundColor:'#FFF',
            borderRadius:window.height/40,
            justifyContent:'center',
            alignItems:'center',
            elevation:5,
            flexDirection:'row'
          }}
          onPress={()=>navigation.navigate("OrderHistory")}
          >
            <FastImage
              source={iconHistory}
              style={{
                marginRight:5,
                width: window.height/30,
                height:window.height/30,
                resizeMode: 'contain'
              }}
            />
            <Text
            style={{
              fontSize:12,
              fontWeight:'bold',
              textAlign:'left'
            }}
            >
            {
            language==='en'?
            "Order history":"আগের অর্ডার"
            }
            </Text>
          </TouchableOpacity>

        </View>
        {/* button row */}


        </View>
        

        </ScrollView>
    </>
  );
};

const styles = StyleSheet.create({
  slideShow: {
    width: window.width,
    height: window.height /2.5,
    justifyContent: 'center',
    alignItems: 'center',
  },
  imageHolder: {
      width: window.width,
      height: window.height /2.8,
      alignItems:'center',
      borderTopLeftRadius:20,
      borderTopRightRadius:20,
  },
  imgFit: {
      width: window.width/1.2,
      height:window.height /2.4,
      borderTopLeftRadius:40,
      borderTopRightRadius:40,
      resizeMode: 'cover'
  },
  indicator: {
      width: window.width * 4 / 375,
      height: window.width * 4 / 375,
      borderRadius: window.width * 2 / 375,
      backgroundColor: '#fff'
    },
  indicatorSelected: {
      width: window.width * 12 / 375,
      height: window.width * 4 / 375,
      borderRadius: window.width * 2 / 375,
      backgroundColor: '#fff'
  },
  sliderLargeText: {
      marginTop:35,  
      fontSize:20,
      color:'white',
      fontWeight:'700'
  },
  scrollView: {
    backgroundColor: "#FFF",
    borderTopLeftRadius:20,
    borderTopRightRadius:20,
  },
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: '600',
    color: Colors.black,
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: '400',
    color: Colors.dark,
  },
  highlight: {
    fontWeight: '700',
  },
  footerIcon: {
		width: window.height / 40,
		height: window.height / 40,
	},
  footer: {
    fontSize: 11,
    fontWeight: '600',
  },
	footerTxt: {
		fontSize: 8,
		color: 'black'
  },
  centeredView: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    marginTop: 22
  },
  modalView: {
    margin: 20,
    backgroundColor: "white",
    borderRadius: 20,
    padding: 35,
    alignItems: "center",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5
  },
  openButton: {
    backgroundColor: "#F194FF",
    borderRadius: 20,
    padding: 10,
    elevation: 2
  },
  textStyle: {
    color: "white",
    fontWeight: "bold",
    textAlign: "center"
  },
  modalText: {
    marginBottom: 15,
    textAlign: "center"
  }
});

export default Profile;
