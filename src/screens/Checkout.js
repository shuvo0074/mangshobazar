import React, { useState, useEffect } from "react";
import { Image, Dimensions, Text, TextInput, TouchableOpacity, View ,StyleSheet, Keyboard, ScrollView, KeyboardAvoidingView, Modal, ActivityIndicator} from "react-native";
import loginStyles from "../styles/loginStyles";
const window = Dimensions.get('window');
import HeaderCard from "../components/HeaderCard";
import {Picker} from "@react-native-community/picker"
import FetchService from "../services/FetchService";
import iconPurchased from '../assets/icons/iconPurchased.png';
import {
  selectCart,
  cleanCart
} from '../features/counter/counterSlice'
import { useSelector, useDispatch, connect } from 'react-redux';
import AsyncStorage from '@react-native-community/async-storage';
import { 
  systemWeights
} from 'react-native-typography';
import {selectLanguage} from '../features/language/languageSlice'
function Checkout (props) {
  const {coupon,couponValid}=props.navigation.state.params
  const language = useSelector(selectLanguage);
  const [keyFocus,setKeyFocus]=useState(false)
  const [modalVisible, setModalVisible] = useState(false);
  const [countries, setCountries]=useState('')
  const [cities, setCities]=useState('')
  const [loading, setloading]=useState(true)
  const [countryLoading, setCountryLoading]=useState(true)
  const [firstName,setFName]=useState('')
  const [lastName,setLName]=useState('')
  const [token,setToken]=useState({
    capital: "Dhaka",
    code: "BD",
    continent: {_id: "5e821761311eb9259c0ba82d", objectId: "mSxk54vkg6", code: "AS", name: "Asia"},
    continentCode: "AS",
    continentName: "Asia",
    currency: "BDT",
    emoji: "🇧🇩",
    emojiU: "U+1F1E7 U+1F1E9",
    name: "Bangladesh",
    native: "Bangladesh",
    objectId: "AWnxgoUzw0",
    phone: "880",
    _id: "5e8218a9a0be4401500e4d37"
  })
  const [city,setCity]=useState({
    altName: "",
    cityId: 1185241,
    country: "5e8218a9a0be4401500e4d37",
    countryCode: "BD",
    countryName: "Bangladesh",
    isCapital: true,
    location: {latitude: 23.7104, longitude: 90.40744},
    name: "Dhaka",
    objectId: "Q0rzROBMnh",
    population: 10356500,
    _id: "5e82297958cd81174c1df387"
  })
  const [ deliveries, setDeliveries ] = useState([])
  const [ delivery , setDelivery ] = useState(-1)
  const [address1,setAddress1]=useState('')
  const [address2,setAddress2]=useState('')
  const [phone,setPhone]=useState('')
  const [email,setEmail]=useState('')
  const [password,setPassword]=useState('')
  const [password2,setPassword2]=useState('')
  const [signupError, setSignupError]=useState('')
  const [ zipCode,setZipCode]= useState('1209')
  const [ additionalInfo,setAddInfo]= useState("No info")
  const [ createAccount, seTcreateAccount] = useState(true)
  const {cart} = useSelector(selectCart);
  const dispatch = useDispatch();
  const [ countryId , setCountryId ] = useState("5e8218a9a0be4401500e4d37")
  const [ cityId , setCityId ] = useState("5e82297958cd81174c1df387")

  const _keyboardDidShow = () => {
    setKeyFocus(true)
  };

  const _keyboardDidHide = () => {
    setKeyFocus(false)
  };
  useEffect(()=>{
    AsyncStorage.getItem('userToken')
    .then(sesToken=>{
      if (sesToken){
        setToken(sesToken)
        seTcreateAccount(false)
        FetchService(props.navigation,"GET","/customer/api/profile")
        .then(response=>{
          setAddress1(response.address1)
          setAddress2(response.address2)
          setFName(response.firstName)
          setLName(response.lastName)
          setPhone(response.phone)
          setEmail(response.email)
        })
        .then(()=>setloading(false))
      }
      else {
        seTcreateAccount(true)
        setloading(false)
      }
    })
    FetchService(props.navigation,"GET","/api/delivery/city/dhaka")
    .then(res=>{
      setDeliveries(res)
      // setDelivery(res[0]._id)
    })
    .catch(er=>console.log("--e",er))

    Keyboard.addListener("keyboardDidShow", _keyboardDidShow);
    Keyboard.addListener("keyboardDidHide", _keyboardDidHide);
    return () => {
      Keyboard.removeListener("keyboardDidShow", _keyboardDidShow);
      Keyboard.removeListener("keyboardDidHide", _keyboardDidHide);
    }
  },[])
  async function handleSubmit() {
    if (firstName.length<1){
      setSignupError(
        language==='en'?
        'insert name'
        :"নাম")
    }
    else if (delivery==-1){
      setSignupError(
        language==='en'?
        'Select delivery area'
        :"এলাকা নির্বাচন করুন")
    }
    else if (address1.length<1){
      setSignupError(
        language==='en'?
        'insert address'
        :"ঠিকানা")
    }
    else if (phone.length<1){
      setSignupError(
        language==='en'?
        'insert phone number'
        :"ফোন নাম্বার আবশ্যক")
    }
    else if (createAccount && (password.length<1 || password2.length<1)){
      setSignupError(
        language==='en'?
        'insert password'
        :"পাসওয়ার্ড আবশ্যক")
    }
    else if (createAccount && password.length<3){
      setSignupError(
        language==='en'?
        'password at least 3 characters'
        :"পাসওয়ার্ড কমপক্ষে তিন ডিজিট")
    }
    else if (createAccount && (password!==password2)){
      setSignupError(
        language==='en'?
        'passwords do not match'
        :"পাসওয়ার্ড সাদৃশ্যপূর্ণ নয়")
    }
    else{
      setloading(true)
      setSignupError('')
      let items=[]
      cart.forEach(cartItem=>items.push({product:cartItem._id,quantity:cartItem.quantity,variation:cartItem.price._id}))

      let body=
      couponValid?
      {
        shippingAddress:{
          firstName,
          lastName,
          country:"Bangladesh",
          city:"Dhaka",
          address1,
          address2:address1,
          phone,
          email,
          zipCode,
          additionalInfo
        },
        password,
        password2,
        createAccount,
        items,
        delivery
      }:{
        shippingAddress:{
          firstName,
          lastName,
          country:"Bangladesh",
          city:"Dhaka",
          address1,
          address2:address1,
          phone,
          email,
          zipCode,
          additionalInfo
        },
        password,
        password2,
        createAccount,
        items,
        delivery,
        coupon
      }
      FetchService(props.navigation,"POST","/api/checkout",3,body,false)
      .then(res=>{
        console.log(res,body)
        if(res.inserted){
          setModalVisible(true)
          setloading(false)
          setTimeout(()=>{
            dispatch(cleanCart())
            setModalVisible(false)
            props.navigation.navigate("Home")
          },2000)
        }
        else throw (Object.entries(res)[0][0]+" : "+Object.entries(res)[0][1])
      })
      .catch(er=>{
        console.log(er)
        // setSignupError(er)
      })
    }

  }
  if (loading)
  return <ActivityIndicator size="large" color="#00163D" style={{
    alignSelf:'center',
    marginTop:'80%'
  }} />
    return (
      <View
      style={loginStyles.loginCont}
      >
        {
          !keyFocus?
          <HeaderCard
            botMarg={window.height/12}
            half
            />
          :<View/>
        }
        
      <KeyboardAvoidingView
        style={{
          backgroundColor:'#FFFFDD',
          width: window.width/1.1,
          height: window.height/1.8,
          alignSelf:'center',
          borderRadius:15
        }}
      >
        <Text
        style={{
          color:'red', 
          alignSelf:'center'            
        }}>
        {signupError}
        </Text>
        <Modal
          animationType="slide"
          transparent={true}
          visible={modalVisible}
        >
          <View
          style={{
            alignSelf:'center',
            backgroundColor: "#F3F3F3",
            borderRadius: 20,
            padding: 35,
            alignItems: "center",
            shadowColor: "#000",
            marginTop:window.height/2.7,
            shadowOffset: {
              width: 0,
              height: 2
            },
            shadowOpacity: 0.25,
            shadowRadius: 3.84,
            elevation: 5
          }}
          >
            <Image
              style={{
                width: window.height / 10,
                height: window.height / 10,
                marginBottom:10
              }}
              source={iconPurchased}
            />
            <Text
            style={{
              marginBottom: 15,
              textAlign: "center"
            }}
            >{
              language==='en'?
            "Purchase successful"
            :"ক্রয় সম্পন্ন হয়েছে"
            }</Text>
          </View>
          
        </Modal>
      <ScrollView
      contentInsetAdjustmentBehavior="automatic"
      >
        
        
          <View
          style={{
            height:window.height/10,
            width:window.width-50,
            alignItems:'center',
            alignSelf:'center',
            justifyContent:'center'
          }}
          >
          {/* title and input starts */}   
          <Text
          style={{
            color:'#00163D',
            fontSize:20,
            alignSelf:'flex-start',
            marginLeft:10
          }}
          >
            {
            language==='en'?
            "Billing Address"
            :"ঠিকানা"}
          </Text>
          </View>
          <View style={loginStyles.signupEmailTop}>
            <View
            style={{
              flexDirection:'row',
              justifyContent:'space-between',
              width: window.width*305/375,
            }}
            >
              <TextInput
                
                placeholderTextColor="#212121"
                placeholder={
                  language==='en'?
                  "First name"
                  :"নামের প্রথম অংশ"
                }
                value={firstName}
                keyboardType="default"
                style={[loginStyles.txtInputSignup,{
                  width: window.width*140/375,
                }]}
                autoFocus={true}
                onChangeText={setFName}
                blurOnSubmit={false}

                />
                <TextInput
                
                placeholderTextColor="#212121"
                placeholder={
                  language==='en'?
                  "Last name"
                :"নামের শেষ অংশ"
              }
                value={lastName}
                keyboardType='default'
                style={[loginStyles.txtInputSignup,{
                  width: window.width*140/375,
                }]}
                onChangeText={setLName}
                blurOnSubmit={false}

                />
            </View>
            <View
            style={{
              flexDirection:'row',
              justifyContent:'space-between',
              width: window.width*305/375,
            }}
            >
              <View>
              <Text
              style={styles.pickerText}
              >{
                language==='en'?
                "City":"শহর"
                }</Text>
                <View
                style={styles.pickerContainer}
                >
                  {
                    // cityLoading?
                    <Text
                    style={styles.pickerStyle}
                    > {
                      language==='en'?
                      "    Dhaka"
                    :"    ঢাকা"
                  }</Text>
                  //   :<Picker
                  //   selectedValue={cityId}
                  //   style={styles.pickerStyle}
                  //   prompt="Select City"
                  //   onValueChange={async (itemValue, itemIndex) =>{
                  //     setCity(itemValue)
                  //     setCityId(itemValue._id)  
                  //   }}>
                  //     {
                  //       cities.map(city=><Picker.Item key={city._id} label={city.name} value={city._id} />)
                  //     }
                  // </Picker>

                  }
                </View>
              </View>
              <View>
              <Text
              style={styles.pickerText}
              >{
              language==='en'?
              "Delivery"
              :"এলাকা"
            }</Text>
                <View
                style={styles.pickerContainer}
                >
                  <Picker
                    selectedValue={delivery}
                    style={styles.pickerStyle}
                    prompt={
                      language==='en'?
                      "Select Delivery"
                    :"এলাকা নির্বাচন করুন"
                  }
                    onValueChange={async (itemValue, itemIndex) =>{
                      setDelivery(itemValue)
                    }}>
                      <Picker.Item key={-1} label={"Select delivery"} value={-1} />
                      {
                        deliveries.map(city=><Picker.Item key={city._id} label={city.name} value={city._id} />)
                      }
                  </Picker>
                </View>
              </View>
            </View>
            <View>
              <TextInput
                
                placeholderTextColor="#212121"
                placeholder={
                  language==='en'?
                  "Address":"ঠিকানা"
                }
                value={address1}
                keyboardType="default"
                style={{
                  ...systemWeights.light,
                  color: '#616161',
                  width: window.width*305/375,
                  height: window.height/19,
                  fontSize: 12,
                  paddingLeft: window.width*15/375,
                  marginVertical: 10,
                  borderRadius:5,
                  backgroundColor:"#fff",
                  elevation:3
              
                }}
                onChangeText={setAddress1}
                blurOnSubmit={false}
                />
            </View>          
            <Text
            style={{
              color:'#00163D',
              fontSize:20,
              alignSelf:'flex-start',
              marginLeft:40,
              marginVertical:10
            }}
            > 
            {
              language==='en'?
              "Contact Info":"যোগাযোগ এর মাধ্যম"
              }
          </Text>
            <View
            style={{
              justifyContent:'space-between',
              width: window.width*305/375,
            }}
            >
              <TextInput
                
                placeholderTextColor="#212121"
                placeholder={
                  language==='en'?
                  "Phone":"ফোন"
                }
                value={phone}
                keyboardType='phone-pad'
                style={[loginStyles.txtInputSignup]}
                onChangeText={setPhone}
                blurOnSubmit={false}

                />
                <TextInput
                
                placeholderTextColor="#212121"
                placeholder={
                  language==='en'?
                  "Email ( Not required )":"ইমেইল ( আবশ্যক নয় )"
                }
                value={email}
                keyboardType='email-address'
                style={[loginStyles.txtInputSignup]}
                onChangeText={setEmail}
                blurOnSubmit={false}

                />
            </View>
            {
              createAccount?
              <View>
              <Text
              style={styles.pickerText}
              >{
              language==='en'?
              "*New account will be created if you are not logged in"
                :"*একাউন্ট না থাকলে নতুন একাউন্ট তৈরি হবে"
            }</Text>
              <View
            style={{
              flexDirection:'row',
              justifyContent:'space-between',
              width: window.width*305/375,
            }}
            >
              <TextInput
                
                placeholderTextColor="#212121"
                placeholder={
                  language==='en'?
                  "New Password":"নতুন পাসওয়ার্ড"
                }
                secureTextEntry={true}
                value={password}
                keyboardType='default'
                style={[loginStyles.txtInputSignup,{
                  width: window.width*140/375,
                }]}
                onChangeText={setPassword}
                blurOnSubmit={false}

                />
                <TextInput
                
                placeholderTextColor="#212121"
                placeholder={
                  language==='en'?
                  "Confirm":"কনফার্ম করুন"
                }
                secureTextEntry={true}
                value={password2}
                keyboardType='default'
                style={[loginStyles.txtInputSignup,{
                  width: window.width*140/375,
                }]}
                onChangeText={setPassword2}
                blurOnSubmit={false}

                />
            </View>
            </View>
            :<View/>
            }
          </View>
          <Text
            style={{
              fontSize:13,
              margin:7,
              marginTop:10
            }}
            >
              <Text
              style={{
                fontWeight:'bold'
              }}
              >{
              language==='en'?
              "Payment Method:"
              :"পেমেন্ট:"
            }</Text> {
            language==='en'?
            "Cash On Delivery"
          :"ক্যাশ অন ডেলিভারি"
          }
            </Text>
          {/* title and input ends */}
          <View
            style={{
              marginVertical:15,
              justifyContent:'space-between',
              width:window.width/2,
              alignItems:'center',
              alignSelf:'flex-end'    
            }}
            >
          <TouchableOpacity
        style={{
          height:window.height/19,
          width:window.width/2.5,
          borderRadius:5,
          backgroundColor:"#FFFE01",
          justifyContent:'center',
          marginBottom:80
        }}
        onPress={handleSubmit}
        >
          <Text
          style={{
            fontSize:15,
            color:'black',
            alignSelf:'center',
            fontWeight:'bold'
          }}
          >
            {
            language==='en'?
            "Place Order":"অর্ডার করুন"
            }
          </Text>
        </TouchableOpacity>
        </View>
        </ScrollView>
        </KeyboardAvoidingView>
        
      </View>
    );
  }

const styles = StyleSheet.create({
  pickerText:{
    fontSize:8,
    alignSelf:'flex-start',
    marginVertical:5
  },
  pickerContainer:{
    backgroundColor:'#fff',
    borderRadius:5,
    height:50,
    elevation:3,
    height: window.height/21,
    width: window.width*140/375
  },
  pickerStyle:{
    height: 20,
    width: window.width/2.5,
    marginTop:5
  }
});

  export default Checkout