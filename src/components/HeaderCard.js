import React, { useEffect, useState } from "react";
import {
    View,
    Dimensions,
    Image
} from "react-native";
import { BaseUrl } from "../env";
import FastImage from "react-native-fast-image";
import FetchService from '../services/FetchService'
import iconMenu from '../assets/icons/iconMenu.png';
import { TouchableOpacity } from "react-native-gesture-handler";

const window = Dimensions.get('window');

export default function HeaderCard (props) {
  const [ logo, setLogo] = useState(BaseUrl+"/images/library/full/136543-lg10.jpg")
  const [ banner, setBanner] = useState(BaseUrl+"/images/library/full/450983-banner.jpg")
  useEffect(()=>{
    FetchService(props.navigation,"GET","/api/component/appBanner")
    .then(res=>setBanner(BaseUrl+res.items[0].image[0].full))
  },[props])

    return (
        <View
        style={{
          marginBottom: props.botMarg,
          height:
          props.half?
          window.height/5
          :window.height/3.3,
          width:window.width,
          borderBottomLeftRadius:20,
          borderBottomRightRadius:20,
        }}
        >
          <FastImage
              style={{
                height:
                props.half?
                window.height/5
                :window.height/3.3,
                width:window.width,
                borderBottomLeftRadius:20,
                borderBottomRightRadius:20,
                resizeMode:'center',
                alignSelf:'center',
                elevation:10
              }}
              source={{uri:banner,
                priority: FastImage.priority.high
            }}
            />
          <View
          style={{
            width:window.width/5,
            height:window.width/5,
            alignSelf:'flex-end',
            position:'absolute',
            top:
            props.half?
            window.height/7
            :window.height/4,
            right:window.width/9,
            borderRadius:10,
            elevation:15,
            justifyContent:'center',
            alignItems:'center',
            backgroundColor:'#FDFEC8'
          }}
          >
            <Image
              style={{
                width: window.width /7,
                height: window.width /5,
                resizeMode:'contain',
            }}
              source={
                {uri:"http://mangshobazar.com/images/logo-app.png",
                  priority: FastImage.priority.high
              }}
            />
          </View>
          {
            props.showMenu?
          <View
          style={{
            width:window.width/6,
            height:window.width/10,
            position:'absolute',
            top:window.width/10,
            left:-4,
            elevation:10,
          }}
          >
          <TouchableOpacity
            onPress={props.menuOpen}
          >
          <Image
              style={{
                width: window.width /5,
                height: window.height /10,
                resizeMode:'cover',
            }}
              source={iconMenu}
            />
          </TouchableOpacity>
        </View>
        :<View/>
          }
          
        </View>
    );
  }

