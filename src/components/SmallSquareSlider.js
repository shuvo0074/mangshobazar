import React from "react";
import {
    FlatList,
    Text,
    TouchableOpacity,
    View,
    Dimensions
} from "react-native";
import { BaseUrl } from "../env";
import FastImage from 'react-native-fast-image'
import {selectLanguage} from '../features/language/languageSlice'
import { useSelector } from 'react-redux';

const window = Dimensions.get('window');

export default function SmallSquareSlider (props) {
    const {recipe,type}=props

    const language = useSelector(selectLanguage);
    return (
        <View
        style={{
          marginTop:50
        }}
        >
          <FlatList
          showsHorizontalScrollIndicator={false}
          numColumns={4}
          data={props.data}
          contentContainerStyle={{
            height: 
            props.data.length<=4?
            window.height/6
            :props.data.length>8?
            window.height/2.15
            :window.height/3.2,
          }}
          renderItem={({ item }) => {
            const rowData = language==='en'?
            {...item}:
            {...item,...item.bn}
            return (
              <TouchableOpacity
                onPress={() => props.navigation.navigate("CategoryDetails",{id:rowData._id,name:rowData.name,recipe,type})}
                style={{
                  width: window.width /5,
                  height: window.width /5,
                  justifyContent: 'space-between',
                  alignItems: 'flex-start',
                  backgroundColor:"#FFFEC7",
                  borderRadius:5,
                  marginTop:25,
                  marginHorizontal:9,
                  elevation:5
              }}
              >
                <View style={{
                  width: window.width /5,
                  height: window.width /5-window.width/15,
                  borderTopLeftRadius:5,
                  borderTopRightRadius:5,
                  overflow: 'hidden',
              }}>
                  <FastImage
                    style={{
                      flex: 1,
                      width: window.width /5-window.width/13,
                      height: window.width /5-window.width/13,
                      resizeMode:'center',
                      alignSelf:'center',
                      borderTopLeftRadius:5,
                      borderTopRightRadius:5,
                  }}
                    source={{
                      priority: FastImage.priority.high,
                      uri:
                      rowData.icon?
                      BaseUrl+rowData.icon:"http://mangshobazar.com/images/logo-app.png"
                  }}
                  />
                </View>
                <View style={{
                  width: window.width /5,
                  height: window.width /15,
                  borderRadius: 5,
                  overflow: 'hidden',
                  justifyContent:'center'
              }}>
                  <Text
                  style={{
                    fontSize:10,
                    fontWeight:'bold',
                    textAlign:'center'
                  }}
                  >
                    {rowData.name}
                  </Text>
                </View>
              </TouchableOpacity>
            );
          }}
          keyExtractor={(item,index) => {
            return item._id.toString()+new Date().getMilliseconds().toString()+index.toString()}}
            />
        </View>
    );
  }

