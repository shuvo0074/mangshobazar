import React, { useEffect } from 'react';
import {
  StyleSheet,
  Dimensions,
  StatusBar,
  View,
  ScrollView,
  Platform
} from 'react-native';
const window = Dimensions.get('window');
import FetchService from '../services/FetchService';

import {
  Colors,
} from 'react-native/Libraries/NewAppScreen';
import VerticalSlider from "../components/VerticalSlider";
import HeaderCard from "../components/HeaderCard";
import SendAnalytics from '../services/SendAnalytics';

const CategoryDetails: () => React$Node = (props) => {
  const {id,name,type}=props.navigation.state.params
  useEffect(()=>{
    SendAnalytics(props.navigation,id,type=="recipe"?"postCategory":type)
  },[props])
  return (
    <>
      <StatusBar barStyle="dark-content" />
      <ScrollView
      style={styles.scrollView}
      >
        <HeaderCard
            botMarg={ window.height/50 }
            />
        <VerticalSlider
        id={id}
        navigation={props.navigation}
        darkText
        type={type}
        limit={15}
        title={name}
        />
        <View
        style={{
          height:window.height/8
        }}
        />
      </ScrollView>
    </>
  );
};

const styles = StyleSheet.create({
  scrollView: {
    paddingBottom:50,
    backgroundColor: "#fff",
    minHeight:window.height
  },
  engine: {
    position: 'absolute',
    right: 0,
  },
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: '600',
    color: Colors.black,
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: '400',
    color: Colors.dark,
  },
  highlight: {
    fontWeight: '700',
  },
  footer: {
    color: Colors.dark,
    fontSize: 12,
    fontWeight: '600',
    padding: 4,
    paddingRight: 12,
    textAlign: 'right',
  },
});

export default CategoryDetails;
