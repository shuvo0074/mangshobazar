import React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  Dimensions,
  StatusBar
} from 'react-native';
const window = Dimensions.get('window');

import {
  Colors,
} from 'react-native/Libraries/NewAppScreen';
import DeliverySlider from "../components/DeliverySlider";
import HeaderCard from "../components/HeaderCard";

const DeliveryList: () => React$Node = ({navigation}) => {
  return (
    <>
      <StatusBar barStyle="dark-content" />
      <SafeAreaView
      style={styles.scrollView}
      >
        <HeaderCard
            half
            botMarg={ window.height/13 }
            />
        <DeliverySlider
        darkText
        navigation={navigation}
        title={"Delivery regions"}
        />
      </SafeAreaView>
    </>
  );
};

const styles = StyleSheet.create({
  scrollView: {
    paddingBottom:50,
    backgroundColor: "#fff",
    minHeight:window.height
  },
  engine: {
    position: 'absolute',
    right: 0,
  },
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: '600',
    color: Colors.black,
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: '400',
    color: Colors.dark,
  },
  highlight: {
    fontWeight: '700',
  },
  footer: {
    color: Colors.dark,
    fontSize: 12,
    fontWeight: '600',
    padding: 4,
    paddingRight: 12,
    textAlign: 'right',
  },
});

export default DeliveryList;
