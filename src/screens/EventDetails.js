import React, { useState, useEffect,useRef } from 'react';
import {
  Alert,
  StyleSheet,
  Dimensions,
  StatusBar,
  View,
  Text,
  Modal,
  ActivityIndicator,
  Image,
  TouchableOpacity,
  Platform
} from 'react-native';
const window = Dimensions.get('window');
import iconCart from '../assets/icons/iconCart.png';
import iconBag from '../assets/icons/iconBag.png';
import SmallH2 from "../components/SmallH2";
import {
  Colors,
} from 'react-native/Libraries/NewAppScreen';
import FastImage from 'react-native-fast-image'
import iconTag from '../assets/icons/iconTag.png';
import iconVariation from '../assets/icons/iconVariation.png';
import FindNumber from "../services/FindNumber";

import { ScrollView } from 'react-native-gesture-handler';
import FetchService from '../services/FetchService';
import { BaseUrl } from '../env';
import HTML from "react-native-render-html";

import {
  addToCart,
  selectCart
} from '../features/counter/counterSlice'
import { useSelector, useDispatch,connect } from 'react-redux';
import {selectLanguage} from '../features/language/languageSlice'
import SendAnalytics from '../services/SendAnalytics';
import iconAdded from '../assets/icons/iconAdded.png';

const EventDetails: () => React$Node = ({navigation}) => {
  const language = useSelector(selectLanguage);
  const cart = useSelector(selectCart);

  const dispatch = useDispatch();
  const [ coverImage, setCoverImage ] = useState("/images/logo-app.png")
  const [ htmlHeight, setHtmlHeight] = useState(false)
  const [loading,setLoading]=useState(true)
  const [modalVisible, setModalVisible] = useState(false);
  const [ quantity,setQuantity ] = useState(1)
  const [data,setData]= useState([])
  const [ variation, setVariation ] = useState({})
  const [ total, setTotal ] = useState(0)
  
  function findItem (items,id) {
    let index= items.findIndex(data=>data._id===id)
    if(index==-1){
      FetchService(navigation,"GET","/api/product/"+id)
      .then(res=>{
        let variationId= res.price.defaultVariation?res.pricing.findIndex(item=>item._id===res.price.defaultVariation):0
        setEventData({
          ...res,
          price:{
            ...res.pricing[variationId].price,
            attribute:res.pricing[variationId].attribute,
            _id:res.pricing[variationId]._id
          },
          quantity:1,
          total:res.pricing[variationId].price.offer ||
                res.pricing[variationId].price.offer !==''?
                parseInt(res.pricing[variationId].price.offer):
                parseInt(res.pricing[variationId].price.regular),
          selectedPrice:res.pricing[variationId].price.offer ||
                res.pricing[variationId].price.offer !==''?
                parseInt(res.pricing[variationId].price.offer):
                parseInt(res.pricing[variationId].price.regular)
        })
        setCoverImage(res.cover.full)
      })
      .then(()=>FetchService(navigation,"GET","/api/product?primaryCategory=1"))
      .then(response=>setData(response.data))
      .then(()=>setLoading(false))
      .catch(err=>console.log(err))
    }
    else {
      setEventData(items[index])
      setLoading(false)
    }
  }
  const _scrollView = useRef()
  useEffect(()=>{
    setLoading(true)
    _scrollView.current?.scrollTo({
      y: 0,
      animated: true,
    });
    const {id} = navigation.state.params
    SendAnalytics(navigation,id,"product")
    findItem(cart.cart,id)
  },[navigation])
  function confirmBooking(){
    dispatch(addToCart(eventData))
    navigation.navigate("Cart")
  }
  function addCartItem(){
    dispatch(addToCart(eventData))
    setModalVisible(true)
    setTimeout(()=>{
      setModalVisible(false)
    },2000)
  }
  const [eventData,setEventData]= useState({})
  const modEventData = language === 'en'? {...eventData}:{...eventData,...eventData.bn}
    if (loading)
    return <ActivityIndicator size="large" color="#00163D" style={{
    alignSelf:'center',
    marginTop:'80%'
  }} />
    return (
    <>
      <StatusBar barStyle="dark-content" />
      <ScrollView
      ref={_scrollView}
      >
        <View
          style={styles.scrollView}>
          <FastImage
          source={{
            priority: FastImage.priority.high,
            uri:
            modEventData.cover?
            BaseUrl+coverImage:"http://mangshobazar.com:5000/images/logo-app.png"
          }}
          style={styles.imgFit}
        />
        <View
        style={{
          width:window.width,
          height:window.height/15,
          justifyContent:'space-evenly',
          flexDirection:'row'
        }}
        >
          {
            modEventData.image.map((im,index)=>
              <TouchableOpacity
              onPress={()=>setCoverImage(modEventData.image[index].full)}
              >
              <FastImage
              key={im.full}
                source={{
                  priority: FastImage.priority.high,
                  uri:
                  BaseUrl+im.full
                }}
                style={{
                  width: window.width/8,
                  height:window.height /16,
                  resizeMode: 'contain',
                  alignSelf:'center',
                }}
              />
              </TouchableOpacity>
              )
          }
          
        </View>

        {/* product details */}
        <View
          style={{
            width:window.width-20,
            height:window.height/7,
            alignSelf:'center',
            padding:15,
          }}
          >
              <Text
              style={{
                fontSize:25,
                color:'black',
                fontWeight:'700',
            }}
              >
                {modEventData.name}
              </Text>
              <Text
              numberOfLines={2}
              style={{
                fontSize:18,
                color:'black',
                fontWeight:'700',
            }}
              >
                ৳ {
                FindNumber(modEventData.selectedPrice,language)
                }<Text
                style={{
                  fontSize:10,
                  fontWeight:'300'
                }}
                >/ {Object.values(modEventData.price.attribute)[0]}</Text>
              </Text>
              
          </View>
          {/* product details */}
          {
            !modEventData.inStock?
            <Text
          style={{
            textAlign:'left',
            fontSize:14,
            color:"red",
            marginLeft:window.width/2.2
          }}
          >
            * Out of Stock
          </Text>
          :<View/>}

          <View
          style={{
            width:window.width,
            height:window.height/15,
            // justifyContent:'center',
            marginVertical:window.height/100,
            paddingHorizontal:window.width/25,
            flexDirection:'row',
          }}
          >
            {/* quantity */}
            <View
            style={{
              height:window.height/18,
              width:window.width/3.5,
              backgroundColor:'#fff',
              borderRadius:window.height/36,
              justifyContent:'center',
              alignItems:'center',
              flexDirection:'row',
              alignSelf:'flex-start',
              elevation:5
            }}>
              <TouchableOpacity
                style={{
                  height:window.height/18-4,
                  margin:2,
                  width:window.height/18-4,
                  borderRadius:window.height/18-4,
                  backgroundColor:'black',
                  alignItems:'center',
                  justifyContent:'center'

                }}
                onPress={()=>{
                  if(eventData.quantity>1){
                    let storeData ={
                    ...eventData,
                    quantity:eventData.quantity-1,
                    total:parseInt(eventData.total)-parseInt(eventData.selectedPrice)
                  }
                  setEventData(storeData)
                }}
              }
              >
                  <Text
                  style={{
                    fontSize:32,
                    fontWeight:'bold',
                    textAlign:'center',
                    color:'#fff',
                    marginBottom:5
                  }}
                  >-</Text>
              </TouchableOpacity>
              <View
                style={{
                  height:window.height/18-4,
                  marginVertical:2,
                  width:window.width/11,
                  alignItems:'center',
                  justifyContent:'center'

                }}
              >
                <Text
                  style={{
                    fontSize:15,
                    fontWeight:'bold',
                    textAlign:'center',
                    color:'black'
                  }}
                >{FindNumber(eventData.quantity,language)}</Text>
              </View>
              <TouchableOpacity
                style={{
                  height:window.height/18-4,
                  margin:2,
                  width:window.height/18-4,
                  borderRadius:window.height/18-4,
                  backgroundColor:'black',
                  alignItems:'center',
                  justifyContent:'center'

                }}
                onPress={()=>{
                  let storeData ={
                    ...eventData,
                    quantity:eventData.quantity+1,
                    total:parseInt(eventData.total)+parseInt(eventData.selectedPrice)
                  }
                  setEventData(storeData)
                }}
              >
                  <Text
                  style={{
                    fontSize:25,
                    fontWeight:'bold',
                    textAlign:'center',
                    color:'#fff',
                    marginBottom:3
                  }}
                  >+</Text>
              </TouchableOpacity>
            </View>
            {/* Quantity */}

            {/* Add to cart */}
            <TouchableOpacity
            style={{
              height:window.height/18,
              width:window.width/4,
              backgroundColor:'#fff',
              borderRadius:window.height/36,
              justifyContent:'space-around',
              alignItems:'center',
              flexDirection:'row',
              paddingHorizontal:10,
              elevation:modEventData.inStock?3:0,
              marginLeft:window.width/8,
            }}
            disabled={!modEventData.inStock}
            onPress={confirmBooking}
            >
              <FastImage
                  style={{
                    width: window.height / 35,
                    height: window.height / 35,
                    alignSelf:'center',
                  }}
                  source={iconBag}
                />
              <Text
              style={{
                fontSize:12,
                fontWeight:'500',
                alignSelf:'center',
                color:'black'
              }}
              >Buy Now</Text>
            </TouchableOpacity>

            <TouchableOpacity
            style={{
              height:window.height/18,
              width:window.width/4,
              backgroundColor:'black',
              borderRadius:window.height/36,
              justifyContent:'space-around',
              alignItems:'center',
              flexDirection:'row',
              paddingHorizontal:10,
              elevation:modEventData.inStock?3:0,
              marginLeft:10,
            }}
              disabled={!modEventData.inStock}
              onPress={addCartItem}
            >
              <FastImage
                  style={styles.footerIcon}
                  source={iconCart}
                />
              <Text
              style={{
                fontSize:12,
                fontWeight:'500',
                alignSelf:'center',
                color:'#fff'
              }}
            >{
              cart.cart.findIndex(cartItem=>cartItem._id===modEventData._id)==-1?
              "Add":"Update"
            }</Text>
            </TouchableOpacity>
            {/* Add to cart */}

          </View>
        
          <Modal
          animationType="slide"
          transparent={true}
          visible={modalVisible}
        >
          <View
          style={{
            alignSelf:'center',
            backgroundColor: "#F3F3F3",
            borderRadius: 20,
            padding: 35,
            alignItems: "center",
            shadowColor: "#000",
            marginTop:window.height/2.7,
            shadowOffset: {
              width: 0,
              height: 2
            },
            shadowOpacity: 0.25,
            shadowRadius: 3.84,
            elevation: 5
          }}
          >
            <Image
              style={{
                width: window.height / 10,
                height: window.height / 10,
                marginBottom:10
              }}
              source={iconAdded}
            />
            <Text
            style={{
              marginBottom: 15,
              textAlign: "center"
            }}
            >{
              language==='en'?
            "Added to cart"
            :"ব্যাগে যোগ হয়েছে"
            }</Text>
          </View>
          
        </Modal>

        <View
          style={{
            width: window.width/8,
            backgroundColor:"#fff",
            position:'absolute',
            top:window.height/6,
            right:0
          }}
          >
            <View
              style={{
                height: window.height/17,
                justifyContent:'center',
                alignItems:'center',
                borderWidth:2
              }}
              >
                <Image
                source={iconVariation}
                style={{
                  height: window.height/22,
                  width: window.height/22,
                  resizeMode:'contain'

                }}
                />
            </View>
            {
              modEventData.pricing.map(vari=>
                <TouchableOpacity
                key={vari._id}
                style={{
                  height: window.height/17,
                  justifyContent:'center',
                  alignItems:'center',
                  borderWidth:2
                }}
                onPress={()=>{
                  let index=cart.cart.findIndex(cartItem=>cartItem._id===eventData._id && cartItem.price._id ===vari._id)
                  if(index == -1){
                    setEventData({
                      ...eventData,
                      price: {
                        ...vari.price,
                        attribute:vari.attribute,
                        _id:vari._id
                      },
                      quantity:1,
                      total: vari.price.offer || vari.price.offer!==""?
                            vari.price.offer:vari.price.regular,
                      selectedPrice: vari.price.offer || vari.price.offer!==""?
                                      parseInt(vari.price.offer):parseInt(vari.price.regular)
                    })
                  }
                  else {
                    setEventData(cart.cart[index])
                  }
                }}
                >
                  <Text
                  style={{
                    alignSelf:'center'
                  }}
                  >
                    {Object.values(vari.attribute)}
                  </Text>
                </TouchableOpacity>
              
                )
            }
            
          </View>
        </View>

        {/* categories */}
        <View
        style={{
          width:window.width,
          height:window.height/15,
          flexDirection:'row',
          alignItems:'flex-end',
          // justifyContent:'space-evenly',
          marginVertical:10
        }}
        >
          {
            modEventData.category.slice(0,4).map((category,index)=>
            <TouchableOpacity
            key={category._id}
              style={{
                width:window.width/5,
                height:window.height/15,
                borderRadius:window.height/50,
                backgroundColor:'white',
                elevation:5,
                justifyContent:'center',
                marginHorizontal:window.width/40
              }}
              onPress={() => navigation.navigate("CategoryDetails",{id:category._id,name:category.name,recipe:false,type:"category"})}
              >
                <Text
                style={{
                  textAlign:'center',
                  fontSize:12,
                  margin:5
                }}
                key={index}
                numberOfLines={2}
                >
                  {
                  language==='en'?
                  category.name
                  :category.bn.name
                }
                </Text>
              </TouchableOpacity>)
          }
          

        </View>
        {/* categories */}

        {/* Description */}
        <Text
        style={{
          fontSize:16,
          fontWeight:'bold',
          marginVertical:10,
          color:'black',
          marginLeft:window.width/17
        }}
        >{
          language==='en'?
          "Descriptions":
          "বর্ণনা"
        }
        </Text>
        {/* <TouchableOpacity
        style={{
          alignSelf:'center',
          justifyContent:'center',
        }}
        onPress={()=>setHtmlHeight(!htmlHeight)}
        > */}
        <HTML
          html={modEventData.description}
          containerStyle={{
            width:window.width/1.2,
            alignSelf:'center',
            justifyContent:'center'
          }}
        />
        {/* </TouchableOpacity> */}
        {/* Description */}

        {/* Tags */}
        {
          modEventData.tags && modEventData.tags.length>0?
          <View
        style={{
          width:window.width/1.1,
          height:window.height/18,
          flexDirection:'row',
          alignItems:'flex-end',
          alignSelf:'center',

        }}
        >
          <Image
          source={iconTag}
          style={{
            width: window.height / 35,
            height: window.height / 35,
            alignSelf:'center',
            marginRight:window.width/40
          }}
          />
          {
            modEventData.tags.slice(0,4).map(tag=>
              <Text
              key={tag._id}
          style={{
            color:'black',
            alignSelf:'center',
            marginRight:10
          }}
          onPress={() => navigation.navigate("CategoryDetails",{id:tag._id,name:tag.name,type:"tag"})}
          >{
          language==='en'?
          tag.name
          :tag.bn.name
        }</Text>
          )
          }
        </View>
        :<View/>
        }
        
        {/* Tags */}

        <SmallH2
          data={data}
          limit={6}
          navigation={navigation}
          darkText
          title={
            language==='en'?
            "Related products":
            "সমজাতীয় পণ্য"
          }
        />
        {/* Unit */}
      </ScrollView>
    </>
  );
};

const styles = StyleSheet.create({
  slideShow: {
    width: window.width,
    height: window.height /2.4,
    justifyContent: 'center',
    alignItems: 'center',
  },
  slideHolder: {
      width: window.width,
      height: window.height /1.9,
      marginBottom:15
  },
  imgFit: {
      width: window.width,
      height:window.height /2,
      resizeMode: 'cover',
      alignSelf:'center',
      marginBottom:20
  },
  indicatorHolder: {
      width: window.width * 42 / 375,
      flexDirection: 'row',
      justifyContent: 'space-around',
      alignItems: 'center',
      alignSelf: 'center',
      position: 'absolute',
      bottom: 10,
      marginHorizontal: 'auto'
  },
  indicator: {
      width: window.width * 4 / 375,
      height: window.width * 4 / 375,
      borderRadius: window.width * 2 / 375,
      backgroundColor: '#fff'
    },
  indicatorSelected: {
      width: window.width * 12 / 375,
      height: window.width * 4 / 375,
      borderRadius: window.width * 2 / 375,
      backgroundColor: '#fff'
  },
  sliderLargeText: {
      fontSize:20,
      color:'white',
      fontWeight:'700'
  },
  sliderTextAbsolute: {
      // position:"absolute",
      color:'#00163D',
      // left:window.width/10,
      
  },
  scrollView: {
    backgroundColor: "#FFF4C7",
    // marginBottom:window.height/4,
    borderBottomLeftRadius:25,
    borderBottomRightRadius:25,
    elevation:5
  },
  engine: {
    position: 'absolute',
    right: 0,
  },
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: '600',
    color: Colors.black,
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: '400',
    color: Colors.dark,
  },
  highlight: {
    fontWeight: '700',
  },
  footer: {
    fontSize: 11,
    fontWeight: '600',
  },
  centeredView: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    marginTop: 22
  },
  modalView: {
    margin: 20,
    backgroundColor: "white",
    borderRadius: 20,
    padding: 35,
    alignItems: "center",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5
  },
  openButton: {
    backgroundColor: "#F194FF",
    borderRadius: 20,
    padding: 10,
    elevation: 2
  },
  textStyle: {
    color: "white",
    fontWeight: "bold",
    textAlign: "center"
  },
  modalText: {
    marginBottom: 15,
    textAlign: "center"
  },
  footerIcon: {
		width: window.height / 30,
    height: window.height / 30,
    alignSelf:'center',
    marginTop:4
	},
});

export default connect()(EventDetails);
