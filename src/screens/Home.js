import React, { useState, useEffect } from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  StatusBar,
  ActivityIndicator,
  Dimensions,
  View,
  Text,
  TouchableOpacity,
  Image,
  Linking
} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
const window = Dimensions.get('window');
import FastImage from 'react-native-fast-image'
import SideMenu from 'react-native-side-menu';
import { BaseUrl } from "../env";
import iconSwitch from '../assets/icons/iconSwitch.png';
import iconMenu from '../assets/icons/iconMenu.png';
import {
  setBn,setEn, selectLanguage
} from '../features/language/languageSlice'
import { useSelector, useDispatch, connect } from 'react-redux';

import {
  Colors,
} from 'react-native/Libraries/NewAppScreen';
import SmallH2 from '../components/SmallH2'
import SmallSquareSlider from "../components/SmallSquareSlider";
import HeaderCard from "../components/HeaderCard";

import FetchService from '../services/FetchService';
const Home: () => React$Node = ({navigation}) => {
  const language = useSelector(selectLanguage);
  const dispatch = useDispatch();
  const [ sesToken , setSesToken ] = useState(null)
  const [ categories, setCategories]=useState([])
  const [loading, setLoading] = useState(true)
  const [ isOpen, setIsOpen ] = useState(false)
  const [ offerData , setOfferData ] = useState([])
  const [ phone , setPhone ] = useState('')
  useEffect(()=>{
    AsyncStorage.getItem('userToken')
    .then(ses=>{
      // console.log(ses)
      if (ses){
        setSesToken(ses)
      }
    })
    FetchService(navigation,"GET","/api/category")
    .then(res=>setCategories(res.data))
    .then(()=>FetchService(navigation,"GET","/api/product"))
    .then(response=>setData(response.data))
    .then(()=>FetchService(navigation,"GET","/api/product/offer"))
    .then(response=>setOfferData(response.data))
    .then(()=> FetchService(navigation,"GET","/api/component/phone"))
    .then(res=>setPhone(res.items[0]))
    .then(()=>setLoading(false))
    .catch(err=>console.log(err))
  },[])
  const [data,setData]= useState([])
  const menu = 
  <View
    style={{
      height:window.height,
      marginLeft:window.width/15
    }}
  >
    <TouchableOpacity

    style={{
      marginVertical:7,
      marginTop:window.height/15
    }}
    onPress={()=>navigation.navigate("AllRecipe")}
    >
      <Text
      style={{
        fontSize:15,
        fontWeight:'bold'
      }}
      >{
      language==='en'?
      "Recipe"
      :"রেসিপি"
      }</Text>
    </TouchableOpacity>
    <TouchableOpacity
    style={{
      marginVertical:7
    }}
    onPress={()=>navigation.navigate("CardDetails",{data:offerData,discount:true})}
    >
      <Text
      style={{
        fontSize:15,
        fontWeight:'bold'
      }}
      >{
      language==='en'?
      "Offers"
      :"অফার"
    }</Text>
    </TouchableOpacity>
    {
      !sesToken?
      <TouchableOpacity

    style={{
      marginVertical:7
    }}
    onPress={()=>navigation.navigate("AuthStack")}
    >
      <Text
      style={{
        fontSize:15,
        fontWeight:'bold'
      }}
      >{
      language==='en'?
      "Login"
      :"লগ ইন"
      }  </Text>
    </TouchableOpacity>
  :<View/>  
  }
  <TouchableOpacity
    style={{
      marginVertical:7,
      borderBottomWidth:2,
      paddingBottom:10,
      flexDirection:'row',
      alignItems:'center'
    }}
    onPress={()=>
      language==='en'?
      dispatch(setBn())
      :dispatch(setEn())
    }
    >
      <Image
					style={{
            width: 20,
            height: 20,
            marginRight:10
          }}
					source={iconSwitch}
				/>
    {
      language==='en'?
      <Text
      style={{
        fontSize:15
      }}
    >
    <Text
    style={{
      color:'red'
    }}
    >En/</Text>বাংলা</Text>:
    <Text
      style={{
        fontSize:15
      }}
    >En/
    <Text
    style={{
      color:'red'
    }}
    >বাংলা</Text></Text>
    }
    </TouchableOpacity>
  {

    categories.map(cate=>{
      const cat = language==='en'?
          {...cate}:
          {...cate,...cate.bn}
      return(
        <TouchableOpacity
        style={{
          marginVertical:7,
          flexDirection:'row'
        }}
        onPress={()=>navigation.navigate("CategoryDetails",{id:cat._id,name:cat.name,recipe:false,type:"category"})}
        >
        <FastImage
          style={{
            width: 20,
            height: 20,
            resizeMode:'contain',
            marginRight:10,
            borderRadius:5
          }}
          source={{uri:
            cat.icon?
            BaseUrl+cat.icon:"http://mangshobazar.com:5000/images/logo-app.png",
            priority: FastImage.priority.high
        }}
        />
          <Text
          style={{
            fontSize:15,
            fontWeight:'bold'
          }}
          >{cat.name}</Text>
        </TouchableOpacity>
      )
  }
  )
  }

    <TouchableOpacity
    style={{
      marginTop:20,
      flexDirection:'row',
      borderColor:'grey',
      borderRadius:5,
      paddingVertical:8,
      borderWidth:1,
      alignItems:'center',
      justifyContent:'center',
      marginRight:20,

    }}
    onPress={()=>Linking.openURL(`tel:${phone.target}`)
  }
    >
    <FastImage
      style={{
        width: 20,
        height: 20,
        resizeMode:'contain',
        marginRight:10,
        borderRadius:5,
      }}
      source={{uri:phone.image && phone.image.length?
        BaseUrl+phone.image[0].icon:"http://mangshobazar.com:5000/images/logo-app.png",
        priority: FastImage.priority.high
    }}
    />
      <Text
      style={{
        fontSize:15,
        fontWeight:'bold'
      }}
      >Call now</Text>
    </TouchableOpacity>
    </View>
  if (loading)
  return <ActivityIndicator size="large" color="#00163D" style={{
    alignSelf:'center',
    marginTop:'80%'
  }} />
  return (
    <SideMenu
    // onSliding={d=>console.log(d)}
    menu={menu}
    isOpen={isOpen}
    >
    <>
      <StatusBar barStyle="dark-content" />
      <SafeAreaView>
        <ScrollView
          contentInsetAdjustmentBehavior="automatic"
          style={styles.scrollView}>
            <HeaderCard
            botMarg={0}
            menuOpen={()=>setIsOpen(!isOpen)}
            isOpen={isOpen}
            // showMenu
            />
            <SmallSquareSlider
            data={categories}
            navigation={navigation}
            recipe={false}
            type="category"
            />
            <SmallH2
            data={offerData}
            navigation={navigation}
            limit={6}
            discount={true}
            darkText
            viewMore
            title={
              language==='en'?
              "Latest offers"
            :"নতুন অফার"}
            />
            <SmallH2
            data={data}
            navigation={navigation}
            darkText
            limit={6}
            discount={false}
            viewMore
            title={
              language==='en'?
              "Popular products":"জনপ্রিয় পণ্য"
            }
            />
            <View
            style={{
              height:window.height/25
            }}
            />
          
        </ScrollView>
        <TouchableOpacity
        style={{
          width:window.width/6,
          height:window.height/9,
          position:'absolute',
          top:window.width/6,
          left:-4,
        }}
          onPress={()=>setIsOpen(!isOpen)}
        >
        <Image
            style={{
              width: window.width /5,
              height: window.height /10,
              resizeMode:'cover',
          }}
            source={iconMenu}
          />
      </TouchableOpacity>
      </SafeAreaView>
    </>
    </SideMenu>
  );
};

const styles = StyleSheet.create({
  scrollView: {
    backgroundColor:"#FFF"
  },
  engine: {
    position: 'absolute',
    right: 0,
  },
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: '600',
    color: Colors.black,
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: '400',
    color: Colors.dark,
  },
  highlight: {
    fontWeight: '700',
  },
  footer: {
    color: Colors.dark,
    fontSize: 12,
    fontWeight: '600',
    padding: 4,
    paddingRight: 12,
    textAlign: 'right',
  },
});

export default Home;
