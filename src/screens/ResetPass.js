import React, { useState, useEffect } from 'react';
import {
    View,
    TextInput,
    Dimensions,
    StatusBar,
    Text,
    TouchableOpacity,
    KeyboardAvoidingView,
    Keyboard
} from 'react-native';
import { useSelector } from 'react-redux';
import {selectLanguage} from '../features/language/languageSlice'
import FetchService from '../services/FetchService';
import HeaderCard from '../components/HeaderCard'

const window = Dimensions.get('window');
const ResetPass: () => React$Node = (props) => {
  const [keyFocus,setKeyFocus]=useState(true)
  const language = useSelector(selectLanguage);
  const {details}=props.navigation.state.params
  const [resetToken,setResetToken ] = useState('')
  const [ errorText,setErrorText] = useState('')

  const _keyboardDidShow = () => {
    setKeyFocus(true)
  };

  const _keyboardDidHide = () => {
    setKeyFocus(false)
  };
  useEffect(()=>{
    Keyboard.addListener("keyboardDidShow", _keyboardDidShow);
    Keyboard.addListener("keyboardDidHide", _keyboardDidHide);
    return () => {
      Keyboard.removeListener("keyboardDidShow", _keyboardDidShow);
      Keyboard.removeListener("keyboardDidHide", _keyboardDidHide);
    }
  },[])
  function handleValidate(){
    let body={
      resetToken
    }
    FetchService(props.navigation,"POST","/customer/auth/recover/validate",1,body,false)
    .then(res=>{
      if(res.success){
        setResetToken('')
        props.navigation.navigate("NewPassword",{resetToken})
      }
      else {
        setErrorText(Object.values(res)[0])
      }
    })
  }

  return (
    <>
      <StatusBar barStyle="dark-content" />
      <View
      style={{
          height:window.height,
          width:window.width,
          alignItems:'center',
          backgroundColor:'#FDFEC8'
        }}
      >
        {
          !keyFocus?<HeaderCard
          botMarg={0}
          isOpen={false}
          />:<View/>
        }
        <KeyboardAvoidingView
          style={{
            alignSelf:'center',
            backgroundColor: "#F3F3F3",
            borderRadius: 20,
            padding: 35,
            alignItems: "center",
            shadowColor: "#000",
            marginTop:keyFocus?window.height/25: window.height/10,
            shadowOffset: {
              width: 0,
              height: 2
            },
            shadowOpacity: 0.25,
            shadowRadius: 3.84,
            elevation: 5
          }}
          >
            <Text
            style={{
              fontSize:20,
              marginBottom:20
            }}
            >{details.customer.firstName}{' '}{details.customer.lastName} </Text>
            <Text
            style={{
              alignSelf:'center'
            }}
            >
              A 6 digit code is sent to: <Text
              style={{
                fontWeight:'bold',
                alignSelf:'center',
                textAlign:'center'
              }}
            >{details.email?details.email+ " and":''} {details.phone?details.phone:''}</Text> 
            </Text>
            <Text
            style={{
              color:'#777',
              alignSelf:'flex-start',
              marginTop:20
            }}
            >
              Please enter the code below
            </Text>
            <TextInput
              placeholderTextColor="#777"
              keyboardType='number-pad'
              maxLength={6}
              placeholder={
              language==='en'?
                "######"
                :"######"
              }
              value={resetToken}
              style={[ {
                color: '#333',
                width: window.width*305/375,
                height: window.height/14,
                fontSize: 20,
                letterSpacing:window.width/10,
                marginBottom: 10,
                borderRadius:5,
                borderColor:'#666',
                borderWidth:1,
            },{
                marginTop:10
              } ]}
              onChangeText={setResetToken}
              blurOnSubmit={false}
              />
              <Text
          style={{
            color:'red',
            alignSelf:'flex-start',
            marginBottom: 10,
          }}
          >{errorText}</Text>
              <TouchableOpacity
                style={{
                  height:window.height/15,
                  width:window.width/2.5,
                  borderRadius:5,
                  backgroundColor:"#FFFE01",
                  justifyContent:'center',
                  marginBottom:10,
                  borderWidth:1,
                  borderColor:"#ccc"
                }}
                onPress={handleValidate}
                >
                  <Text
                  style={{
                    fontSize:15,
                    color:'black',
                    alignSelf:'center'
                  }}
                  >
                    {
                    language=='en'?
                    "Continue"
                    :"পরবর্তী"
                  }
                  </Text>
                </TouchableOpacity>
            
          </KeyboardAvoidingView>
      </View>
    </>
  );
};

export default ResetPass;
