import React, { useState, useEffect } from 'react';
import {
  StyleSheet,
  ScrollView,
  StatusBar,
  Text,
  Dimensions,
  View
} from 'react-native';
const window = Dimensions.get('window');
import logoNogod from '../assets/images/logoNogod.png';
import logoBkash from '../assets/images/logoBkash.png';
import logoDelivery from '../assets/images/logoDelivery.png';
import logoRocket from '../assets/images/logoRocket.png';
import FastImage from "react-native-fast-image"
import {
  Colors,
} from 'react-native/Libraries/NewAppScreen';
import HeaderCard from "../components/HeaderCard";

import FetchService from '../services/FetchService';
import { TouchableOpacity } from 'react-native-gesture-handler';

const Shipping: () => React$Node = ({navigation}) => {
  useEffect(()=>{
    // FetchService("GET","/api/category")
    // .then(async res=>setData(res.data))
    // .then(()=>setLoading(false))

  },[navigation])


  return (
    <>
      <StatusBar barStyle="dark-content" />
        <ScrollView
          style={styles.scrollView}>
            <HeaderCard
            half
            botMarg={ window.height/9 }
            />
            <View
              style={{
                backgroundColor:'#FFFFDD',
                width: window.width/1.1,
                height: window.height/1.9,
                alignSelf:'center',
                borderRadius:15,
                justifyContent:'space-evenly',
                padding:20
              }}
              >
                
                <View
                style={{
                  flexDirection:'row',
                  justifyContent:'space-evenly'
                }}
                >
                  <View
                  style={{
                    height:window.width/3.5,
                    width:window.width/3.5,
                    backgroundColor:'#fff',
                    borderRadius:10,
                    alignItems:'center',
                    justifyContent:'center',
                    elevation:5,
                  }}
                  >
                    <TouchableOpacity
                    onPress={()=>navigation.navigate("PayDetails",{id:1})}
                    >
                    <FastImage
                      style={{
                        width: window.height / 10,
                        height: window.height / 10,
                      }}
                      source={logoBkash}
                    />
                    </TouchableOpacity>
                  </View>

                  <View
                  style={{
                    height:window.width/3.5,
                    width:window.width/3.5,
                    backgroundColor:'#fff',
                    borderRadius:10,
                    alignItems:'center',
                    justifyContent:'center',
                    elevation:5
                  }}
                  >
                    <TouchableOpacity
                    onPress={()=>navigation.navigate("PayDetails",{id:2})}
                    >
                    <FastImage
                      style={{
                        width: window.height / 10,
                        height: window.height / 10,
                      }}
                      source={logoNogod}
                    />
                    </TouchableOpacity>
                  </View>
                  
                </View>

                <View
                style={{
                  flexDirection:'row',
                  justifyContent:'space-evenly'
                }}
                >
                  <View
                  style={{
                    height:window.width/3.5,
                    width:window.width/3.5,
                    backgroundColor:'#fff',
                    borderRadius:10,
                    alignItems:'center',
                    justifyContent:'center',
                    elevation:5
                  }}
                  >
                    <TouchableOpacity
                    onPress={()=>navigation.navigate("PayDetails",{id:3})}
                    >
                    <FastImage
                      style={{
                        width: window.height / 10,
                        height: window.height / 10,
                      }}
                      source={logoRocket}
                    />
                    </TouchableOpacity>
                  </View>

                  <View
                  style={{
                    height:window.width/3.5,
                    width:window.width/3.5,
                    backgroundColor:'#fff',
                    borderRadius:10,
                    alignItems:'center',
                    justifyContent:'center',
                    elevation:5
                  }}
                  >
                    <TouchableOpacity
                    onPress={()=>navigation.navigate("PayDetails",{id:4})}
                    >
                    <FastImage
                      style={{
                        width: window.height / 10,
                        height: window.height / 10,
                      }}
                      source={logoDelivery}
                    />
                    </TouchableOpacity>
                  </View>
                  
                </View>
                
              </View>
            
            
        </ScrollView>
    </>
  );
};

const styles = StyleSheet.create({
  scrollView: {
    backgroundColor:"#FFF"
  },
  engine: {
    position: 'absolute',
    right: 0,
  },
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: '600',
    color: Colors.black,
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: '400',
    color: Colors.dark,
  },
  highlight: {
    fontWeight: '700',
  },
  footer: {
    color: Colors.dark,
    fontSize: 12,
    fontWeight: '600',
    padding: 4,
    paddingRight: 12,
    textAlign: 'right',
  },
});

export default Shipping;
