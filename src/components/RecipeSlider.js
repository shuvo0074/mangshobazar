import React, { useEffect } from "react";
import {
    FlatList,
    Text,
    TouchableOpacity,
    View,
    Dimensions,
    Image
} from "react-native";
import { BaseUrl } from "../env";
import FastImage from 'react-native-fast-image'
import iconPerson from '../assets/icons/iconPerson.png';
import iconTime from '../assets/icons/iconTime.png';
import {selectLanguage} from "../features/language/languageSlice";
import { useSelector, connect } from 'react-redux';

const window = Dimensions.get('window');

export default function RecipeSlider (props) {
  const language = useSelector(selectLanguage);
  const {data,navigation,title,darkText,limit,viewMore,discount,recipe}=props
  useEffect(()=>{},[])
  function _handlePress(id) {
    navigation.navigate("RecipeDetails",{id})
  }
    if(data.length<1 && limit==6)
    return <View/>
    return (
        <View
        style={{
          marginTop:20
        }}
        >
          <View
          style={{
            flexDirection:'row',
            height:window.height/17,
            justifyContent:'space-between',
          }}
          >
            <Text
              style={{
                fontWeight:'bold',
                fontSize:15,
                color:'#FADC62',
                marginLeft: 20,
                color: 'black'
              }}
              >
                {
                data.length>0?
                  title
                  :language==='en'?
                  `No ${title} available`
                  :`${title} অবশিষ্ট নেই`
                }
              </Text>
          </View>
          <FlatList
          numColumns={2}
          showsHorizontalScrollIndicator={false}
          data={data.slice(0,limit)}
          contentContainerStyle={{
            alignSelf:'center'
          }}
          renderItem={({ item }) => {
            const rowData = language==='en'?
            {...item}:
            {...item,...item.bn}
            return (
              <TouchableOpacity
                onPress={() => _handlePress(rowData._id)}
                style={{
                  width: window.width /2.4,
                  height: window.height / 3,
                  marginHorizontal: 7,
                  backgroundColor:"#F1F1F1",
                  borderRadius:8,
                  elevation:5,
                  marginVertical:15,
                  marginHorizontal:8
                }}
              >
                <View style={{
                    width: window.width /2.4,
                    height: window.height /3,
                    overflow: 'hidden'
                }}>
                  <FastImage
                    style={{
                      flex: 1,
                      width: undefined,
                      height: undefined,
                      resizeMode:'cover',
                      borderRadius:8,
                      margin:1
                    }}
                    source={{uri:
                      rowData.cover?
                      BaseUrl+rowData.cover.full:"http://mangshobazar.com:5000/images/logo-app.png",
                      priority: FastImage.priority.high
                  }}
                  />
                  {/* pricing/offer  tag */}
                      <View
                      style={{
                        width:window.width /2.4,
                        height:window.height/20,
                        // backgroundColor:'red',
                        position:'absolute',
                        // top:7,
                        left:0,
                        justifyContent:'space-between',
                        flexDirection:'row',
                        alignItems:'center'
                      }}
                      >
                        {/* time tag */}
                      <View
                      style={{
                        backgroundColor:'#fff',
                        // width:window.width/12,
                        height: window.width/30,
                        borderRadius:window.width/60,
                        flexDirection:'row',
                        elevation:3,
                        marginLeft:window.width/30,
                        paddingRight:2
                      }}
                      >
                        <View
                        style={{
                          height:window.width/28,
                          width:window.width/28,
                          borderRadius:window.width/60,
                          backgroundColor:'black',
                          justifyContent:'center',
                          alignItems:'center'
                        }}
                        >
                          <Image
                            style={{
                              width: window.height / 90,
                              height: window.height / 90,                          
                            }}
                            source={iconTime}
                          />
                        </View>
                        <View
                        style={{
                          height:window.width/30,
                          // width:window.width/20,
                          justifyContent:'center',
                        }}
                        >
                          <Text
                          style={{
                            fontSize:5,
                            color:'black',
                          }}
                        >{rowData.preparationTime}</Text>
                        </View>
                        </View>
                        {/* time tag */}
                        {/* person tag */}
                        <View
                      style={{
                        backgroundColor:'#fff',
                        // width:window.width/15,
                        height: window.width/30,
                        borderRadius:window.width/60,
                        flexDirection:'row',
                        elevation:3,
                        marginRight:window.width/30,
                        paddingRight:2
                      }}
                      >
                        <View
                        style={{
                          height:window.width/28,
                          width:window.width/28,
                          borderRadius:window.width/60,
                          backgroundColor:'black',
                          justifyContent:'center',
                          alignItems:'center'
                        }}
                        >
                          <Image
                            style={{
                              width: window.height / 100,
                              height: window.height / 100,                          
                            }}
                            source={iconPerson}
                          />
                        </View>
                        <View
                        style={{
                          height:window.width/30,
                          // width:window.width/30,
                          justifyContent:'center',
                          alignItems:'center'
                        }}
                        >
                          <Text
                          style={{
                            fontSize:5,
                            color:'black',
                            textAlign:'center'
                          }}
                        >{rowData.servingSize}</Text>
                        </View>
                        </View>
                        {/* person tag */}
                      </View>
                      
                  
                  {/* pricing/offer  tag */}
                </View>
                <View style={{
                  width: window.width /2.4,
                  height: window.height / 12,
                  justifyContent: 'center',
                  alignItems:'flex-start',
                  paddingLeft:10,
                  backgroundColor:'#F1F1F1',
                  position:'absolute',
                  bottom:0,
                  borderBottomLeftRadius:8,
                  borderBottomRightRadius:8,
                  borderTopLeftRadius:22,
                  borderTopRightRadius:22

              }}>
                <Text style={{
                  textAlign: 'left',
                  fontWeight:'bold',
                  fontSize: 10,
                  color: '#000'}}
                  numberOfLines={1}
                  >
                {rowData.name}
                </Text>
                {/* <Text style={{
                  textAlign: 'left',
                  fontSize: 8,
                  color: '#000'}}
                  numberOfLines={1}
                  >
                {rowData.primaryCategory.name}
                </Text> */}
              </View>
              </TouchableOpacity>
            );
          }}
          keyExtractor={(item,index) => {
            return item._id.toString()+new Date().getMilliseconds().toString()+index.toString()}}
            />
        </View>
    );
  }

