import React, {useEffect} from 'react';
import {
  StyleSheet,
  StatusBar,
  Dimensions,
  View,
  Text,
  Image,
  BackHandler
} from 'react-native';
const window = Dimensions.get('window');

import ErrorSign from "../assets/images/ErrorSign.png";
const ErrorLanding: () => React$Node = ({navigation}) => {

  useEffect(()=>{
    const backHandler = BackHandler.addEventListener(
      "hardwareBackPress",
      ()=>navigation.navigate("HomeStack")
    );

    return () => {
      backHandler.remove();
    }
  },[])

  const {errorText} = navigation.state.params
  return (
    <>
      <StatusBar barStyle="dark-content" />
      <View
      style={styles.scrollView}
      >
        <Image
        source={ErrorSign}
        style={{
          height:window.height/2,
          width:window.width/1.5,
          resizeMode:'contain'
        }}
        />
        <Text
        style={{
          fontSize:15,
          fontWeight:'bold',
          marginHorizontal:window.width/10
        }}
        >
          {errorText}
      {'\n'}Please Try again later
        </Text>
      </View>
    </>
  );
};

const styles = StyleSheet.create({
  scrollView: {
    backgroundColor:"#FFF4C7",
    justifyContent:'center',
    alignItems:'center',
    height:window.height,
    width:window.width
  }
});

export default ErrorLanding;
