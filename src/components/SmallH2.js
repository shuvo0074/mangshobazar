import React, { useEffect } from "react";
import {
    FlatList,
    Text,
    TouchableOpacity,
    View,
    Dimensions,
    Image
} from "react-native";
import { BaseUrl } from "../env";
import FastImage from 'react-native-fast-image'
import iconPerson from '../assets/icons/iconPerson.png';
import iconTime from '../assets/icons/iconTime.png';
import {selectLanguage} from '../features/language/languageSlice'
import { useSelector } from 'react-redux';
import FindNumber from "../services/FindNumber";

const window = Dimensions.get('window');

export default function SmallH2 (props) {
  const language = useSelector(selectLanguage);
  const {data,navigation,title,darkText,limit,viewMore,discount,recipe,cart}=props
  useEffect(()=>{},[])
  function _handlePress(id) {
    recipe?
    navigation.navigate("RecipeDetails",{id}):
    navigation.navigate("EventDetails",{id})
  }

    return (
        <View
        style={{
          marginTop:20
        }}
        >
          <View
          style={{
            flexDirection:'row',
            height:window.height/17,
            justifyContent:'space-between',
          }}
          >

        <Text
        style={{
          fontWeight:'bold',
          fontSize:15,
          color:'#FADC62',
          marginLeft: 20,
          color: darkText? '#00102D':'#FADC62',
        }}
        >
          {
            data.length>0?
              title
              :language==='en'?
              `No ${title} available`
              :`${title} অবশিষ্ট নেই`
            }
        </Text>
        {
          viewMore && data.length>limit?
          <Text
            style={{
              marginRight:20,
              fontSize:10,
              fontWeight:'bold',
              marginTop:10
            }}
            onPress={()=>recipe?navigation.navigate("AllRecipe"): navigation.navigate("CardDetails",{data,discount})}
            >
              {
              language==='en'?
              "See all":"সব দেখুন"
              }
            </Text>:<View/>
              }
          </View>
          <FlatList
          numColumns={3}
          showsHorizontalScrollIndicator={false}
          data={data.slice(0,limit)}
          renderItem={({ item }) => {
            const rowData = language==='en'?
            {...item}:
            {...item,...item.bn}
            return (
              <TouchableOpacity
                onPress={() => _handlePress(rowData._id)}
                style={{
                  width: window.width /3.4,
                  height: window.height / 4,
                  justifyContent: 'center',
                  marginHorizontal: 7,
                  backgroundColor:"#F1F1F1",
                  borderRadius:8,
                  elevation:5,
                  marginVertical:10
                }}
              >
                <View style={{
                    width: window.width /3.4,
                    height: window.height /5,
                    overflow: 'hidden'
                }}>
                  <FastImage
                    style={{
                      flex: 1,
                      width: undefined,
                      height: undefined,
                      resizeMode:'cover',
                      borderTopLeftRadius:8,
                      borderTopRightRadius:8,
                      margin:1
                    }}
                    source={{uri:
                      rowData.cover?
                      BaseUrl+rowData.cover.full:"http://mangshobazar.com:5000/images/logo-app.png",
                      priority: FastImage.priority.high
                  }}
                  />
                  {/* pricing/offer  tag */}
                  {
                    discount?
                    <View
                      style={{
                        height: window.width /13,
                        width: window.width /13,
                        borderRadius: window.width /26,
                        position:'absolute',
                        top: window.width /50,
                        left: window.width /50,
                        backgroundColor:'#FFFEC7',
                        justifyContent:'center',
                        alignItems:'center',
                        padding:2,
                        elevation:2
                      }}
                      >
                        <Text
                        numberOfLines={2}
                        style={{
                          textAlign:'center',
                          fontSize:8,
                          fontWeight:'bold'
                        }}
                        > ৳ {rowData.price.offer?
                          FindNumber((parseInt(rowData.price.regular) - parseInt(rowData.price.offer)),language):0
                        } {'\n'} {
                        language=='en'?
                        "off"
                        :"ছাড়"
                      } </Text>
                      </View>
                      :recipe?
                      <View
                      style={{
                        width:window.width /3.4,
                        height:window.height/20,
                        // backgroundColor:'red',
                        position:'absolute',
                        // top:7,
                        left:0,
                        justifyContent:'space-between',
                        flexDirection:'row',
                        alignItems:'center'
                      }}
                      >
                        {/* time tag */}
                      <View
                      style={{
                        backgroundColor:'#fff',
                        width:window.width/12,
                        height: window.width/30,
                        borderRadius:window.width/60,
                        flexDirection:'row',
                        elevation:3,
                        marginLeft:window.width/50,
                      }}
                      >
                        <View
                        style={{
                          height:window.width/28,
                          width:window.width/28,
                          borderRadius:window.width/60,
                          backgroundColor:'black',
                          justifyContent:'center',
                          alignItems:'center'
                        }}
                        >
                          <Image
                            style={{
                              width: window.height / 90,
                              height: window.height / 90,                          
                            }}
                            source={iconTime}
                          />
                        </View>
                        <View
                        style={{
                          height:window.width/30,
                          width:window.width/20,
                          justifyContent:'center',
                        }}
                        >
                          <Text
                          style={{
                            fontSize:5,
                            color:'black',
                          }}
                        >25 Min</Text>
                        </View>
                        </View>
                        {/* time tag */}
                        {/* person tag */}
                        <View
                      style={{
                        backgroundColor:'#fff',
                        width:window.width/15,
                        height: window.width/30,
                        borderRadius:window.width/60,
                        flexDirection:'row',
                        elevation:3,
                        marginRight:window.width/50,
                      }}
                      >
                        <View
                        style={{
                          height:window.width/28,
                          width:window.width/28,
                          borderRadius:window.width/60,
                          backgroundColor:'black',
                          justifyContent:'center',
                          alignItems:'center'
                        }}
                        >
                          <Image
                            style={{
                              width: window.height / 100,
                              height: window.height / 100,                          
                            }}
                            source={iconPerson}
                          />
                        </View>
                        <View
                        style={{
                          height:window.width/30,
                          width:window.width/30,
                          justifyContent:'center',
                          alignItems:'center'
                        }}
                        >
                          <Text
                          style={{
                            fontSize:5,
                            color:'black',
                            textAlign:'center'
                          }}
                        >50</Text>
                        </View>
                        </View>
                        {/* person tag */}
                      </View>
                      :<View
                      style={{
                        backgroundColor:'#fff',
                        width:window.width/11,
                        height: window.width/25,
                        position:'absolute',
                        left:5,
                        top:7,
                        borderRadius:window.width/50,
                        flexDirection:'row',
                        elevation:3
                      }}
                      >
                        <View
                        style={{
                          height:window.width/25,
                          width:window.width/19.5,
                          justifyContent:'center',
                          alignItems:'center'
                        }}
                        >
                          <Text
                          style={{
                            fontSize:5,
                            color:'black',
                            textAlign:'center'
                          }}
                        >৳ {
                          cart?
                          rowData.price:
                          rowData.price.regular
                        }</Text>
                        </View>
                        <View
                        style={{
                          height:window.width/25,
                          width:window.width/25,
                          borderRadius:window.width/50,
                          backgroundColor:'black',
                          justifyContent:'center',
                          alignItems:'center'
                        }}
                        >
                          <Text
                          style={{
                            fontSize:6,
                            color:'#fff'
                          }}
                          >KG</Text>
                        </View>
                      </View>
                  }
                  
                  {/* pricing/offer  tag */}
                </View>
                <View style={{
                  width: window.width /4,
                  height: window.height / 18,
                  justifyContent: 'center',
                  alignItems:'flex-start',
                  paddingLeft:10,
              }}>
                <Text style={{
                  textAlign: 'left',
                  fontWeight:'bold',
                  fontSize: 9,
                  color: '#000'}}
                  numberOfLines={1}
                  >
                {rowData.name}
                </Text>
                {
                  cart?
                  <Text style={{
                    textAlign: 'left',
                    fontSize: 9,
                    color: '#000'}}
                    numberOfLines={1}
                    >
                      ৳ {FindNumber(rowData.price,language)}{'   '}{language==='en'?"Quantity: ":"পরিমাণ: "}{FindNumber(rowData.quantity,language)}
                  </Text>
                  :rowData.price.offer?
                  <Text style={{
                    textAlign: 'left',
                    fontSize: 9,
                    color: '#000'}}
                    numberOfLines={1}
                    >
                  <Text style={{
                    textAlign: 'left',
                    fontSize: 9,
                    color: 'red',
                    textDecorationLine:"line-through"
                  }}
                    numberOfLines={1}
                    >
                      ৳ {FindNumber(rowData.price.regular,language)}{' '}
                  </Text>
                      ৳ {FindNumber(rowData.price.offer,language)}
                  </Text>:
                  <Text style={{
                  textAlign: 'left',
                  fontSize: 9,
                  color: '#000'}}
                  numberOfLines={1}
                  >
                    ৳ {FindNumber(rowData.price.regular,language)}
                </Text>
              }
              </View>
              </TouchableOpacity>
            );
          }}
          keyExtractor={(item,index) => {
            return item._id.toString()+new Date().getMilliseconds().toString()+index.toString()}}
            />
        </View>
    );
  }

