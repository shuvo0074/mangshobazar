import React, { useEffect,useState } from "react";
import {
    FlatList,
    Text,
    TouchableOpacity,
    View,
    Dimensions,
    ActivityIndicator
} from "react-native";
import { BaseUrl } from "../env";
import {selectLanguage} from "../features/language/languageSlice";
import { useSelector, connect } from 'react-redux';
import FindNumber from "../services/FindNumber";

import FetchService from '../services/FetchService';
import FastImage from "react-native-fast-image";
const window = Dimensions.get('window');

export default function VerticalSlider ({navigation,title,limit,viewMore,id,type}) {
  const language = useSelector(selectLanguage);
  const [data,setData]=useState([])
  useEffect(()=>{
    FetchService(navigation,"GET",`/api/${type==='recipe'?"post/category":type}/${id}/${type=="recipe"?"post":"product"}?primaryCategory=1`)
        .then(products=>setData(products.data))
        .then(()=>setLoading(false))
  },[navigation])
  const [loading, setLoading]=useState(true)

  if (loading)
  return <ActivityIndicator size="large" color="#00163D" style={{
    alignSelf:'center',
    marginTop:'80%'
  }} />

  if(data.length<1 && limit==6)
  return <View/>
  return (
      <View
      style={{
        marginTop:20
      }}
      >
        <View
          style={{
            flexDirection:'row',
            height:window.height/17,
            justifyContent:'space-between',
          }}
          >

        <Text
        style={{
          fontWeight:'bold',
          fontSize:15,
          marginLeft: 20,
          color: '#00102D',
        }}
        >
          {
            data.length>0?
              title
              :language==='en'?
              `No ${title} available`
              :`${title} অবশিষ্ট নেই`
            }
        </Text>
        {
          viewMore && data.length>limit?
          <Text
            style={{
              marginRight:20,
              fontSize:10,
              fontWeight:'bold',
              marginTop:10
            }}
            onPress={()=>navigation.navigate("CategoryDetails",{id,name:title,type})}
            >{
              language==='en'?
              "See all":"সব দেখুন"}</Text>:<View/>
        }
          </View>
        {
        data.length>0?
        <FlatList
          numColumns={3}
          showsHorizontalScrollIndicator={false}
          data={data.slice(0,limit)}
          renderItem={({ item }) => {
            const rowData = language==='en'?
            {...item}:
            {...item,...item.bn}
            return (
              <TouchableOpacity
                onPress={() => navigation.navigate(type=="recipe"?"RecipeDetails":"EventDetails",{id:rowData._id})}
                style={{
                  width: window.width /3.4,
                  height: window.height / 4,
                  justifyContent: 'center',
                  marginHorizontal: 7,
                  backgroundColor:"#F1F1F1",
                  borderRadius:8,
                  elevation:5,
                  marginVertical:10
                }}
              >
                <View style={{
                    width: window.width /3.4,
                    height: window.height /5,
                    overflow: 'hidden'
              }}>
                  <FastImage
                    style={{
                      flex: 1,
                      width: undefined,
                      height: undefined,
                      resizeMode:'cover',
                      borderTopLeftRadius:8,
                      borderTopRightRadius:8,
                      margin:1
                    }}
                    source={{uri:
                      rowData.cover?
                      BaseUrl+rowData.cover.full:"http://mangshobazar.com:5000/images/logo-app.png",
                      priority: FastImage.priority.high
                  }}
                  />
                </View>
                <View style={{
                  width: window.width /4,
                  height: window.height / 18,
                  justifyContent: 'center',
                  alignItems:'flex-start',
                  paddingLeft:10,
                }}>
                  <Text style={{
                    textAlign: 'left',
                    fontWeight:'bold',
                    fontSize: 8,
                    color: '#000'}}
                    numberOfLines={1}
                    >
                  {rowData.name}
                  </Text>
                  {
                  type=="recipe"?
                  <Text style={{
                    textAlign: 'left',
                    fontSize: 8,
                    color: '#000'}}
                    numberOfLines={1}
                    >
                      {rowData.category.name}
                  </Text>:
                  rowData.price.offer?
                  <Text style={{
                    textAlign: 'left',
                    fontSize: 8,
                    color: '#000'}}
                    numberOfLines={1}
                    >
                  <Text style={{
                    textAlign: 'left',
                    fontSize: 8,
                    color: 'red',
                    textDecorationLine:"line-through"
                  }}
                    numberOfLines={1}
                    >
                      ৳ {FindNumber(rowData.price.regular,language)}{' '}
                  </Text>
                      ৳ {FindNumber(rowData.price.offer,language)}
                  </Text>:
                  <Text style={{
                  textAlign: 'left',
                  fontSize: 8,
                  color: '#000'}}
                  numberOfLines={1}
                  >
                    ৳ {FindNumber(rowData.price.regular,language)}
                </Text>
              }
                </View>
              </TouchableOpacity>
            );
          }}
          keyExtractor={(item,index) => {
            return item._id.toString()+new Date().getMilliseconds().toString()+index.toString()}}
            />:<View/>
        }
      </View>
    );
  }

