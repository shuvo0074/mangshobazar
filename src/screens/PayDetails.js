import React, { useState, useEffect } from 'react';
import {
  StyleSheet,
  ScrollView,
  StatusBar,
  Text,
  Dimensions,
  View,
  TextInput,
  Keyboard
} from 'react-native';
const window = Dimensions.get('window');
import logoNogod from '../assets/images/logoNogod.png';
import logoBkash from '../assets/images/logoBkash.png';
import logoDelivery from '../assets/images/logoDelivery.png';
import logoRocket from '../assets/images/logoRocket.png';

import {
  selectCart,
  cleanCart
} from '../features/counter/counterSlice'
import { useSelector, useDispatch } from 'react-redux';

import {
  Colors,
} from 'react-native/Libraries/NewAppScreen';
import HeaderCard from "../components/HeaderCard";

import FetchService from '../services/FetchService';
import { TouchableOpacity } from 'react-native-gesture-handler';

import { 
  systemWeights
} from 'react-native-typography';
import FastImage from 'react-native-fast-image';

const PayDetails: () => React$Node = (props) => {
  const cart = useSelector(selectCart);
  const dispatch = useDispatch();
  const [ total ,setTotal ] = useState(0)
  const [ number,setNumber ] = useState('')
  const [ transactionID,setTransactionID ] = useState('')
  const [keyFocus,setKeyFocus]=useState(true)

  const datas=[
    {
      id:1,
      logo:logoBkash
    },
    {
      id:2,
      logo:logoNogod
    },
    {
      id:3,
      logo:logoRocket
    },
    {
      id:4,
      logo:logoDelivery
    },
  ]

  const [ data, setData ]=useState({
    id:4,
    logo:logoDelivery
  })
  const _keyboardDidShow = () => {
    setKeyFocus(true)
  };

  const _keyboardDidHide = () => {
    setKeyFocus(false)
  };
  useEffect(()=>{
    // FetchService("GET","/api/category")
    // .then(async res=>setData(res.data))
    // .then(()=>setLoading(false))
    let flagTotal=0
    cart.map(cartItem=>flagTotal+=(parseInt(cartItem.item.price.regular)*cartItem.quantity))
    setTotal(flagTotal)
    Keyboard.addListener("keyboardDidShow", _keyboardDidShow);
    Keyboard.addListener("keyboardDidHide", _keyboardDidHide);

    const index=datas.findIndex(d=>d.id==props.navigation.state.params.id)
    setData(datas[index])

    return () => {
      Keyboard.removeListener("keyboardDidShow", _keyboardDidShow);
      Keyboard.removeListener("keyboardDidHide", _keyboardDidHide);
    }

  },[props])
  
  

  return (
    <>
      <StatusBar barStyle="dark-content" />
        <ScrollView
          style={styles.scrollView}>

        {
          !keyFocus?
            <HeaderCard
            half
            botMarg={ window.height/5 }
            />
            :<View/>
        }
            <View
              style={{
                backgroundColor:'#FFFFDD',
                width: window.width/1.1,
                height: window.height/2.3,
                alignSelf:'center',
                borderRadius:15,
                
              }}
              >
                {
                  !keyFocus?
                  <View
                
                style={{
                  height: window.height/4.6,
                  width: window.height/4.6,
                  backgroundColor:'#fff',
                  borderRadius: window.height/9.2,
                  borderWidth:1,
                  marginTop:-window.height/9.2,
                  alignSelf:'center',
                  alignItems:'center',
                  justifyContent:'center'
                }}
                >
                  <FastImage
                      style={{
                        width: window.height / 10,
                        height: window.height / 10,
                      }}
                      source={data.logo}
                    />
                </View>:<View/>
                }
                

                <Text
                numberOfLines={3}
                style={{
                  marginTop:!keyFocus?20:window.height/10,
                  textAlign:'center',
                  fontSize:12,
                  fontWeight:'bold'
                }}
                >Send {total+5} taka ( bdt {total} + transaction fee bdt 5 ) to 0198765432 {'\n'} Enter your phone number and Transaction ID below  </Text>
                <View 
                style={{
                  flexDirection:'row',
                  justifyContent:'space-between',
                  width: window.width*315/375,
                  marginVertical:window.height/25,
                  alignSelf:'center'

                }}>
                    <TextInput
                      placeholderTextColor="#212121"
                      placeholder="Payment Phone"
                      autoFocus={true}
                      value={number}
                      keyboardType="email-address"
                      style={ {
                        ...systemWeights.light,
                        color: '#616161',
                        width: window.width*140/375,
                        height: window.height/19,
                        fontSize: 12,
                        paddingLeft: window.width*15/375,
                        borderRadius:5,
                        backgroundColor:"#fff",
                        elevation:3
                      }}
                      onChangeText={setNumber}
                      blurOnSubmit={false}

                      />
                    <TextInput
                      // underlineColorAndroid="#8d8d8d"
                      placeholderTextColor="#212121"
                      placeholder="Transaction ID"
                      value={transactionID}
                      style={{
                        ...systemWeights.light,
                        color: '#616161',
                        width: window.width*140/375,
                        height: window.height/19,
                        fontSize: 12,
                        paddingLeft: window.width*15/375,
                        borderRadius:5,
                        backgroundColor:"#fff",
                        elevation:3
                      }}
                      onChangeText={setTransactionID}
                      blurOnSubmit={false}
                      />
                </View>

                <TouchableOpacity
                  style={{
                    height:window.height/19,
                    width:window.width/2.5,
                    borderRadius:5,
                    backgroundColor:"#FFFE01",
                    justifyContent:'center',
                    marginBottom:10,
                    elevation:2,
                    alignSelf:'center',
                    marginRight: 10
                  }}
                  onPress={()=>{
                    dispatch(cleanCart())
                    props.navigation.navigate("Home")
                  }}
                  >
                    <Text
                    style={{
                      fontSize:15,
                      color:'black',
                      alignSelf:'center'
                    }}
                    >
                      Place order
                    </Text>
                  </TouchableOpacity>
                
              </View>
            
            
        </ScrollView>
    </>
  );
};

const styles = StyleSheet.create({
  scrollView: {
    backgroundColor:"#FFF"
  },
  engine: {
    position: 'absolute',
    right: 0,
  },
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: '600',
    color: Colors.black,
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: '400',
    color: Colors.dark,
  },
  highlight: {
    fontWeight: '700',
  },
  footer: {
    color: Colors.dark,
    fontSize: 12,
    fontWeight: '600',
    padding: 4,
    paddingRight: 12,
    textAlign: 'right',
  },
});

export default PayDetails;
