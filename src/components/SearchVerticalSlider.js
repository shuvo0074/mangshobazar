import React, { useEffect,useState } from "react";
import {
    FlatList,
    Text,
    TouchableOpacity,
    View,
    Dimensions,
} from "react-native";
import { BaseUrl } from "../env";
import FastImage from 'react-native-fast-image'
import { useSelector } from 'react-redux';
import {selectLanguage} from '../features/language/languageSlice'
import FindNumber from "../services/FindNumber";
const window = Dimensions.get('window');

export default function SearchVerticalSlider (props) {
  const language = useSelector(selectLanguage);

  if(props.data.length<1)
  return <View/>

  return (
      <View
      style={{
        marginBottom:150,
        padding:10,
        backgroundColor:'#FFFEBF',
        // marginTop:window.height/10,
        borderRadius:20,
        display:'flex'
      }}
      >
        <FlatList
          data={props.data}
          numColumns={3}
          showsHorizontalScrollIndicator={false}
          renderItem={({ item }) => {
            const rowData = language==='en'?
            {...item}:
            {...item,...item.bn}
            return (
              <TouchableOpacity
                onPress={() => props.navigation.navigate("EventDetails",{id:rowData._id})}
                style={{
                  width: window.width /3.4,
                  height: window.height / 4,
                  justifyContent: 'center',
                  marginHorizontal: 7,
                  backgroundColor:"#F1F1F1",
                  borderRadius:8,
                  elevation:5,
                  marginVertical:10
                }}
              >
                <View style={{
                  width: window.width /3.4,
                  height: window.height /5,
                  overflow: 'hidden'
              }}>
                  <FastImage
                    style={{
                      flex: 1,
                      width: undefined,
                      height: undefined,
                      resizeMode: 'cover',
                      borderTopLeftRadius:8,
                      borderTopRightRadius:8,
                      margin:1
                  }}
                    source={{
                      priority: FastImage.priority.high,
                      uri:
                      rowData.cover?BaseUrl+rowData.cover.full:"http://mangshobazar.com:5000/images/logo-app.png"}}
                  />
                </View>
                <View
                  style={{
                    height: window.width /15,
                    width: window.width /15,
                    borderRadius: window.width /24,
                    position:'absolute',
                    top: window.width /70,
                    left: window.width /70,
                    backgroundColor:'#FFFEC7',
                    justifyContent:'center',
                    alignItems:'center',
                    padding:2
                  }}
                  >
                    <Text
                    numberOfLines={2}
                    style={{
                      textAlign:'center',
                      fontSize:6,
                      fontWeight:'bold'
                    }}
                    > ৳ {rowData.price.offer?
                      FindNumber((parseInt(rowData.price.regular) - parseInt(rowData.price.offer)),language):0
                    } {'\n'} {
                    language=='en'?
                    "off"
                    :"ছাড়"
                  } </Text>
                  </View>
                  <View style={{
                  width: window.width /4,
                  height: window.height / 18,
                  justifyContent: 'center',
                  alignItems:'flex-start',
                  paddingLeft:10,
              }}>
                <Text style={{
                  textAlign: 'left',
                  fontWeight:'bold',
                  fontSize: 8,
                  color: '#000'}}
                  numberOfLines={1}
                  >
                {rowData.name}
                </Text>
                {
                  rowData.price.offer?
                  <Text style={{
                    textAlign: 'left',
                    fontSize: 8,
                    color: '#000'}}
                    numberOfLines={1}
                    >
                  <Text style={{
                    textAlign: 'left',
                    fontSize: 8,
                    color: 'red',
                    textDecorationLine:"line-through"
                  }}
                    numberOfLines={1}
                    >
                      ৳ {FindNumber(rowData.price.regular)}{' '}
                  </Text>
                      ৳ {FindNumber(rowData.price.offer)}
                  </Text>:
                  <Text style={{
                  textAlign: 'left',
                  fontSize: 8,
                  color: '#000'}}
                  numberOfLines={1}
                  >
                    ৳ {FindNumber(rowData.price.regular)}
                </Text>
              }
              </View>

              </TouchableOpacity>
            );
          }}
          keyExtractor={(item,index) => {
            return item._id.toString()+new Date().getMilliseconds().toString()+index.toString()}}
            />
        
      </View>
    );
  }

