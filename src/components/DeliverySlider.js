import React, { useEffect,useState } from "react";
import {
    FlatList,
    Text,
    TouchableOpacity,
    View,
    Dimensions,
    ActivityIndicator
} from "react-native";
import { BaseUrl } from "../env";

import GlobalStyles from '../styles/Styles';
import FetchService from '../services/FetchService';
const window = Dimensions.get('window');
import defaultLogo from '../assets/icons/iconOrderList.png';

export default function DeliverySlider (props) {
  useEffect(()=>{
    FetchService(props.navigation,"GET",`/api/delivery`)
        .then(products=>setData(products))
        .then(()=>setLoading(false))
  },[props])
  const [loading, setLoading]=useState(true)
  const [data,setData]=useState([])

  if (loading)
  return <ActivityIndicator size="large" color="#00163D" style={{
    alignSelf:'center',
    marginTop:'80%'
  }} />

  if(data.length<1 && props.limit==3)
  return <View/>

  return (
      <View
      style={{
        marginBottom:10,
        padding:10,
        backgroundColor:'#FFFEBF',
        // marginTop:window.height/10,
        borderRadius:20,
        display:'flex'
      }}
      >
        <Text
        style={{
          fontWeight:'bold',
          fontSize:15,
          color:'#FADC62',
          marginLeft: 20,
          color: props.darkText? '#00102D':'#FADC62'
        }}
        >
          {    
            data.length>0?
              props.title
              :`No ${props.title} available`
            }
        </Text>
        {
        data.length>0?
        <FlatList
          data={data}
          contentContainerStyle={{
          marginVertical: 15,
          }}
          ListFooterComponent={()=><View
          style={{
            height: props.limit==3?0: 450
          }}/>}
          renderItem={({ item }) => {
            return (
              <TouchableOpacity
                onPress={() => props.navigation.navigate("Checkout",{delivery:item._id})}
                style={{
                  width: window.width -40,
                  height: window.height /8,
                  justifyContent: 'space-between',
                  marginVertical:7,
                  alignItems: 'flex-start',
                  marginHorizontal: 10,
                  backgroundColor:"#fff",
                  borderRadius:10,
                  elevation:5,
                  flexDirection: 'row'
              }}
              >
                <View style={{
                  width: window.width-40,
                  height: window.height /8.5,
                  justifyContent:'space-evenly',
                  alignItems: 'center',
                  padding:5,
                  flexDirection:'row',
                  // backgroundColor:'black'
                }}>
                  <View
                  style={{
                    height: window.height /9,
                    width:window.width/2.5,
                    // backgroundColor:'red',
                    justifyContent:'center'
                  }}
                  >
                    <Text style={[GlobalStyles.caption, GlobalStyles.medium, GlobalStyles.leftTxt, {color: '#00163D'}]}
                    numberOfLines={1}>
                    {item.name}
                    </Text>
                    <Text style={[GlobalStyles.body2, GlobalStyles.light, GlobalStyles.leftTxt, {color: '#00163D'}]}>
                    Country: {item.countryName}
                    </Text>
                  </View>
                  <View
                  style={{
                    height: window.height /9,
                    width:window.width/4,
                    marginRight:12,
                    justifyContent:'center',
                    alignItems:'center'
                  }}
                  >
                    <Text
                    style={{
                      fontSize:10,
                      fontWeight:'bold',
                      color:'black'
                    }}
                    >time: {item.time} </Text>
                    <Text
                    style={{
                      fontSize:20,
                      fontWeight:'bold',
                      color:'black'
                    }}
                  >{item.charge[0]}TK</Text>
                  </View>
                </View>
              </TouchableOpacity>
            );
          }}
          keyExtractor={(item,index) => {
            return item._id.toString()+new Date().getMilliseconds().toString()+index.toString()}}
            />:<View/>
        }
      </View>
    );
  }

