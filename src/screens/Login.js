import React, { useState, useEffect } from "react";
import {  Dimensions, Text, TextInput, TouchableOpacity, View ,BackHandler, Keyboard,Modal} from "react-native";
import loginStyles from "../styles/loginStyles";
import FetchService from "../services/FetchService";
import HeaderCard from "../components/HeaderCard";

import { useSelector } from 'react-redux';
import {selectLanguage} from '../features/language/languageSlice'
import { 
  systemWeights
} from 'react-native-typography';

const window = Dimensions.get('window');

function Login (props) {
  const language = useSelector(selectLanguage);
  const [keyFocus,setKeyFocus]=useState(true)
  const [signupError, setSignupError]=useState('')

  const [username,setName]=useState('')
  const [password,setPassword]=useState('')
  const [modalVisible, setModalVisible] = useState(false);
  const _keyboardDidShow = () => {
    setKeyFocus(true)
  };

  const _keyboardDidHide = () => {
    setKeyFocus(false)
  };
  useEffect(()=>{

    Keyboard.addListener("keyboardDidShow", _keyboardDidShow);
    Keyboard.addListener("keyboardDidHide", _keyboardDidHide);

    return () => {
      Keyboard.removeListener("keyboardDidShow", _keyboardDidShow);
      Keyboard.removeListener("keyboardDidHide", _keyboardDidHide);
    }
  },[])
  function handleRecover(){
    let body={
      username
    }
    FetchService(props.navigation,"POST", "/customer/auth/recover",1,body,false)
    .then(details=> {
      setName('')
      setModalVisible(false)
      if(details.email){
        props.navigation.navigate("ResetPass",{details})
      }
      else setSignupError(Object.values(details)[0])
    })
  }

  function handleSubmit() {

    if (username.length<1){
      setSignupError(
        language==='en'?
        'insert email or username'
        :'নাম অথব ইমেইল'
        )
    }
    else if (password.length<1){
      setSignupError(
        language==='en'?
        'insert password'
        :"পাসওয়ার্ড আবশ্যক" )
    }
    else if (password.length<3){
      setSignupError(
        language==='en'?
        'password at least 3 characters':
        "পাসওয়ার্ড কমপক্ষে ৩ ডিজিট")
    }
    else{
      let body={
        username,password
      }
      FetchService(props.navigation,"POST","/customer/auth/login",3,body)
      .then(res=>{
        if(res.success){
          props.navigation.navigate("AppStack")
        }
        else setSignupError(Object.values(res)[0])
      })
    }
  }
    return (
      <View style={loginStyles.loginCont}>
        {
          !keyFocus?
          <HeaderCard
            botMarg={window.height/8}
            half
            />
          :<View/>
        }
        <View
        style={{
          backgroundColor:'#FFFFDD',
          width: window.width/1.1,
          height: window.height/2,
          alignSelf:'center',
          borderRadius:15

        }}
        >
        <Text
          style={{
            color:'red',
            alignSelf:'flex-start',
            alignSelf:'center',
            marginTop:10,
            marginBottom:-40
  
          }}>
            {signupError}
          </Text>

        <View style={loginStyles.loginContent}>

          {/* title and input starts */}
          <View style={{
            justifyContent: 'space-around',
            alignItems: 'center',
            alignSelf: 'center'
          }}>
              <TextInput
                // underlineColorAndroid="#8d8d8d"
                placeholderTextColor="#212121"
                placeholder={
                  language==='en'?
                  "Email or phone"
                  :"ইমেইল অথবা ফোন"
                }
                autoFocus={true}
                value={username}
                keyboardType="email-address"
                style={ {
                  ...systemWeights.light,
                  color: '#616161',
                  width: window.width*305/375,
                  height: window.width*56/375,
                  fontSize: 16,
                  paddingLeft: window.width*25/375,
                  marginTop: 70,
                  borderRadius:5,
                  backgroundColor:"#fff",
                  elevation:3
                }}
                onChangeText={setName}
                blurOnSubmit={false}

                />
              <TextInput
                // underlineColorAndroid="#8d8d8d"
                placeholderTextColor="#212121"
                placeholder={
                language==='en'?
                  "Password"
                  :"পাসওয়ার্ড"
                }
                value={password}
                secureTextEntry={true}
                style={[ {
                ...systemWeights.light,
                color: '#616161',
                width: window.width*305/375,
                height: window.width*56/375,
                fontSize: 16,
                paddingLeft: window.width*25/375,
                marginBottom: 30,
                borderRadius:5,
                backgroundColor:"#fff",
                elevation:3
              },{
                  marginTop:40
                } ]}
                onChangeText={setPassword}
                blurOnSubmit={false}
                />
          </View>
          {/* title and input ends */}
        </View>
        <Modal
          animationType="slide"
          transparent={true}
          visible={modalVisible}
        ><View
        style={{
          height:window.height,
          width:window.width,
          backgroundColor:'#00000099'
        }}
        >
          <View
          style={{
            alignSelf:'center',
            width:window.width/1.1,
            backgroundColor: "#FDFEC8ee",
            borderRadius: 20,
            padding: 35,
            alignItems: "center",
            shadowColor: "#000",
            marginTop:keyFocus?window.height/10: window.height/2.7,
            shadowOffset: {
              width: 0,
              height: 2
            },
            shadowOpacity: 0.25,
            shadowRadius: 3.84,
            elevation: 5
          }}
          >
            <View
            style={{
              flexDirection:'row',
              width:'100%',
              width:window.width*305/375,
              justifyContent:'space-between'
            }}
            >
            <Text
            style={{
              fontSize:18,
              color:'#555',
            }}
            >
              Enter Username
            </Text>
            <TouchableOpacity
            style={{
              height:window.width/12,
              width:window.width/12,
              borderRadius:window.width/12,
              justifyContent:'center',
              alignItems:'center',
              borderWidth:1,
              marginTop:-15
            }}
            onPress={()=>setModalVisible(false)}
            >
              <Text
              style={{
                color:'black'
              }}
              >X</Text>
            </TouchableOpacity>
            </View>
            <TextInput
              // underlineColorAndroid="#8d8d8d"
              placeholderTextColor="#212121"
              placeholder={
              language==='en'?
                "Recovery Email or phone"
                :"রিকভারি ইমেইল অথবা ফোন"
              }
              value={username}
              style={[ {
              ...systemWeights.light,
              color: '#333',
              width: window.width*305/375,
              height: window.height/14,
              fontSize: 12,
              paddingLeft: window.width*25/375,
              marginBottom: 30,
              borderRadius:5,
              borderColor:'#666',
              borderWidth:1,
            },{
                marginTop:25
              } ]}
              onChangeText={setName}
              blurOnSubmit={false}
              keyboardType="email-address"
              />
              <TouchableOpacity
                style={{
                  height:window.height/15,
                  width:window.width/2.5,
                  borderRadius:5,
                  backgroundColor:"#FFFE01",
                  justifyContent:'center',
                  marginBottom:10,
                  borderWidth:1,
                  borderColor:"#ccc"
                }}
                onPress={handleRecover}
                >
                  <Text
                  style={{
                    fontSize:15,
                    color:'black',
                    alignSelf:'center'
                  }}
                  >
                    {
                    language=='en'?
                    "Send Code"
                    :"কোড পাঠান"
                  }
                  </Text>
                </TouchableOpacity>
            
          </View>
          </View>
        </Modal>
        <View
        style={{
          margin:15,
          justifyContent:'space-between',
          width:window.width/2,
          alignItems:'center',
          alignSelf:'center'
        }}
        >
        <View
        style={{
          flexDirection:'row',
          marginBottom:20
        }}
        >
        <TouchableOpacity
        style={{
          height:window.height/20,
          width:window.width/2.5,
          alignSelf:'center',
          backgroundColor:"transparent",
          justifyContent:'center'
        }}
        onPress={()=>setModalVisible(true)}
        >
          <Text
          style={{
            fontSize:12,
            color:'#00163D',
            alignSelf:'center',
            fontWeight:'bold'
          }}
          >
            {
            language=='en'?
            "Forgot Password"
            :"পাসওয়ার্ড পরিবর্তন"
          }
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
        style={{
          height:window.height/20,
          width:window.width/2.5,
          alignSelf:'center',
          backgroundColor:"transparent",
          justifyContent:'center'
        }}
        onPress={()=>props.navigation.navigate("Signup")}
        >
          <Text
          style={{
            fontSize:15,
            color:'#00163D',
            alignSelf:'center',
            fontWeight:'bold'
          }}
          >
            {
            language=='en'?
            "Create Account"
            :"নতুন একাউন্ট"
          }
          </Text>
        </TouchableOpacity>
        </View>
        <TouchableOpacity
        style={{
          height:window.height/15,
          width:window.width/2.5,
          borderRadius:5,
          backgroundColor:"#FFFE01",
          justifyContent:'center',
          marginBottom:10,
          elevation:2
        }}
        onPress={handleSubmit}
        >
          <Text
          style={{
            fontSize:15,
            color:'black',
            alignSelf:'center'
          }}
          >
            {
            language=='en'?
            "Login"
            :"লগইন"
          }
          </Text>
        </TouchableOpacity>

        </View>
        </View>     
      </View>
    );
  }
  export default Login