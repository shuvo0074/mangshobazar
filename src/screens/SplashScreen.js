import React, { useState, useEffect } from 'react';
import {
    View,
    Image,
    Dimensions,
    StatusBar
} from 'react-native';

const window = Dimensions.get('window');
const SplashScreen: () => React$Node = (props) => {
  useEffect(()=>{
      setTimeout(()=>{
          props.navigation.navigate("AppStack")
      },3000)
  },[props])

  return (
    <>
      <StatusBar barStyle="dark-content" />
      <View
      style={{
          height:window.height,
          width:window.width,
          alignItems:'center',
          justifyContent:'center',
          backgroundColor:'#FDFEC8'
        }}
      >
        <Image
            style={{
            width: window.width /1.5,
            height: window.width /1.5,
            resizeMode:'contain',
        }}
            source={
            {uri:"http://mangshobazar.com/images/logo-app.png"}}
        />
      </View>
    </>
  );
};

export default SplashScreen;
