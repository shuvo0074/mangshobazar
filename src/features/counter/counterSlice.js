import { createSlice } from '@reduxjs/toolkit';

      // Redux Toolkit allows us to write "mutating" logic in reducers. It
      // doesn't actually mutate the state because it uses the Immer library,
      // which detects changes to a "draft state" and produces a brand new
      // immutable state based off those changes

export const cartSlice = createSlice({
  name: 'cart',
  initialState: {
    value: {
      total:0,
      cart:[]
    }
  },
  reducers: {
    addToCart: (state,action) => {
      let index= state.value.cart.findIndex(data=>data._id===action.payload._id && data.price._id===action.payload.price._id)
      if(index==-1){
        state.value.cart.push(action.payload)
        state.value.total=parseInt(state.value.total)+parseInt(action.payload.total)
      }
      else {
        state.value.total=parseInt(state.value.total)+parseInt(action.payload.total)-parseInt(state.value.cart[index].total)
        state.value.cart[index]=action.payload
      }
    },
    removeFromCart: (state,action) => {
      let arr = state.value.cart
      arr = arr.filter( data => data._id!=action.payload.dataId || data.price._id != action.payload.priceId)
      state.value.total=parseInt(state.value.total)-parseInt(action.payload.total)
      state.value.cart=arr
    },
    cleanCart: state=> {
      state.value={
        total:0,
        cart:[]
      }
    }
  }
})

export const { addToCart, removeFromCart, cleanCart } = cartSlice.actions;

// The function below is called a thunk and allows us to perform async logic. It
// can be dispatched like a regular action: `dispatch(incrementAsync(10))`. This
// will call the thunk with the `dispatch` function as the first argument. Async
// code can then be executed and other actions can be dispatched
// The function below is called a selector and allows us to select a value from
// the state. Selectors can also be defined inline where they're used instead of
// in the slice file. For example: `useSelector((state) => state.counter.value)`
export const selectCart = state => state.cart.value;

export default cartSlice.reducer;
