import React, { useState, useEffect } from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  StatusBar,
  ActivityIndicator,
  Dimensions,
} from 'react-native';
const window = Dimensions.get('window');

import {selectLanguage} from '../features/language/languageSlice'
import { useSelector } from 'react-redux';

import {
  Colors,
} from 'react-native/Libraries/NewAppScreen';
import VerticalSlider from "../components/VerticalSlider";
import HeaderCard from "../components/HeaderCard";

import FetchService from '../services/FetchService';

const Categories: () => React$Node = ({navigation}) => {
  const language = useSelector(selectLanguage);
  useEffect(()=>{
    FetchService(navigation,"GET","/api/category")
    .then(async res=>setData(res.data))
    .then(()=>setLoading(false))

  },[navigation])

  const [loading,setLoading]=useState(true)
  const [data,setData]=useState([])

  if (loading)
  return <ActivityIndicator size="large" color="#FFFEBF" style={{
    alignSelf:'center',
    marginTop:'80%'
  }} />
  return (
    <>
      <StatusBar barStyle="dark-content" />
        <ScrollView
          style={styles.scrollView}>
            <HeaderCard
            half
            botMarg={ window.height/25 }
            />
            {
              data.map(ite=>
                {
                  const item = language==='en'?
                {...ite}:
                {...ite,...ite.bn}
                return (
                  <VerticalSlider
                  id={item._id}
                  limit={6}
                  type={"category"}
                  navigation={navigation}
                  darkText
                  viewMore
                  title={item.name}
                  />
                )
                }
              )
            }
        </ScrollView>
    </>
  );
};

const styles = StyleSheet.create({
  scrollView: {
    backgroundColor:"#FFF"
  },
  engine: {
    position: 'absolute',
    right: 0,
  },
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: '600',
    color: Colors.black,
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: '400',
    color: Colors.dark,
  },
  highlight: {
    fontWeight: '700',
  },
  footer: {
    color: Colors.dark,
    fontSize: 12,
    fontWeight: '600',
    padding: 4,
    paddingRight: 12,
    textAlign: 'right',
  },
});

export default Categories;
