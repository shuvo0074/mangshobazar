import React from "react";
import {StyleSheet,SafeAreaView,TouchableOpacity,Image,Text,Dimensions, View, Platform} from "react-native"
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from '@react-navigation/stack';
import { createCompatNavigatorFactory, createSwitchNavigator } from '@react-navigation/compat';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { Provider } from 'react-redux';
import store from './store/store';
import FindNumber from "./services/FindNumber";

import {
	selectCart
  } from './features/counter/counterSlice'
import {selectLanguage} from "./features/language/languageSlice";
import { useSelector, connect } from 'react-redux';

import SplashScreen from './screens/SplashScreen'
import Home from './screens/Home'
import Search from './screens/Search'
import Category from './screens/Categories'
import Login from './screens/Login'
import Signup from './screens/Signup'
import EventDetails from './screens/EventDetails'
import CategoryDetails from './screens/CategoryDetails'
import CardDetails from './screens/CardDetails'
import SearchResult from './screens/SearchResult'
import RecipeDetails from './screens/RecipeDetails'
import OrderDetails from './screens/OrderDetails'
import ResetPass from './screens/ResetPass'
import NewPassword from './screens/NewPassword'
import ChangePassword from './screens/ChangePassword'

import Profile from './screens/Profile'
import AllRecipe from './screens/AllRecipe'
import EditProfile from './screens/ProfileEdit'
import OrderHistory from './screens/OrderHistory'
import DeliveryList from './screens/DeliveryList'
import Checkout from './screens/Checkout'
import Shipping from './screens/Shipping'
import PayMethod from './screens/PayMethod'
import PayDetails from './screens/PayDetails'
import Cart from './screens/Cart'
import ErrorLanding from './screens/ErrorLanding'
import AsyncStorage from '@react-native-community/async-storage';

const window = Dimensions.get('window');

import iconHome from './assets/icons/iconHome.png';
import iconCategory from './assets/icons/iconCategory.png';
import iconSearch from './assets/icons/iconSearch.png';
import iconProfile from './assets/icons/iconProfile.png';
import iconCartBlack from './assets/icons/iconCartBlack.png';


const MyTabBar= connect()(({ state,navigation })=> {
	const {cart,total} = useSelector(selectCart);
	const language = useSelector(selectLanguage)

	return (
    <SafeAreaView >{
		cart.length?
		<View
	style={{
		width: window.width,
		paddingHorizontal:15,
		height: window.height/15,
		flexDirection: 'row',
		backgroundColor:'#FDD670',
		justifyContent:'space-between',
		alignItems:'center'
	
	}}
	>
		<View
		style={{
			width: window.width/3,
			height: window.height/15,
			backgroundColor:
			total>=5000?
			'#2ecc71':'#FDD670',
			justifyContent:'center',
			alignItems:'center'
		}}
		>
			{
			total>=5000?
				<Text
			style={{
				fontWeight:'bold'
			}}
			>{
			language==='en'?
			"Shipping free!":
			"ডেলিভারি চার্জ ফ্রী !"
			}</Text>:<View/>
		
		}
			

		</View>
		<Text
		style={{
			fontWeight:'bold'
		}}
		>
			{
			language==='en'?
			"Total cost":"মোট খরচ"
		}
		</Text>
		<Text
		style={{
			marginRight:window.width/10
		}}
		>
		৳ {FindNumber(total,language)}
		</Text>

	</View>:<View/>
	}
	
	<View
    style={styles.footer}
		>
			<TouchableOpacity
				
				style={styles.footerTab}
				onPress={() =>
					state.index==0?
					navigation.navigate("Home")
					:navigation.navigate("HomeStack")					
				}
			>
				<Image
					style={styles.footerIcon}
					source={iconHome}
				/>
			</TouchableOpacity>

			<TouchableOpacity
				style={styles.footerTab}
				onPress={() =>
					state.index==1?
					navigation.navigate("Search")
					:navigation.navigate("SearchStack")
				}
			>
				<Image
					style={styles.footerIcon}
					source={iconSearch}
				/>
			</TouchableOpacity>

			<TouchableOpacity
				style={styles.footerTab}
				onPress={() =>navigation.navigate("Cart")}
			>
				<Image
					style={{
						width: window.height / 23,
						height: window.height / 23,
					}}
					source={iconCartBlack}
				/>
				<View
				style={{
					position:'absolute',
					top:window.height/50,
					right:window.width/25,
					height:16,
					width:16,
					borderRadius:8,
					backgroundColor:'black',
					justifyContent:'center',
					alignItems:'center'
				}}
				>
				<Text
				style={{
					fontSize:10,
					fontWeight:'bold',
					color:'white'
				}}
				>{FindNumber(cart.length,language)}</Text>
				</View>
			</TouchableOpacity>

			<TouchableOpacity
				style={styles.footerTab}
				onPress={() =>
				{
					AsyncStorage.getItem('userToken')
					.then(sesToken=>{
						if (sesToken){
							state.index==3?
							navigation.navigate("Profile")
							:navigation.navigate("ProfileStack")
						}
						else {
							navigation.navigate("AuthStack")
						}
					})
				}
			}
			>
				<Image
					style={{
						width: window.height / 29,
						height: window.height / 29,
					}}
					source={iconProfile}
				/>
			</TouchableOpacity>

			<TouchableOpacity
				style={styles.footerTab}
				onPress={() =>
				state.index==4?
				navigation.navigate("Category")
				:navigation.navigate("CategoryStack")
			}
			>
				<Image
					style={styles.footerIcon}
					source={iconCategory}
				/>
			</TouchableOpacity>
			</View>
		</SafeAreaView>
	);
})

// three stacks under three tabs of app tab navigator
const HomeStack = createCompatNavigatorFactory(createStackNavigator)(
	{
		Home: { screen: Home, navigationOptions:{headerShown:false}  },
		EventDetails: { screen: EventDetails, navigationOptions:{headerShown:false}  },
		RecipeDetails: { screen: RecipeDetails, navigationOptions:{headerShown:false}  },
		AllRecipe: { screen: AllRecipe, navigationOptions:{headerShown:false}  },
		Checkout: { screen: Checkout, navigationOptions:{headerShown:false}  },
		// Cart: {screen: Cart, navigationOptions:{headerShown:false } },
		Shipping: { screen: Shipping, navigationOptions:{headerShown:false}  },
		PayMethod: { screen: PayMethod, navigationOptions:{headerShown:false}  },
		PayDetails: { screen: PayDetails, navigationOptions:{headerShown:false}  },
		CategoryDetails: { screen: CategoryDetails, navigationOptions:{headerShown:false}  },
		CardDetails: { screen: CardDetails, navigationOptions:{headerShown:false}  },
		DeliveryList: { screen: DeliveryList, navigationOptions:{headerShown:false}  }

	},
	options
);

const CategoryStack = createCompatNavigatorFactory(createStackNavigator)(
	{
		Category: { screen: Category, navigationOptions:{headerShown:false}  },
		EventDetails: { screen: EventDetails, navigationOptions:{headerShown:false}  },
		PayDetails: { screen: PayDetails, navigationOptions:{headerShown:false}  },
		PayMethod: { screen: PayMethod, navigationOptions:{headerShown:false}  },
		Shipping: { screen: Shipping, navigationOptions:{headerShown:false}  },
		Checkout: { screen: Checkout, navigationOptions:{headerShown:false}  },
		// Cart: {screen: Cart, navigationOptions:{headerShown:false } },
		DeliveryList: { screen: DeliveryList, navigationOptions:{headerShown:false}  },
		CategoryDetails: { screen: CategoryDetails, navigationOptions:{headerShown:false}  }
	},
	options
);

const ProfileStack = createCompatNavigatorFactory(createStackNavigator)(
	{
		Profile: { screen: Profile, navigationOptions:{headerShown:false}  },
		EditProfile: { screen: EditProfile, navigationOptions:{headerShown:false}  },
		ChangePassword: { screen: ChangePassword, navigationOptions:{headerShown:false}  },
		OrderHistory: { screen: OrderHistory, navigationOptions:{headerShown:false}  },
		EventDetails: { screen: EventDetails, navigationOptions:{headerShown:false}  },
		PayDetails: { screen: PayDetails, navigationOptions:{headerShown:false}  },
		PayMethod: { screen: PayMethod, navigationOptions:{headerShown:false}  },
		Shipping: { screen: Shipping, navigationOptions:{headerShown:false}  },
		Checkout: { screen: Checkout, navigationOptions:{headerShown:false}  },
		OrderDetails: {screen: OrderDetails, navigationOptions:{headerShown:false } },
		DeliveryList: { screen: DeliveryList, navigationOptions:{headerShown:false}  }
	},
	options
);

const SearchStack = createCompatNavigatorFactory(createStackNavigator)(
	{
		Search: { screen: Search, navigationOptions:{headerShown:false}  },
		EventDetails: { screen: EventDetails, navigationOptions:{headerShown:false}  },
		PayMethod: { screen: PayMethod, navigationOptions:{headerShown:false}  },
		PayDetails: { screen: PayDetails, navigationOptions:{headerShown:false}  },
		Checkout: { screen: Checkout, navigationOptions:{headerShown:false}  },
		Shipping: { screen: Shipping, navigationOptions:{headerShown:false}  },
		// Cart: {screen: Cart, navigationOptions:{headerShown:false } },
		SearchResult: { screen: SearchResult, navigationOptions:{headerShown:false}  },
		DeliveryList: { screen: DeliveryList, navigationOptions:{headerShown:false}  }
	},
	options
);

const Tab = createBottomTabNavigator();

// App Tab navigator
function HomeContainer() {
    return (
      <Tab.Navigator screenOptions={options} 
      tabBar={props => <MyTabBar {...props} />}
      >
          <Tab.Screen name="HomeStack" component={HomeStack} />
          <Tab.Screen name="SearchStack" component={SearchStack} />
          <Tab.Screen name="Cart" component={Cart} />
          <Tab.Screen name="ProfileStack" component={ProfileStack} />
          <Tab.Screen name="CategoryStack" component={CategoryStack} />
        </Tab.Navigator>
    );
  }

// pass screen props to home navigator
function Home_Navigation({navigation}){
    return <HomeContainer screenProps={{ rootNavigation: navigation }} />
}

const appRoutes = {
    Home_Navigation: { screen: Home_Navigation, navigationOptions:{headerShown:false} }
};

const AppStack = createCompatNavigatorFactory(createStackNavigator)(appRoutes, options);

const options = {
  cardStyle: {
    backgroundColor: "#fff"
  },
  navigationOptions: {
    gesturesEnabled: true
  }
};
const AuthStack = createCompatNavigatorFactory(createStackNavigator)(
	{
	  Login: { screen: Login },
	  Signup: { screen: Signup },
	  ResetPass: { screen: ResetPass},
	  NewPassword:{ screen: NewPassword}
	},
	{
	  headerMode: "none"
	}
  );
const AppNavigator = createSwitchNavigator(
    {
		SplashScreen: {screen:SplashScreen},
        AppStack,
		AuthStack,
		ErrorLanding: {screen:ErrorLanding}
    },
    {
      initialRouteName: Platform.OS==='android'? "SplashScreen":'AppStack',
      headerMode: 'none'
    }
  )

export default function Routes(){
    return (
		<Provider store={store}>
        <NavigationContainer><AppNavigator/></NavigationContainer>
		</Provider>
    )
}
const styles = StyleSheet.create({
	footer: {
    width: window.width,
    // paddingHorizontal:15,
    height: window.height/12,
	flexDirection: 'row',
	justifyContent: 'space-between',
	alignItems: 'center',
	backgroundColor: '#FFF4C7',
	borderTopWidth: 0.5,
	borderTopColor: '#cfcfcf',
	shadowColor: 'rgba(0, 0, 0, 0.25)',
	shadowOffset: {width: 0, height: -4},
	shadowOpacity: 1,
	shadowRadius: 4,
	elevation: 9,
	},
	footerTab: {
    width: window.width / 5,
	height: window.height/12,
    justifyContent: 'center',
	alignItems: 'center',
    backgroundColor: 'transparent',
    borderRadius:10
	},
	activeTab: {
		width: window.width / 4,
		height: window.height / 12,
		justifyContent: 'center',
		alignItems: 'center',
		backgroundColor: '#FF0000'
	},
	footerIcon: {
		width: window.height / 24,
		height: window.height / 24,
	},
	footerTxt: {
		fontSize: 8,
		color: '#fff'
	},
	activeTxt: {
		fontSize: 8,
		fontWeight: 'bold',
		color: '#FFFEBF'
	},
	countHolder: {
		width: window.height / 20,
		height: window.height / 20,
		borderRadius: window.height / 40,
		justifyContent: 'center',
		alignItems: 'center',
		backgroundColor: '#fff000',
		position: 'absolute',
		right: 1,
		top: 1
	},
	messageCount: {
		color: '#333',
		fontSize: 12,
		fontWeight: 'bold'
	}
});