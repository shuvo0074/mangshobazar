import React, { useState, useEffect } from "react";
import { Dimensions, Text, TextInput, TouchableOpacity, View ,StyleSheet, Keyboard, ScrollView, Picker, KeyboardAvoidingView,Image} from "react-native";
import loginStyles from "../styles/loginStyles";
const window = Dimensions.get('window');
import HeaderCard from "../components/HeaderCard";
import ImagePicker from 'react-native-image-picker';
const pickerOptions = {
  title: 'Select Image',
  cancelButtonTitle:'Go back',
  cameraType:'front',
  mediaType:'photo',
  quality:1,
  storageOptions: {
    skipBackup: true,
    path: 'mangsho_bazar',
  },
};
import { useSelector } from 'react-redux';
import {selectLanguage} from '../features/language/languageSlice'
import FetchService from "../services/FetchService";
import FastImage from "react-native-fast-image";
import defaultLogo from '../assets/images/defaultLogo.png';
import AsyncStorage from '@react-native-community/async-storage';

function ProfileEdit (props) {
  const language = useSelector(selectLanguage);
  const [ dealerCode, setDealerCode ] = useState('')
  const [ showDealerCode, setShowDealerCode ] = useState(false)
  const [keyFocus,setKeyFocus]=useState(false)
  const [data,setData]=useState([])
  const [countries, setCountries]=useState('')
  const [cities, setCities]=useState('')
  const [cityLoading, setCityLoading]=useState(true)
  const [countryLoading, setCountryLoading]=useState(true)
  const [firstName,setFName]=useState('')
  const [lastName,setLName]=useState('')
  const [ countryId , setCountryId ] = useState("5e8218a9a0be4401500e4d37")
  const [ cityId , setCityId ] = useState("5e82297958cd81174c1df387")
  const [country,setCountry]=useState({
    capital: "Dhaka",
    code: "BD",
    continent: {_id: "5e821761311eb9259c0ba82d", objectId: "mSxk54vkg6", code: "AS", name: "Asia"},
    continentCode: "AS",
    continentName: "Asia",
    currency: "BDT",
    emoji: "🇧🇩",
    emojiU: "U+1F1E7 U+1F1E9",
    name: "Bangladesh",
    native: "Bangladesh",
    objectId: "AWnxgoUzw0",
    phone: "880",
    _id: "5e8218a9a0be4401500e4d37"
  })
  const [city,setCity]=useState({
    altName: "",
    cityId: 1185241,
    country: "5e8218a9a0be4401500e4d37",
    countryCode: "BD",
    countryName: "Bangladesh",
    isCapital: true,
    location: {latitude: 23.7104, longitude: 90.40744},
    name: "Dhaka",
    objectId: "Q0rzROBMnh",
    population: 10356500,
    _id: "5e82297958cd81174c1df387"
  })
  const [address1,setAddress1]=useState('')
  const [address2,setAddress2]=useState('')
  const [phone,setPhone]=useState('')
  const [email,setEmail]=useState('')
  const [password,setPassword]=useState('')
  const [password2,setPassword2]=useState('')
  const [signupError, setSignupError]=useState('')
  const [ profilImage, setProfileImage ] = useState({uri:"http://mangshobazar.com/images/logo-app.png"})
  
  const _keyboardDidShow = () => {
    setKeyFocus(true)
  };

  const _keyboardDidHide = () => {
    setKeyFocus(false)
  };

  function uploadImage(){
    ImagePicker.showImagePicker(pickerOptions, (response) => {
    
      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      } else {
        const source = { uri: response.uri };
    
        // You can also display the image using data:
        // const source = { uri: 'data:image/jpeg;base64,' + response.data };
        setProfileImage(source)
      }
    });
    
  }
  useEffect(()=>{
    AsyncStorage.getItem('userToken')
    .then(sesToken=>{
      if (sesToken){
        FetchService(props.navigation,"GET","/customer/api/profile")
        .then(response=>{
          setAddress1(response.address1)
          setAddress2(response.address2)
          setData(response)
          setFName(response.firstName)
          setLName(response.lastName)
          setPhone(response.phone)
          setEmail(response.email)
        })
        .then(()=>setLoading(false))
      }
      else {
        navigation.navigate("AuthStack")
      }
    })
    FetchService(props.navigation,"GET","/api/geo/country")
    .then(res=>setCountries(res))
    .then(()=>setCountryLoading(false))
    .then ( ()=> FetchService(props.navigation,"GET",`/api/geo/${countryId}/city`))
    .then(res=>setCities(res))
    .then(()=>setCityLoading(false))
    .catch(er=>console.log("--e",er))

    Keyboard.addListener("keyboardDidShow", _keyboardDidShow);
    Keyboard.addListener("keyboardDidHide", _keyboardDidHide);
    return () => {
      Keyboard.removeListener("keyboardDidShow", _keyboardDidShow);
      Keyboard.removeListener("keyboardDidHide", _keyboardDidHide);
    }
  },[])
  function findCity(country){
    setCountry(country)
    setCountryId(country._id)

    FetchService(props.navigation,"GET",`/api/geo/${country._id}/city`)
    .then(res=>setCities(res))
    .then(()=>setCityLoading(false))
    .catch(er=>console.log("--e",er))
  }

  function handleSubmit() {

    if (firstName.length<1){
      setSignupError(
        language==='en'?
        'insert first name'
        :"নামের প্রথম অংশ")
    }
    else if (lastName.length<1){
      setSignupError(
        language==='en'?
        'insert last name'
        :"নামের শেষ অংশ")
    }
    else if (!country._id){
      setSignupError(
        language==='en'?
        'insert country'
        :"দেশের নাম")
    }
    else if (!city._id){
      setSignupError(
        language==='en'?
        'insert city'
        :"শহরের নাম")
    }
    else if (address1.length<1 && address2.length<1){
      setSignupError(
        language==='en'?
        'insert address'
        :"ঠিকানা")
    }
    else if (phone.length<1){
      setSignupError(
        language==='en'?
        'insert phone number'
        :"ফোন নাম্বার")
    }
    // else if (email.length<1){
    //   setSignupError('insert email')
    // }
    // else if (password.length<1){
    //   setSignupError('insert password')
    // }
    // else if (password.length<8){
    //   setSignupError('password at least 8 characters')
    // }
    // else if (password!==password2){
    //   setSignupError('passwords do not match')
    // }
    else{
      let body={
        firstName,lastName,country:country.name,city:city.name,address1,address2,phone,email,dealerCode
      }
      FetchService(props.navigation,"POST","/customer/api/profile/update",3,body)
      .then(res=>{
        if(res.success){
          props.navigation.navigate("Profile")
        }
        else throw (Object.entries(res)[0][0]+" : "+Object.entries(res)[0][1])
      })
      .catch(er=>{
        // setSignupError(er)
      })
    }

  }
    return (
      <View
      style={loginStyles.loginCont}
      >
        {
          !keyFocus?
          <HeaderCard
            botMarg={window.height/12}
            half
            />
          :<View/>
        }        
      <KeyboardAvoidingView
        style={{
          backgroundColor:'#FFFFDD',
          width: window.width/1.1,
          height: window.height/1.8,
          alignSelf:'center',
          borderRadius:15
        }}
      >
      <ScrollView
      contentInsetAdjustmentBehavior="automatic"
      >
        <View
        style={{
          width: window.width/1.1,
          height: window.height/3,
          borderTopLeftRadius:15,
          borderTopRightRadius:15
        }}
        >
          <Image
            source={profilImage}
            style={{
              width: window.width,
              height:window.height /3,
              borderRadius:20,
              resizeMode: 'contain',
              backgroundColor:'#FDFEC8'
            }}
            />
          <TouchableOpacity
          style={{
            height:30,
            width:30,
            borderRadius:15,
            backgroundColor:'#fff',
            justifyContent:'center',
            alignItems:'center',
            position:'absolute',
            top:15,
            left:15
          }}
          onPress={uploadImage}
          >
            <Text
            style={{
              fontSize:26,
              fontWeight:'bold',
              color:'black'
            }}
            >
              +
            </Text>
          </TouchableOpacity>
        </View>
        <Text
        style={{
          color:'red', 
          alignSelf:'center',
          marginBottom:10        
        }}>
        {signupError}
        </Text>
        
        
          <View
          style={{
            height:window.height/10,
            width:window.width-50,
            alignItems:'center',
            alignSelf:'center',
            justifyContent:'center'
          }}
          >
          {/* title and input starts */}   
          <Text
          style={{
            color:'#00163D',
            fontSize:20,
            alignSelf:'flex-start',
            marginLeft:10
          }}
          >{
            language==='en'?
            "Update information"
            :"রেজিস্ট্রেশন ফর্ম"  
          }
            
          </Text>
          </View>
          <View style={loginStyles.signupEmailTop}>
            <View
            style={{
              flexDirection:'row',
              justifyContent:'space-between',
              width: window.width*305/375,
            }}
            >
              <TextInput
                
                placeholderTextColor="#212121"
                placeholder={
                  language==='en'?
                  "First name"
                  :"নামের প্রথম অংশ"
                }
                value={firstName}
                keyboardType="default"
                style={[loginStyles.txtInputSignup,{
                  width: window.width*140/375,
                }]}
                autoFocus={true}
                onChangeText={setFName}
                blurOnSubmit={false}

                />
                <TextInput
                
                placeholderTextColor="#212121"
                placeholder={
                  language==='en'?
                  "Last name"
                :"নামের শেষ অংশ"
              }
                value={lastName}
                keyboardType="default"
                style={[loginStyles.txtInputSignup,{
                  width: window.width*140/375,
                }]}
                onChangeText={setLName}
                blurOnSubmit={false}

                />
            </View>
            <View
            style={{
              flexDirection:'row',
              justifyContent:'space-between',
              width: window.width*305/375,
            }}
            >
              <View>
              <Text
              style={styles.pickerText}
              >{
                language==='en'?
                      "Country":"দেশ"
              }</Text>
                <View
                style={styles.pickerContainer}
                >
                  {
                    // countryLoading?
                    <Text
                    style={styles.pickerStyle}
                    >{
                      language==='en'?
                      "     Bangladesh":"   বাংলাদেশ"
                      }</Text>
                    // :<Picker
                    // selectedValue={countryId}
                    // style={styles.pickerStyle}
                    // prompt={
                    //   language==='en'?
                    //   "Select Country":"দেশ নির্বাচন করুন"
                    // }
                    // onValueChange={async (itemValue, itemIndex) =>findCity(itemValue)}>
                    //   {
                    //     countries.map(city=><Picker.Item label={city.name} value={city._id} key={city._id} />)
                    //   }
                    // </Picker>
                  }
                </View>
                </View>
                <View>
                <Text
                style={styles.pickerText}
                >{
                  language==='en'?
                  "City":"শহর"
                  }</Text>
                <View
                style={styles.pickerContainer}
                >
                  {
                    // cityLoading?
                    <Text
                    style={styles.pickerStyle}
                    >{
                      language==='en'?
                      "       Dhaka":"  ঢাকা"
                      }</Text>
                  //   :<Picker
                  //   selectedValue={cityId}
                  //   style={styles.pickerStyle}
                  //   prompt="Select City"
                  //   onValueChange={async (itemValue, itemIndex) =>{
                  //     setCity(itemValue)
                  //     setCityId(itemValue._id)  
                  //   }}>
                  //     {
                  //       cities.map(city=><Picker.Item label={city.name} value={city._id} key={city._id} />)
                  //     }
                  // </Picker>

                  }
                  </View>
                </View>
            </View>
            <View>
              <TextInput
                
                placeholderTextColor="#212121"
                placeholder={
                  language==='en'?
                  "Address":"ঠিকানা"
                }
                value={address1}
                keyboardType="default"
                style={loginStyles.txtInputSignup}
                onChangeText={setAddress1}
                blurOnSubmit={false}

                />
            </View>
            <View
            style={{
              flexDirection:'row',
              justifyContent:'space-between',
              width: window.width*305/375,
              alignItems:'center'
            }}
            >{
              showDealerCode?
              <TextInput
                
                placeholderTextColor="#212121"
                placeholder={
                  language==='en'?
                  "Referral Code"
                  :"রেফারেন্স কোড"
                }
                value={dealerCode}
                keyboardType="default"
                style={[loginStyles.txtInputSignup,{
                  width: window.width*140/375,
                }]}
                onChangeText={setDealerCode}
                blurOnSubmit={false}
                />
                :<View
                style={{
                  marginVertical: 10,
                  height: window.height/19,
                  width: window.width*140/375,
                }}
                />
            }
            <TouchableOpacity
            style={{
              height: window.height/19,
              width: window.width/2.7,
              borderRadius:5,
              borderWidth:1,
              justifyContent:'center',
              alignItems:'center',
              borderColor:'#6E5D4B',
            }}
            onPress={()=>{
              setShowDealerCode(!showDealerCode)
            }}
            >
            <Text
                style={{
                  textAlign:'center',
                  color:'#6E5D4B',
                  fontWeight:'bold',
                  fontSize:9
                }}
                >{
                language==='en'?
                "Add referral code\n(optional)":"রেফারেন্স যোগ করুন"
                }</Text>
            </TouchableOpacity>
            </View>
            <Text
            style={{
              color:'#00163D',
              fontSize:20,
              alignSelf:'flex-start',
              marginLeft:40,
              marginVertical:10
            }}
            > 
            {
              language==='en'?
              "Contact Info":"যোগাযোগ এর মাধ্যম"
              }
          </Text>
            <View
            style={{
              flexDirection:'row',
              justifyContent:'space-between',
              width: window.width*305/375,
            }}
            >
              <TextInput
                
                placeholderTextColor="#212121"
                placeholder={
                  language==='en'?
                  "Phone":"ফোন"
                }
                value={phone}
                keyboardType='phone-pad'
                style={[loginStyles.txtInputSignup,{
                  width: window.width*140/375,

                }]}
                onChangeText={setPhone}
                blurOnSubmit={false}

                />
                <TextInput
                
                placeholderTextColor="#212121"
                placeholder={
                  language==='en'?
                  "Email ( Not required )":"ইমেইল ( আবশ্যক নয় )"
                }
                value={email}
                keyboardType='email-address'
                style={[loginStyles.txtInputSignup,{
                  width: window.width*140/375,

                }]}
                onChangeText={setEmail}
                blurOnSubmit={false}

                />
            </View>
            {/* <View
            style={{
              flexDirection:'row',
              justifyContent:'space-between',
              width: window.width*305/375,
            }}
            >
              <TextInput
                
                placeholderTextColor="#212121"
                placeholder="Password"
                secureTextEntry={true}
                value={password}
                keyboardType="default"
                style={[loginStyles.txtInputSignup,{
                  width: window.width*140/375,
                }]}
                onChangeText={setPassword}
                blurOnSubmit={false}

                />
                <TextInput
                
                placeholderTextColor="#212121"
                placeholder="Confirm"
                secureTextEntry={true}
                value={password2}
                keyboardType="default"
                style={[loginStyles.txtInputSignup,{
                  width: window.width*140/375,
                }]}
                onChangeText={setPassword2}
                blurOnSubmit={false}

                />
            </View> */}
          </View>
          {/* title and input ends */}
          <View
            style={{
              margin:15,
              justifyContent:'space-between',
              width:window.width/2,
              alignItems:'center',
              alignSelf:'center'    
            }}
            >
          <TouchableOpacity
        style={{
          height:window.height/15,
          width:window.width/2.5,
          borderRadius:5,
          backgroundColor:"#FFFE01",
          justifyContent:'center'
        }}
        onPress={handleSubmit}
        >
          <Text
          style={{
            fontSize:15,
            color:'black',
            alignSelf:'center'
          }}
          >{
            language==='en'?
            "Update"
            :"পরিবর্তন"
          }
          </Text>
        </TouchableOpacity>
        </View>
        <View
        
        style={{
          height:80
        }}
        />
        </ScrollView>
        </KeyboardAvoidingView>
        
      </View>
    );
  }

const styles = StyleSheet.create({
  pickerText:{
    fontSize:8,
    alignSelf:'flex-start',
    marginVertical:5
  },
  pickerContainer:{
    backgroundColor:'#fff',
    borderRadius:5,
    height:50,
    elevation:3,
    height: window.height/21,
    width: window.width*140/375
  },
  pickerStyle:{
    height: 20,
    width: window.width/2.5,
    marginTop:5
  }
});

  export default ProfileEdit