import React, { useState, useEffect } from 'react';
import {
    View,
    TextInput,
    Dimensions,
    StatusBar,
    Text,
    TouchableOpacity,
    KeyboardAvoidingView,
    Keyboard,
    Modal,
    Image
} from 'react-native';
import { useSelector } from 'react-redux';
import {selectLanguage} from '../features/language/languageSlice'
import FetchService from '../services/FetchService';
import HeaderCard from '../components/HeaderCard'
import iconAdded from '../assets/icons/iconAdded.png';

const window = Dimensions.get('window');
const ChangePassword: () => React$Node = (props) => {
  const [ errorText,setErrorText] = useState('')
  const {resetToken}=props.navigation.state.params
  const [keyFocus,setKeyFocus]=useState(true)
  const language = useSelector(selectLanguage);
  const [ password, setpassword ] = useState('')
  const [ newPassword, setnewPassword ] = useState('')
  const [modalVisible, setModalVisible] = useState(false);


  const _keyboardDidShow = () => {
    setKeyFocus(true)
  };

  const _keyboardDidHide = () => {
    setKeyFocus(false)
  };
  useEffect(()=>{
    Keyboard.addListener("keyboardDidShow", _keyboardDidShow);
    Keyboard.addListener("keyboardDidHide", _keyboardDidHide);
    return () => {
      Keyboard.removeListener("keyboardDidShow", _keyboardDidShow);
      Keyboard.removeListener("keyboardDidHide", _keyboardDidHide);
    }
  },[])
  function handleValidate(){
    let body={
      password,
      newPassword,
      newPassword2:newPassword
    }
    FetchService(props.navigation,"POST","/customer/api/profile/changePassword",1,body,false)
    .then(res=>{
      console.log(res)
      if(res.success){
        setModalVisible(true)
        setTimeout(()=>{
          setModalVisible(false)
          props.navigation.navigate("Profile")
        },2000)
      }
      else{
        setErrorText(Object.values(res)[0])
      }
    })
  }

  return (
    <>
      <StatusBar barStyle="dark-content" />
      <Modal
          animationType="slide"
          transparent={true}
          visible={modalVisible}
        >
          <View
          style={{
            alignSelf:'center',
            backgroundColor: "#F3F3F3",
            borderRadius: 20,
            padding: 35,
            alignItems: "center",
            shadowColor: "#000",
            marginTop:window.height/2.7,
            shadowOffset: {
              width: 0,
              height: 2
            },
            shadowOpacity: 0.25,
            shadowRadius: 3.84,
            elevation: 5
          }}
          >
            <Image
              style={{
                width: window.height / 10,
                height: window.height / 10,
                marginBottom:10
              }}
              source={iconAdded}
            />
            <Text
            style={{
              marginBottom: 15,
              textAlign: "center"
            }}
            >{
              language==='en'?
            "Password changed"
            :"পাসওয়ার্ড পরিবর্তিত"
            }</Text>
          </View>
          
        </Modal>
      <View
      style={{
          height:window.height,
          width:window.width,
          alignItems:'center',
          backgroundColor:'#FDFEC8'
        }}
      >
        {
          !keyFocus?<HeaderCard
          botMarg={window.height/15}
          isOpen={false}
          />:<View/>
        }
        <KeyboardAvoidingView
          style={{
            alignSelf:'center',
            backgroundColor: "#F3F3F3",
            borderRadius: 20,
            padding: 35,
            alignItems: "center",
            shadowColor: "#000",
            shadowOffset: {
              width: 0,
              height: 2
            },
            shadowOpacity: 0.25,
            shadowRadius: 3.84,
            elevation: 5
          }}
          ><Text
          style={{
            color:'red',
          }}
          >{errorText}</Text>
            <Text
            style={{
              color:'#333',
              alignSelf:'flex-start',
              fontSize:20
            }}
            >{
              language==='en'?
              "Edit Password":
              "পাসওয়ার্ড পরিবর্তন করুন"
            }
            </Text>
            <TextInput
              placeholderTextColor="#212121"
              placeholder={
              language==='en'?
                "Old password"
                :"পুরাতন পাসওয়ার্ড"
              }
              value={password}
              autoFocus={true}
              style={[ {
                color: '#333',
                width: window.width*305/375,
                height: window.height/14,
                fontSize: 12,
                paddingLeft: window.width*25/375,
                marginBottom: 10,
                borderRadius:5,
                borderColor:'#666',
                borderWidth:1,
              },{
                  marginTop:5
                } ]}
              onChangeText={setpassword}
              blurOnSubmit={false}
              />
              <TextInput
              placeholderTextColor="#212121"
              placeholder={
              language==='en'?
                "New password"
                :"নতুন পাসওয়ার্ড"
              }
              value={newPassword}
              style={[ {
                color: '#333',
                width: window.width*305/375,
                height: window.height/14,
                fontSize: 12,
                paddingLeft: window.width*25/375,
                marginBottom: 30,
                borderRadius:5,
                borderColor:'#666',
                borderWidth:1,
              } ]}
              onChangeText={setnewPassword}
              blurOnSubmit={false}
              />
              <TouchableOpacity
                style={{
                  height:window.height/15,
                  width:window.width/2.5,
                  borderRadius:5,
                  backgroundColor:"#FFFE01",
                  justifyContent:'center',
                  marginBottom:10,
                  borderWidth:1,
                  borderColor:"#ccc"
                }}
                onPress={handleValidate}
                >
                  <Text
                  style={{
                    fontSize:15,
                    color:'black',
                    alignSelf:'center'
                  }}
                  >
                    {
                    language=='en'?
                    "Change Password"
                    :"পাসওয়ার্ড বদলান"
                  }
                  </Text>
                </TouchableOpacity>
            
          </KeyboardAvoidingView>
      </View>
    </>
  );
};

export default ChangePassword;
