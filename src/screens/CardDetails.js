import React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  Dimensions,
  StatusBar,
  View
} from 'react-native';
const window = Dimensions.get('window');

import {
  Colors,
} from 'react-native/Libraries/NewAppScreen';
import HeaderCard from "../components/HeaderCard";
import SmallH2 from '../components/SmallH2';

const CardDetails: () => React$Node = ({navigation}) => {
  const {discount,data,name,recipe}=navigation.state.params
  return (
    <>
      <StatusBar barStyle="dark-content" />
      <SafeAreaView
      style={styles.scrollView}
      >
        <HeaderCard
          half
          botMarg={ 0 }
          />
          <SmallH2
          data={data}
          limit={data.length}
          navigation={navigation}
          darkText
          discount={discount}
          recipe={recipe}
          title={name}
          />
        
      </SafeAreaView>
    </>
  );
};

const styles = StyleSheet.create({
  scrollView: {
    paddingBottom:50,
    backgroundColor: "#fff",
    minHeight:window.height
  },
  engine: {
    position: 'absolute',
    right: 0,
  },
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: '600',
    color: Colors.black,
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: '400',
    color: Colors.dark,
  },
  highlight: {
    fontWeight: '700',
  },
  footer: {
    color: Colors.dark,
    fontSize: 12,
    fontWeight: '600',
    padding: 4,
    paddingRight: 12,
    textAlign: 'right',
  },
});

export default CardDetails;
