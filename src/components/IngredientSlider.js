import React from "react";
import {
    FlatList,
    Text,
    TouchableOpacity,
    View,
    Dimensions
} from "react-native";
import { BaseUrl } from "../env";
import FastImage from 'react-native-fast-image'

const window = Dimensions.get('window');

export default class IngredientSlider extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    const {title, data, onClick} = this.props
    return (
        <View>
          <Text
          style={{
            fontWeight:'bold',
            fontSize:15,
            marginLeft:10
          }}
          >{title}</Text>
          <FlatList
          showsHorizontalScrollIndicator={false}
          numColumns={4}
          data={data}
          ListFooterComponent={()=><View
          style={{
            height:10
          }}
          />}
          renderItem={({ item: rowData }) => {
            return (
              <TouchableOpacity
                onPress={() => onClick(rowData._id)}
                style={{
                  width: window.width /5,
                  height: window.width /5,
                  justifyContent: 'space-between',
                  alignItems: 'flex-start',
                  backgroundColor:"#FFB655",
                  borderRadius:5,
                  marginTop:25,
                  marginHorizontal:9,
                  elevation:5
              }}
              >
                <View style={{
                  width: window.width /5,
                  height: window.width*6 /50,
                  borderTopLeftRadius:5,
                  borderTopRightRadius:5,
                  backgroundColor:"#FEFEBE",
                  overflow: 'hidden',
                  borderBottomLeftRadius:5,
                  borderBottomRightRadius:5,
              }}>
                  <FastImage
                    style={{
                      flex: 1,
                      width: window.width /5-window.width/13,
                      height: window.width /5-window.width/13,
                      resizeMode:'center',
                      alignSelf:'center',
                      borderTopLeftRadius:5,
                      borderTopRightRadius:5,
                  }}
                    source={{
                      priority: FastImage.priority.high,
                      uri:
                      rowData.cover?
                      BaseUrl+rowData.cover.full:"http://mangshobazar.com:5000/images/logo-app.png"
                  }}
                  />
                  {/* <Text
                  style={{
                    fontSize:7,
                    textAlign:'center'
                  }}
                  >250 gm</Text> */}
                </View>
                <View style={{
                  width: window.width /5,
                  height: window.width *4/50,
                  borderRadius: 5,
                  overflow: 'hidden',
                  justifyContent:'center'
              }}>
                  <Text
                  style={{
                    fontSize:8,
                    fontWeight:'bold',
                    marginLeft:7
                  }}
                  >
                    {rowData.name}
                  </Text>
                  <Text
                  style={{
                    fontSize:7,
                    marginLeft:7
                  }}
                  >
                    ৳ {rowData.selectedPrice}
                  </Text>
                </View>
              </TouchableOpacity>
            );
          }}
          keyExtractor={(item,index) => {
            return item._id.toString()+new Date().getMilliseconds().toString()+index.toString()}}
            />
        </View>
    );
  }
}

