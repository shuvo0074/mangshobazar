import React, { useEffect,useState } from "react";
import {
    FlatList,
    Text,
    TouchableOpacity,
    View,
    Dimensions,
    ActivityIndicator
} from "react-native";
import { BaseUrl } from "../env";

import GlobalStyles from '../styles/Styles';
import FetchService from '../services/FetchService';
import FastImage from "react-native-fast-image";
const window = Dimensions.get('window');
import defaultLogo from '../assets/icons/iconOrderList.png';
import {selectLanguage} from "../features/language/languageSlice";
import { useSelector, connect } from 'react-redux';

export default function VerticalSliderCart ({data,navigation,title,limit}) {
  const language = useSelector(selectLanguage);
  return (
      <View
      style={{
        marginBottom:10,
        padding:10,
        backgroundColor:'#FFFEBF',
        // marginTop:window.height/10,
        borderRadius:20,
        display:'flex'
      }}
      >
        <Text
        style={{
          fontWeight:'bold',
          fontSize:15,
          color:'#FADC62',
          marginLeft: 20,
          color: '#00102D'
        }}
        >
          {
            data.length>0?
              title
              :language==='en'?
              `No ${title} available`
              :`${title} অবশিষ্ট নেই`
            }
        </Text>
        {
        data.length>0?
        <FlatList
          data={data}
          contentContainerStyle={{
          marginVertical: 15,
          }}
          ListFooterComponent={()=><View
          style={{
            height: limit==3?0: 450
          }}/>}
          renderItem={({ item }) => {
            const rowData = language==='en'?
            {...item}:
            {...item,...item.bn}
            return (
              <TouchableOpacity
                onPress={() => navigation.navigate("OrderDetails",{id:rowData._id})}
                style={{
                  width: window.width -40,
                  height: window.height /8,
                  justifyContent: 'space-between',
                  marginVertical:7,
                  alignItems: 'flex-start',
                  marginHorizontal: 10,
                  backgroundColor:"#fff",
                  borderRadius:10,
                  elevation:5,
                  flexDirection: 'row'
              }}
              >
                <View style={{
                  width: window.height /9,
                  height: window.height /9,
                  borderRadius: 5,
                  margin:5,
                  overflow: 'hidden',
              }}>
                  <FastImage
                    style={{
                      flex: 1,
                      width: undefined,
                      height: undefined,
                      resizeMode: 'cover',
                      borderRadius:8
                  }}
                    source={defaultLogo}
                  />
                </View>
                <View style={{
                  width: window.width/1.5,
                  height: window.height /8.5,
                  justifyContent:'space-between',
                  alignItems: 'center',
                  padding:5,
                  flexDirection:'row',
                  // backgroundColor:'black'
                }}>
                  <View
                  style={{
                    height: window.height /9,
                    width:window.width/2.5,
                    // backgroundColor:'red',
                    justifyContent:'center'
                  }}
                  >
                    <Text style={[GlobalStyles.caption, GlobalStyles.medium, GlobalStyles.leftTxt, {color: '#00163D'}]}
                    numberOfLines={1}>
                      Added on:{' '}
                      <Text
                      style={{
                        fontWeight:'300'
                      }}
                      >
                      {rowData.added.split("T")[0]}
                      </Text>
                    </Text>
                    <Text style={[GlobalStyles.body2, GlobalStyles.light, GlobalStyles.leftTxt, {color: '#00163D'}]}>
                      {
                      language==='en'?
                      "Status:":
                      "স্ট্যাটাস"
                      } {
                      typeof rowData.status === 'string'?
                      rowData.status:
                      language==='en'?
                      rowData.status.name:
                      rowData.status.bn.name
                      }
                    </Text>
                  </View>
                  <View
                  style={{
                    height: window.height /9,
                    width:window.width/4,
                    marginRight:12,
                    justifyContent:'center',
                    alignItems:'center'
                  }}
                  >
                    <Text
                    style={{
                      fontSize:10,
                      fontWeight:'bold',
                      color:'black'
                    }}
                    >Charge: ৳ {!rowData.deliveryCharge?0:rowData.deliveryCharge}
                      </Text>
                    <Text
                    style={{
                      fontSize:20,
                      fontWeight:'bold',
                      color:'black'
                    }}
                    >৳ {rowData.totalPrice?rowData.totalPrice:0}</Text>
                  </View>
                </View>
              </TouchableOpacity>
            );
          }}
          keyExtractor={(item,index) => {
            return item._id.toString()+new Date().getMilliseconds().toString()+index.toString()}}
            />:<View/>
        }
      </View>
    );
  }

