import React, { useState, useEffect } from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  StatusBar,
  ActivityIndicator,
  Dimensions,
  View,
  Text
} from 'react-native';
const window = Dimensions.get('window');
import SideMenu from 'react-native-side-menu';

import {
  Colors,
} from 'react-native/Libraries/NewAppScreen';
import RecipeSlider from '../components/RecipeSlider'
import SmallSquareSlider from "../components/SmallSquareSlider";
import HeaderCard from "../components/HeaderCard";

import FetchService from '../services/FetchService';
const AllRecipe: () => React$Node = ({navigation}) => {
  const [ categories, setCategories]=useState([])
  const [loading, setLoading] = useState(true)
  const [ isOpen, setIsOpen ] = useState(false)
  useEffect(()=>{
    FetchService(navigation,"GET","/api/post/category")
    .then(res=>setCategories(res.data))
    .then(()=>FetchService(navigation,"GET","/api/post?primaryCategory=1"))
    .then(response=>setData(response.data))
    .then(()=>setLoading(false))
    .catch(err=>console.log(err))
  },[])
  const [data,setData]= useState([])
  if (loading)
  return <ActivityIndicator size="large" color="#00163D" style={{
    alignSelf:'center',
    marginTop:'80%'
  }} />
  return (
    <>
      <StatusBar barStyle="dark-content" />
      <SafeAreaView>
        <ScrollView
          contentInsetAdjustmentBehavior="automatic"
          style={styles.scrollView}>
            <HeaderCard
            botMarg={0}
            />
            <SmallSquareSlider
            data={[...categories,...categories.slice(0,8-categories.length)]}
            type="recipe"
            navigation={navigation}
            />
            <RecipeSlider
            data={data}
            navigation={navigation}
            limit={6}
            darkText
            title=""
            />
            <View
            style={{
              height:window.height/25
            }}
            />
          
        </ScrollView>
      </SafeAreaView>
    </>
  );
};

const styles = StyleSheet.create({
  scrollView: {
    backgroundColor:"#FFF"
  },
  engine: {
    position: 'absolute',
    right: 0,
  },
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: '600',
    color: Colors.black,
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: '400',
    color: Colors.dark,
  },
  highlight: {
    fontWeight: '700',
  },
  footer: {
    color: Colors.dark,
    fontSize: 12,
    fontWeight: '600',
    padding: 4,
    paddingRight: 12,
    textAlign: 'right',
  },
});

export default AllRecipe;
