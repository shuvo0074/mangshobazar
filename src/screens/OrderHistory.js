import React, { useEffect,useState } from 'react';
import {
  SafeAreaView,
  StyleSheet,
  Dimensions,
  StatusBar,
  ActivityIndicator
} from 'react-native';
const window = Dimensions.get('window');
import FetchService from '../services/FetchService';

import {
  Colors,
} from 'react-native/Libraries/NewAppScreen';
import VerticalSliderCart from "../components/VerticalSliderCart";
import HeaderCard from "../components/HeaderCard";
import AsyncStorage from '@react-native-community/async-storage';
import { BaseUrl } from "../env";

const OrderHistory: () => React$Node = ({navigation}) => {
  const [ data,setData ] = useState([])
  const [loading, setLoading]=useState(true)

  useEffect(()=>{
      FetchService(navigation,"GET","/customer/api/order")
      .then(res=>setData(res.data))
      .then(()=>setLoading(false))

},[navigation])

  if (loading)
  return <ActivityIndicator size="large" color="#00163D" style={{
    alignSelf:'center',
    marginTop:'80%'
  }} />
  return (
    <>
      <StatusBar barStyle="dark-content" />
      <SafeAreaView
      style={styles.scrollView}
      >
        <HeaderCard
            half
            botMarg={ window.height/13 }
            />
        <VerticalSliderCart
        data={data}
        navigation={navigation}
        darkText
        limit={15}
        title={"Orders"}
        />
      </SafeAreaView>
    </>
  );
};

const styles = StyleSheet.create({
  scrollView: {
    paddingBottom:50,
    backgroundColor: "#fff",
    minHeight:window.height
  },
  engine: {
    position: 'absolute',
    right: 0,
  },
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: '600',
    color: Colors.black,
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: '400',
    color: Colors.dark,
  },
  highlight: {
    fontWeight: '700',
  },
  footer: {
    color: Colors.dark,
    fontSize: 12,
    fontWeight: '600',
    padding: 4,
    paddingRight: 12,
    textAlign: 'right',
  },
});

export default OrderHistory;
