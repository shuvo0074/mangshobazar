import React, { useEffect,useState } from 'react';
import {
  ScrollView,
  StyleSheet,
  Dimensions,
  StatusBar,
  ActivityIndicator,
  View,
  Text
} from 'react-native';
const window = Dimensions.get('window');
import FetchService from '../services/FetchService';
import AsyncStorage from '@react-native-community/async-storage';
import { BaseUrl } from "../env";

import {
  Colors,
} from 'react-native/Libraries/NewAppScreen';
import { useSelector, connect } from 'react-redux';

import {selectLanguage} from '../features/language/languageSlice'
import HeaderCard from "../components/HeaderCard";
import SmallH2 from '../components/SmallH2';

const OrderDetails: () => React$Node = ({navigation}) => {
  const language = useSelector(selectLanguage);
  const {id}=navigation.state.params

  const [ data,setData ] = useState([])
  const [loading, setLoading]=useState(true)

  useEffect(()=>{
      FetchService(navigation,"GET","/customer/api/order/"+id)
      .then(res=>setData(res.order))
      .then(()=>setLoading(false))
      .catch(err=>console.log(err))
},[navigation])

  if (loading)
  return <ActivityIndicator size="large" color="#00163D" style={{
    alignSelf:'center',
    marginTop:'80%'
  }} />
  return (
    <>
      <StatusBar barStyle="dark-content" />
      <ScrollView
      style={styles.scrollView}
      >{
        console.log(data)
      }
        <HeaderCard
            half
            botMarg={ window.height/13 }
            />

        <View
        style={{
          width:window.width/1.1,
          paddingVertical:20,
          borderRadius:15,
          backgroundColor:'white',
          alignSelf:'center',
          elevation:5
        }}
        >
          <Text
          style={{
            fontSize:16,
            fontWeight:'bold',
            textAlign:'center'
          }}
          >{data.customer.firstName} {data.customer.lastName}</Text>
          <Text
          style={{
            fontSize:15,
            textAlign:'center'
          }}
          ><Text
          style={{
            fontWeight:'bold'
          }}
          >Address: </Text>{data.customer.address1} {data.customer.address2}</Text>

          <Text
          style={{
            fontSize:15,
            textAlign:'center'
          }}
          >
            <Text
            style={{
              fontWeight:'bold'
            }}
            >Total price: </Text>৳ {data.totalPrice}</Text>
            <Text
          style={{
            fontSize:15,
            textAlign:'center'
          }}
          >
            <Text
            style={{
              fontWeight:'bold'
            }}
            >Delivery charge: </Text>৳ {!data.deliveryCharge?0:data.deliveryCharge}</Text>
        <Text
          style={{
            fontSize:15,
            textAlign:'center'
          }}
          >
            <Text
            style={{
              fontWeight:'bold'
            }}
            >{
              language==='en'?
              "Status:":
              "স্ট্যাটাস"
              } </Text>{
                typeof data.status === 'string'?
                data.status:
                language==='en'?
                data.status.name:
                data.status.bn.name
                }</Text>


        </View>
        <SmallH2
          data={data.products}
          navigation={navigation}
          limit={6}
          darkText
          cart={true}
          title={
            language==='en'?
            "Purchased Products"
          :"ক্রয়কৃত পণ্য"}
        />
        <View
        style={{
          height:window.height/7
        }}
        />
      </ScrollView>
    </>
  );
};

const styles = StyleSheet.create({
  scrollView: {
    paddingBottom:50,
    backgroundColor: "#fff",
    minHeight:window.height
  },
  engine: {
    position: 'absolute',
    right: 0,
  },
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: '600',
    color: Colors.black,
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: '400',
    color: Colors.dark,
  },
  highlight: {
    fontWeight: '700',
  },
  footer: {
    color: Colors.dark,
    fontSize: 12,
    fontWeight: '600',
    padding: 4,
    paddingRight: 12,
    textAlign: 'right',
  },
});

export default connect()(OrderDetails);
