import { configureStore } from '@reduxjs/toolkit';
import cartReducer from '../features/counter/counterSlice';
import languageReducer from '../features/language/languageSlice';

export default configureStore({
  reducer: {
    cart: cartReducer,
    language: languageReducer
  },
});
