import React, { useEffect } from "react";
import {
    FlatList,
    Text,
    TouchableOpacity,
    View,
    Dimensions
} from "react-native";
import { BaseUrl } from "../env";

import GlobalStyles from '../styles/Styles';
import SliderStyles from '../styles/SliderStyles';
import FastImage from "react-native-fast-image";
const window = Dimensions.get('window');

export default function WideSlider (props) {
  useEffect(()=>{},[props])
  function _handlePress(id) {
    props.navigation.navigate("EventDetails",{id})
  }

    return (
        <View
        style={{
          marginTop:-10
        }}
        >
          <FlatList
          horizontal
          showsHorizontalScrollIndicator={false}
          data={props.data}
          contentContainerStyle={{
            paddingLeft: 10,
            marginBottom:20,
            height: window.height / 8 +10,

          }}
          renderItem={({ item: rowData }) => {
            return (
              <TouchableOpacity
                onPress={() => _handlePress(rowData._id)}
                style={{
                  width: window.width /2.5,
                  height: window.height / 8,
                  alignItems: 'center',
                  marginHorizontal: 10,
                  backgroundColor:"#F9F89C",
                  borderRadius:10,
                  flexDirection:'row',
                  elevation:5

                }}
              >
                <View 
                style={{
                  width: window.width /6,
                  height: window.height / 8,
                    overflow: 'hidden',
                }}>
                  <FastImage
                    style={[{
                      flex: 1,
                      width: undefined,
                      height: undefined,
                      resizeMode:'cover',
                      borderTopLeftRadius:10,
                      borderBottomLeftRadius:10,
                      margin:1
                    }]}
                    source={{
                      priority: FastImage.priority.high,
                      uri:
                      rowData.cover?
                      BaseUrl+rowData.cover.full:"http://mangshobazar.com:5000/images/logo-app.png"
                  }}
                  />
                  {/* discount */}
                  <View
                  style={{
                    height: window.width /15,
                    width: window.width /15,
                    borderRadius: window.width /24,
                    position:'absolute',
                    top: window.width /70,
                    left: window.width /70,
                    backgroundColor:'#FD435F',
                    justifyContent:'center',
                    alignItems:'center'
                  }}
                  >
                    <Text
                    numberOfLines={2}
                    style={{
                      textAlign:'center',
                      fontSize:7,
                      fontWeight:'bold'
                    }}
                    >120tk{'\n'} off </Text>
                  </View>
                </View>
                <View style={{
                    width: window.width /4.5,
                    height: window.height /9,
                    justifyContent: 'center',
                    alignItems: 'center',
                    padding:5
                }}
                >
                <Text style={{
                  color: '#000',
                  fontSize: 8,
                  fontWeight: 'bold',
                  textAlign: 'left'
                  }}
                  numberOfLines={1}
                  >
                    {rowData.name}
                    </Text>
                    <Text 
                    style={{
                      color: '#000',
                      fontSize: 6,
                      fontWeight: '300',
                      textAlign: 'left'
                      }}
                    >

                    {rowData.name}
                    </Text>
                </View>
              </TouchableOpacity>
            );
          }}
          keyExtractor={(item,index) => {
            return item._id.toString()+new Date().getMilliseconds().toString()+index.toString()}}
            />
        </View>
    );
  }

